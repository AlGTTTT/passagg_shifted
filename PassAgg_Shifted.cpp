
#include "PassAgg_Shifted.h"

FILE *fin_Train;
FILE *fin_Test;

FILE *fout;

int
	nY_Train_Actual_Glob = -2, // no training yet

	iVec_Train_Glob = -2,// no training yet

	nNumOfItersOfTrainingTot_Glob = nNumOfItersOfTrainingTot,
	nK_Glob = nK,
	nDim_H_Glob = nDim_H,

	nSrandInit_Glob = nSrandInit,

	iFea1_Glob,
	iFea2_Glob;

float
	fFeaConstInit_Glob = fFeaConstInit,

	fEpsilon_Glob = fEpsilon,

	fBiasForClassifByLossFunction_Glob = fBiasForClassifByLossFunction,

	fU_Init_Min_Glob = fU_Init_Min,
	fU_Init_Max_Glob = fU_Init_Max,

	fW_Init_Min_Glob = fW_Init_Min,
	fW_Init_Max_Glob = fW_Init_Max,

	fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob;

int main()
{
int Normalizing_Every_Fea_To_ARange(
				const float fLargef,
				const float fepsf,

				const int nDimf,

				const int nVec1stf,
				const int nVec2ndf, 

				float fFeaMinArrf[],
				float fFeaMaxArrf[],
				
				float fFeaArr1stf[], //to be normalized
				float fFeaArr2ndf[]); //to be normalized

int NumOfNonZeros(
				  const int nDimf,
				  const int nArrf[], // [nDimf]

				  int &NumOfNonZeros);

int FindingBestSeparByRatioOfOneFea(
					const float flargef,
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fPrecisionOf_Golden_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, 

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					float &fEfficBestSeparOfOneFeaf,

					float &fPosSeparOfOneFeaBestf,

					int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestf,
					float &fNumArr2ndSeparBestf,

					float &fDiffSeparOfArr1stAndArr2ndBestf,
					float &fRatioOfSeparOfArr1stAndArr2ndBestf);

int SelectingBestFeas(
					const float fLargef,
					const  float fepsf,

					const int nDimf,
					const int nDimSelecMaxf,

					const int nNumVec1stf,
					const int nNumVec2ndf,

					const float fPrecisionOf_Golden_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
					const float fEfficBestSeparOfOneFeaAcceptMinf,

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

					int &nDimSelecf,
					//float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf]
					//float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf]

					float &fRatioBestMinf,

					int &nPosOneFeaBestMaxf,

					int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestMaxf,
					float &fNumArr2ndSeparBestMaxf,

					float fRatioBestArrf[], //nDimSelecMaxf

					int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

int Converting_Arr_To_Selec(
					const int nDimf,
					const int nDimSelecf,

					const int nNumVecf,

					const int nPosFeaSeparBestArr[], //[nDimSelecf]

					const float fVecArr[],
					float fVecSelecArr[]); //[nDimSelecf]


int FindingBestSeparByDiffOfOneFea(
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fPrecisionOf_Golden_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByDiffOfOneFeaf, 

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					float &fEfficBestSeparOfOneFeaf,

					float &fPosSeparOfOneFeaBestf,

					int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestf,
					float &fNumArr2ndSeparBestf,

					float &fDiffSeparOfArr1stAndArr2ndBestf,
					float &fRatioOfSeparOfArr1stAndArr2ndBestf);


int doPasAggMaxOut_TrainTest(
	const int nNumOfItersOfTrainingTotf,

		//const int nDimf, == nDim_D_WithConstf

		const int nNumVecTrainTotf,
		const int nNumVecTestTotf,
		/////////////////////////

		const int nDim_D_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		///////////////////////////////////////////////////

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

int
	iFea,
	iFea1,

	n1stOr2nd,
	i1,
	iVec,
	iPos,
	
	nDimRead,

	 nVecInit1st, 
	 nVecInit2nd, 

	nIndic_1st_2nd, //1-->1st or -1 -->2nd
	nResult;


float
	fReadf,

	fRatioMax,

	fDiffBest,
	fRatioBest;

	//fRatioBestMax;

//////////////////////////////////////////////////////////////////////////////////////////

//!change 'nNumVecTrainTot' as well
//fin_Train = fopen("svmguide1_train.txt", "r"); //nNumVecTrainTot = 3089
fin_Train = fopen("svmguide1_train_2000_2178.txt", "r"); //nNumVecTrainTot = 4178


//!change 'nNumVecTrainTot' as well
//fin_Train = fopen("svmguide1_train_3times_Negatives.txt", "r"); //
//fin_Train = fopen("svmguide1_train_Pos49_Neg101.txt", "r"); //actually, 49 and 151

fin_Test = fopen("svmguide1_test.txt","r");

if (fin_Train == NULL || fin_Test == NULL)
{
printf("\n\n An error: fin_Train == NULL || ...");
getchar();	exit(1);
} //if (fin_Train == NULL || ...)

//////////////////////////////////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS

fout = fopen("wMain_PassAggMaxout.txt","w");

if (fout == NULL) // 
{
printf("\n\nAn error: fout == NULL");
getchar();	exit(1);
} //if (fout == NULL )


fprintf(fout, "\n\n The constants: nDim_D_WithConst = %d, nDim_H = %d, fW_Init_Min_Glob = %E, fW_Init_Max_Glob = %E, nNumOfHyperplanes = nK = %d", nDim, nDim_H, fW_Init_Min_Glob, fW_Init_Max_Glob, nNumOfHyperplanes);
fprintf(fout, "\n nDim_U = ((nDim_D_WithConst*nDim_H*nK) = %d, fU_Init_Min_Glob = %E, fU_Init_Max_Glob = %E, fAlpha = %E, fEpsilon = %E, fCr = %E, fC = %E",
	nDim_U, fU_Init_Min_Glob, fU_Init_Max_Glob, fAlpha, fEpsilon, fCr, fC);

fprintf(fout, "\n\n  nNumVecTrainTot = %d, nNumVecTestTot = %d, nNumOfItersOfTrainingTot = %d", nNumVecTrainTot, nNumVecTestTot, nNumOfItersOfTrainingTot);

	#ifdef USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1
	fprintf(fout, "\n\n All feas are normalized to mean 0 and stdev 1");
	#endif //#ifdef USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//Specified in 'PasAggMaxOut_Train'
  //srand(1);

///////////////////
/*
nResult = SelectingBestFeas(
					fLarge, //const float fLargef,
					eps, //const  float fepsf,

					nDim, //const int nDimf,
					nDimSelecMax, //const int nDimSelecMaxf,

					nNumVecSelec, //const int nNumVec1stf,
					nNumVecSelec, //const int nNumVec2ndf,

					fPrecisionOf_Golden_Search, //const float fPrecisionOf_Golden_Searchf,

					0.0, //const  float fBorderBelf,
					1.0, //const  float fBorderAbof,

					nNumIterMaxForFindingBestSeparByRatioOfOneFea, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
					fEfficBestSeparOfOneFeaAcceptMin, //const float fEfficBestSeparOfOneFeaAcceptMinf,

					fFeaSelecAll_FeasPosArr, //const float fArr1stf[], //0 -- 1
					fFeaSelecAll_FeasNegArr, //const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

					nDimSelec, //int &nDimSelecf,

					fRatioBestMin, //float &fRatioBestMinf,

					nPosOneFeaBestMax, //int &nPosOneFeaBestMaxf,

					nSeparDirectionBestMax, //int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparBestMax, //float &fNumArr1stSeparBestMaxf,
					fNumArr2ndSeparBestMax, //float &fNumArr2ndSeparBestMaxf,

					fRatioBestArr, //float fRatioBestArrf[], //nDimSelecMaxf

					nPosFeaSeparBestArr); //int nPosFeaSeparBestArrf[]) //nDimSelecMaxf

printf("\n\nAfter 'SelectingBestFeas': fRatioBestMin = %E, nPosOneFeaBestMax = %d, nDimSelec = %d",
	   fRatioBestMin, nPosOneFeaBestMax, nDimSelec);

fprintf(fout,"\n\nAfter 'SelectingBestFeas': fRatioBestMin = %E, nPosOneFeaBestMax = %d, nDimSelec = %d",
	   fRatioBestMin, nPosOneFeaBestMax, nDimSelec);

	if (nResult == 2)
	{
	printf("\n\nAfter 'SelectingBestFeas': nResult == 2 -- exit");
	fprintf(fout,"\n\nAfter 'SelectingBestFeas': nResult == 2 -- exit");
	getchar(); exit(1);
	} // if (nResult == 2)

for (iFea = 0; iFea < nDimSelec; iFea++)
{
printf("\n\nAfter 'SelectingBestFeas': fRatioBestArr[%d] = %E, nPosFeaSeparBestArr[%d] = %d",
	iFea,fRatioBestArr[iFea], iFea,nPosFeaSeparBestArr[iFea]);

fprintf(fout,"\n\nAfter 'SelectingBestFeas': fRatioBestArr[%d] = %E, nPosFeaSeparBestArr[%d] = %d",
	iFea,fRatioBestArr[iFea], iFea,nPosFeaSeparBestArr[iFea]);
} //for (iFea = 0; iFea < nDimSelec; iFea++)

fflush(fout);
*/

PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TrainResults;
PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TestResults;



nResult = doPasAggMaxOut_TrainTest(
			nNumOfItersOfTrainingTot_Glob, //const int nNumOfItersOfTrainingTotf,

				//const int nDimf, == nDim_D_WithConstf

			nNumVecTrainTot, //const int nNumVecTrainTotf,
			nNumVecTestTot, //const int nNumVecTestTotf,
			/////////////////////////

			nDim_D_WithConst, //const int nDim_D_WithConstf, // = dimension of the original space

			nDim_H_Glob, //const int nDim_Hf, //dimension of the nonlinear/transformed space

			nK_Glob, //const int nKf, //nNumOfHyperplanes
			nDim_U, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

			///////////////////////
			fAlpha, //const float fAlphaf, // < 1.0
			fEpsilon_Glob, //const float fEpsilonf,
			fCr, //const float fCrf,
			fC, //const float fCf,
			///////////////////////////////////////////////////

			&sPasAggMaxOut_TrainResults, //PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
			&sPasAggMaxOut_TestResults); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

////////////////////////////////////////////////////////////
fclose(fin_Train);
fclose(fin_Test);

#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\n The end:\n");
fprintf(fout,"\n\n The end:\n"); 

printf("\n\nPlease press any key:"); fflush(fout); getchar();
fclose(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

return 1;
} //int main()

int	VecInit(
		const int nDimf,
		const int nNumVecf,
		const int nNumIterMaxForVecInitf,

		const int nIndic_1st_2ndf,
		float fVecArr[])
{
int
	//nRand_1f,
	//nRand_2f,

	iIterf,
	nNumVecFilled = 0,
	//iVecf,
	iFeaf;

/*
float
	fRand_1f,
	fRand_2f;
*/

int* nRandArrf = new int[nDimf];
float *fRandArrf = new float[nDimf];

if (nRandArrf == NULL || fRandArrf == NULL)
{
printf("\n\nAn error in 'VecInit': nRandArrf == NULL || fRandArrf == NULL");
getchar(); exit(1);
} //if (nRandArrf == NULL || fRandArrf == NULL)


if (nIndic_1st_2ndf != 1 && nIndic_1st_2ndf != -1)
{
printf("\n\nAn error in 'VecInit': nIndic_1st_2ndf = %d",nIndic_1st_2ndf);
getchar(); exit(1);
} // if (nIndic_1st_2ndf != 1 && nIndic_1st_2ndf != -1)

for (iIterf = 0; iIterf < nNumIterMaxForVecInitf; iIterf++)
{

	//for (iFeaf = 0; iFeaf < 2; iFeaf++)
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
	nRandArrf[iFeaf] = rand();
	fRandArrf[iFeaf] = (float)(nRandArrf[iFeaf])/(float)(RAND_MAX);
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)


/*
nRand_1f = srand();
nRand_2f = srand();

fRand_1f = (float)(nRand_1f)/(float)(RAND_MAX);
fRand_2f = (float)(nRand_2f)/(float)(RAND_MAX);
*/
	if (nIndic_1st_2ndf == 1 && fRandArrf[1] >= fRandArrf[0])
	{
	nNumVecFilled += 1;

		//for (iFeaf = 0; iFeaf < 2; iFeaf++)
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
		fVecArr[iFeaf + (nNumVecFilled - 1)*nDimf] = fRandArrf[iFeaf];
		} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		if (nNumVecFilled >= nNumVecf)
			break;
	} // if (nIndic_1st_2ndf == 1 && fRandArrf[1] >= fRandArrf[0])

	if (nIndic_1st_2ndf == -1 && fRandArrf[1] < fRandArrf[0])
	{
	nNumVecFilled += 1;

		//for (iFeaf = 0; iFeaf < 2; iFeaf++)
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
		fVecArr[iFeaf + (nNumVecFilled - 1)*nDimf] = fRandArrf[iFeaf];
		} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		if (nNumVecFilled >= nNumVecf)
			break;
	} // if (nIndic_1st_2ndf == 1 && fRandArrf[1] < fRandArrf[0)

} // for (iIterf = 0; iIterf < nNumIterMaxForVecInitf; iIterf++)


if (nNumVecFilled != nNumVecf)
{
printf("\n\nAn error in 'VecInit': nNumVecFilled = %d != nNumVecf = %d",nNumVecFilled,nNumVecf);
printf("\n\nPlease increase 'nNumIterMaxForVecInitf'\n");
getchar(); exit(1);
} //if (nNumVecFilled != nNumVecf)

delete [] fRandArrf;
delete [] nRandArrf;
return 1;
} // int VecInit(

int	WeiEstimLogisRegr(
		const int nDimf,

		const int nNumIterForWeiMaxf,

		const int nNumVecTrain_1stf,
		const int nNumVecTrain_2ndf,

		const int nNumVecSelecf,
		const int nProdSelecForWeiEstimLogisRegrf,

		const float fRatef, 
		const float fVecTrain_1stf[],
		const float fVecTrain_2ndf[],

		float fWeif[],
		float &fEfficTrain_1stf,
		float &fEfficTrain_2ndf)
{

int Rand_Selec (
				const int nDimf,
				const int nVecInitf,
				const int nVecSelecf, // < nVecInitf
				const float fFeaAll_InitArrf[],
				
				float fFeaSelecAll_FeasArr[]);

float fProbab(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[]);

float Effici_EstimLogisReg(
		const int nDimf,
		const int nNumVecf,

		const int nIndic_1st_2ndf,

		const float fVecArrf[],
		const float fWeif[]);

int
	nResf,

	nNumRight_1stf = 0,
	nNumRight_2ndf = 0,

	nNumWrong_1stf = 0,
	nNumWrong_2ndf = 0,
	nIndic_1st_2ndf,

	iLoopsf,
	iVecf,
	iFeaf;

float
//	fScaProdf,
	fProbf;

printf("\n\n'WeiEstimLogisRegr' 1"); fflush(stdout);
float *fVecSelec_1stf = new float[nProdSelecForWeiEstimLogisRegrf];

if (fVecSelec_1stf == NULL)
{
printf("\n\nAn error in 'WeiEstimLogisRegr': fVecSelec_1stf == NULL ");
getchar(); exit(1);
} //if (fVecSelec_1stf == NULL)

printf("\n\n'WeiEstimLogisRegr' 1_1"); fflush(stdout);

float *fVecSelec_2ndf = new float[nProdSelecForWeiEstimLogisRegrf];
if (fVecSelec_2ndf == NULL)
{
printf("\n\nAn error in 'WeiEstimLogisRegr': fVecSelec_2ndf == NULL ");
getchar(); exit(1);
} //if (fVecSelec_2ndf == NULL)

printf("\n\n'WeiEstimLogisRegr' 1_2"); fflush(stdout);

float *fVec_1stf = new float[nDimf];
if (fVec_1stf == NULL)
{
printf("\n\nAn error in 'WeiEstimLogisRegr': fVec_1stf == NULL ");
getchar(); exit(1);
} //if (fVec_1stf == NULL)

printf("\n\n'WeiEstimLogisRegr' 1_3"); fflush(stdout);
float *fVec_2ndf = new float[nDimf];
if (fVec_2ndf == NULL)
{
printf("\n\nAn error in 'WeiEstimLogisRegr': fVec_2ndf == NULL ");
getchar(); exit(1);
} //if (fVec_2ndf == NULL)

printf("\n\n'WeiEstimLogisRegr' 1_4"); fflush(stdout);

if (fVec_1stf == NULL || fVec_2ndf == NULL || fVecSelec_1stf == NULL || fVecSelec_2ndf == NULL)
{
printf("\n\nAn error in 'WeiEstimLogisRegr': fVec_1stf == NULL || fVec_2ndf == NULL || ...");
getchar(); exit(1);
} //if (fVec_1stf == NULL || fVec_2ndf == NULL || ...)

printf("\n\n'WeiEstimLogisRegr' 2"); fflush(stdout);

/*
nResf = Rand_Selec (
				nDimf,
				nNumVecTrain_1stf,

				nNumVecSelecf, // < nVecInitf
				fVecTrain_1stf, //const float fFeaAll_InitArrf[],
				
				fVecSelec_1stf); //float fFeaSelecAll_FeasArr[]);


nResf = Rand_Selec (
				nDimf,
				nNumVecTrain_2ndf,

				nNumVecSelecf, // < nVecInitf
				fVecTrain_2ndf, //const float fFeaAll_InitArrf[],
				
				fVecSelec_2ndf); //float fFeaSelecAll_FeasArr[]);
*/


for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
	fWeif[iFeaf] = 0.0;

printf("\n\n'WeiEstimLogisRegr' 3"); fflush(stdout);

//fProbf = 0.5; 

for (iLoopsf = 0; iLoopsf < nNumIterForWeiMaxf; iLoopsf++)
{
nNumRight_1stf = 0;
nNumRight_2ndf = 0;

nNumWrong_1stf = 0;
nNumWrong_2ndf = 0;

printf("\n\n'WeiEstimLogisRegr' 3_1: iLoopsf = %d",iLoopsf); fflush(stdout);

nResf = Rand_Selec (
				nDimf,
				nNumVecTrain_1stf,

				nNumVecSelecf, // < nVecInitf
				fVecTrain_1stf, //const float fFeaAll_InitArrf[],
				
				fVecSelec_1stf); //float fFeaSelecAll_FeasArr[]);


nResf = Rand_Selec (
				nDimf,
				nNumVecTrain_2ndf,

				nNumVecSelecf, // < nVecInitf
				fVecTrain_2ndf, //const float fFeaAll_InitArrf[],
				
				fVecSelec_2ndf); //float fFeaSelecAll_FeasArr[]);

printf("\n\n'WeiEstimLogisRegr' 3_2: iLoopsf = %d",iLoopsf); fflush(stdout);


	for (iVecf = 0; iVecf < nNumVecSelecf;  iVecf++)
	{
	printf("\n1: WeiEstimLogisRegr: iVecf = %d, iLoopsf = %d",
		iVecf,iLoopsf); fflush(stdout);

	//1st
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVec_1stf[iFeaf] = fVecSelec_1stf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	fProbf = fProbab(
						nDimf,
						fWeif,
						fVec_1stf);


	//printf("\n\n2 WeiEstimLogisRegr: iVecf = %d, fProbf = %E",iVecf,fProbf); fflush(stdout);

		if (fProbf > 0.5) //predicting 1st //or 2nd?
		{
		nNumRight_1stf += 1;
		} // if (fProbf > 0.5)
		else 
		{
		nNumWrong_1stf += 1;
		} //else

		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fWeif[iFeaf] = fWeif[iFeaf] + (1.0 - fProbf)*fVec_1stf[iFeaf]*fRatef;
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	//////////////////////////////////
	//2nd
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVec_2ndf[iFeaf] = fVecSelec_2ndf[iFeaf + iVecf*nDimf];

		//printf("\n\n3_a: WeiEstimLogisRegr: iVecf = %d, fVec_2ndf[%d] = %E",
		//	iVecf,iFeaf,fVec_2ndf[iFeaf]); 	fflush(stdout);

		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	fProbf = fProbab(
						nDimf,
						fWeif,
						fVec_2ndf);

	//printf("\n\n3_b: WeiEstimLogisRegr: iVecf = %d",iVecf); fflush(stdout);

		if (fProbf > 0.5) //predicting 2nd //or 2nd?
		{
		nNumWrong_2ndf += 1;
		} // if (fProbf > 0.5)
		else 
		{
		nNumRight_2ndf += 1;
		} //else

		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fWeif[iFeaf] = fWeif[iFeaf] - fProbf*fVec_2ndf[iFeaf]*fRatef;
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	} // for (iVecf = 0; iVecf < nNumVecSelecf;  iVecf++)

printf("\n\n'WeiEstimLogisRegr' 3_3: iLoopsf = %d",iLoopsf); fflush(stdout);

} //for (iLoopsf = 0; iLoopsf < nNumIterForWeiMaxf; iLoopsf++)

printf("\n\n'WeiEstimLogisRegr' 4"); fflush(stdout);

nIndic_1st_2ndf = 1;
fEfficTrain_1stf =  Effici_EstimLogisReg(
						nDimf,
						nNumVecTrain_1stf,

						nIndic_1st_2ndf,

						 fVecTrain_1stf,
						 fWeif);

nIndic_1st_2ndf = -1;

fEfficTrain_2ndf =  Effici_EstimLogisReg(
						nDimf,
						nNumVecTrain_2ndf,

						nIndic_1st_2ndf,

						 fVecTrain_2ndf,
						 fWeif);

printf("\n\n'WeiEstimLogisRegr' 5"); fflush(stdout);

/*
//1st and 2nd separately
	for (iVecf = 0; iVecf < nNumVec_1stf;  iVecf++)
	{

//1st
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVec_1stf[iFeaf] = fVecSelec_1stf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	fProbf = fProbab(
						nDimf,
						fWeif,
						fVec_1stf);


		if (fProbf > 0.5) //predicting 1st //or 2nd?
		{
		nNumRight_1stf += 1;
		} // if (fProbf > 0.5)
		else 
		{
		nNumWrong_1stf += 1;
		} //else

		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fWeif[iFeaf] = fWeif[iFeaf] + (1.0 - fProbf)*fVec_1stf[iFeaf]*fRatef;
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
	} // for (iVecf = 0; iVecf < nNumVec_1stf;  iVecf++)


//2nd
	for (iVecf = 0; iVecf < nNumVec_2ndf;  iVecf++)
	{
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVec_2ndf[iFeaf] = fVecSelec_2ndf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	fProbf = fProbab(
						nDimf,
						fWeif,
						fVec_2ndf);

		if (fProbf > 0.5) //predicting 2nd //or 2nd?
		{
		nNumWrong_2ndf += 1;
		} // if (fProbf > 0.5)
		else 
		{
		nNumRight_2ndf += 1;
		} //else

		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fWeif[iFeaf] = fWeif[iFeaf] - fProbf*fVec_2ndf[iFeaf]*fRatef;
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	} // for (iVecf = 0; iVecf < nNumVec_2ndf;  iVecf++)
*/

printf("\n\nDuring 'WeiEstimLogisRegr': nNumRight_1stf = %d, nNumWrong_1stf = %d, nNumRight_2ndf = %d, nNumWrong_2ndf = %d",
										nNumRight_1stf,nNumWrong_1stf,nNumRight_2ndf,nNumWrong_2ndf);

fprintf(fout,"\n\nDuring 'WeiEstimLogisRegr': nNumRight_1stf = %d, nNumWrong_1stf = %d, nNumRight_2ndf = %d, nNumWrong_2ndf = %d",
										nNumRight_1stf,nNumWrong_1stf,nNumRight_2ndf,nNumWrong_2ndf);


delete [] fVecSelec_1stf;
delete [] fVecSelec_2ndf;

delete [] fVec_1stf;
delete [] fVec_2ndf;

return 1;
} //int	WeiEstimLogisRegr(


int	WeiEstimRandomBudgetPerceptron(
		const int nDimf,
		const int nNumVec_1stf,
		const int nNumVec_2ndf,

		//const float fRatef, 
		const float fVecTrain_1stf[],
		const float fVecTrain_2ndf[],
		float fWeif[],

		float &fEfficTrain1stf,
		float &fEfficTrain2ndf)
{

int Rand_Selec (
				const int nDimf,
				const int nVecInitf,
				const int nVecSelecf, // < nVecInitf
				const float fFeaAll_InitArrf[],
				
				float fFeaSelecAll_FeasArr[]);

float fProbab(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[]);

int
	nNumRight_1stf = 0,
	nNumRight_2ndf = 0,

	nNumWrong_1stf = 0,
	nNumWrong_2ndf = 0,
	iVecf,
	iFeaf;

float
//	fScaProdf,
	fProbf;

float *fVec_1stf = new float[nDimf];
float *fVec_2ndf = new float[nDimf];

if (fVec_1stf == NULL || fVec_2ndf == NULL)
{
printf("\n\nAn error in 'WeiEstimRandomBudgetPerceptron': fVec_1stf == NULL || fVec_2ndf == NULL");
getchar(); exit(1);
} //if (fVec_1stf == NULL || fVec_2ndf == NULL)


for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
	fWeif[iFeaf] = 0.0;

//fProbf = 0.5; 

if (nNumVec_1stf == nNumVec_2ndf) 
{
	for (iVecf = 0; iVecf < nNumVec_1stf;  iVecf++)
	{

//1st
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVec_1stf[iFeaf] = fVecTrain_1stf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	fProbf = fProbab(
						nDimf,
						fWeif,
						fVec_1stf);



		if (fProbf > 0.5) //predicting 1st //or 2nd?
		{
		nNumRight_1stf += 1;
		} // if (fProbf > 0.5)
		else 
		{
		nNumWrong_1stf += 1;
		} //else

		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		//fWeif[iFeaf] = fWeif[iFeaf] + (1.0 - fProbf)*fVec_1stf[iFeaf]*fRatef;
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

//////////////////////////////////
//2nd
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVec_2ndf[iFeaf] = fVecTrain_2ndf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	fProbf = fProbab(
						nDimf,
						fWeif,
						fVec_2ndf);

		if (fProbf > 0.5) //predicting 2nd //or 2nd?
		{
		nNumWrong_2ndf += 1;
		} // if (fProbf > 0.5)
		else 
		{
		nNumRight_2ndf += 1;
		} //else

		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		//fWeif[iFeaf] = fWeif[iFeaf] - fProbf*fVec_2ndf[iFeaf]*fRatef;
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	} // for (iVecf = 0; iVecf < nNumVec_1stf;  iVecf++)

/*
//1st and 2nd separately
	for (iVecf = 0; iVecf < nNumVec_1stf;  iVecf++)
	{

//1st
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVec_1stf[iFeaf] = fVecTrain_1stf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	fProbf = fProbab(
						nDimf,
						fWeif,
						fVec_1stf);


		if (fProbf > 0.5) //predicting 1st //or 2nd?
		{
		nNumRight_1stf += 1;
		} // if (fProbf > 0.5)
		else 
		{
		nNumWrong_1stf += 1;
		} //else

		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fWeif[iFeaf] = fWeif[iFeaf] + (1.0 - fProbf)*fVec_1stf[iFeaf]*fRatef;
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
	} // for (iVecf = 0; iVecf < nNumVec_1stf;  iVecf++)


//2nd
	for (iVecf = 0; iVecf < nNumVec_2ndf;  iVecf++)
	{
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVec_2ndf[iFeaf] = fVecTrain_2ndf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	fProbf = fProbab(
						nDimf,
						fWeif,
						fVec_2ndf);

		if (fProbf > 0.5) //predicting 2nd //or 2nd?
		{
		nNumWrong_2ndf += 1;
		} // if (fProbf > 0.5)
		else 
		{
		nNumRight_2ndf += 1;
		} //else

		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fWeif[iFeaf] = fWeif[iFeaf] - fProbf*fVec_2ndf[iFeaf]*fRatef;
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)

	} // for (iVecf = 0; iVecf < nNumVec_2ndf;  iVecf++)
*/

printf("\n\nDuring 'WeiEstimRandomBudgetPerceptron': nNumRight_1stf = %d, nNumWrong_1stf = %d, nNumRight_2ndf = %d, nNumWrong_2ndf = %d",
										nNumRight_1stf,nNumWrong_1stf,nNumRight_2ndf,nNumWrong_2ndf);
fprintf(fout,"\n\nDuring 'WeiEstimRandomBudgetPerceptron': nNumRight_1stf = %d, nNumWrong_1stf = %d, nNumRight_2ndf = %d, nNumWrong_2ndf = %d",
										nNumRight_1stf,nNumWrong_1stf,nNumRight_2ndf,nNumWrong_2ndf);

printf("\nnNumVec_1stf == nNumVec_2ndf = %d",nNumVec_2ndf);
} // if (nNumVec_1stf == nNumVec_2ndf) 
else 
{
printf("\n\nAn error in 'WeiEstimRandomBudgetPerceptron': nNumVec_1stf = %d != nNumVec_2ndf = %d",nNumVec_1stf,nNumVec_2ndf);
getchar(); exit(1);
} // else

delete [] fVec_1stf;
delete [] fVec_2ndf;

return 1;
} //int	WeiEstimRandomBudgetPerceptron(

float fProbab(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[])
{
float fSclarProd(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[]);

int 
	i1;

float
	fExpf,
	fScaProf,
	fProbf;

fScaProf = fSclarProd(
					nDimf,
					fArr1f,
					fArr2f);

fExpf = exp(fScaProf);

if (fExpf == HUGE_VAL)
{
printf("\n\nAn error in 'fProbab': fExpf == HUGE_VAL, fScaProf = %E\n",fScaProf);

	for (i1  = 0; i1 < nDimf; i1++)
		printf("\nfArr1f[%d] = %E, fArr2f[%d] = %E",i1,fArr1f[i1],i1,fArr2f[i1]);

getchar(); exit(1);

} //if (fExpf == HUGE_VAL)

if (fExpf == 0.0)
{
printf("\n\nAn error in 'fProbab': fExpf == 0.0, fScaProf = %E",fScaProf);
getchar(); exit(1);

} //if (fExpf == 0.0)

fProbf = fExpf/(1.0 + fExpf);

return fProbf;
} // float fProbab(

float fSclarProd(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[])
{
int
	i1;

float
	fScalarProdf = 0.0;

for (i1 = 0; i1 < nDimf; i1++)
{

fScalarProdf += fArr1f[i1]*fArr2f[i1];
} // for (i1 = 0; i1 < i1++)

return fScalarProdf;
} //float fSclarProd(

float Effici_EstimLogisReg(
		const int nDimf,
		const int nNumVecf,

		const int nIndic_1st_2ndf,

		const float fVecArrf[],
		const float fWeif[])
{
float fProbab(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[]);

int
	nNumRightf = 0,
	nNumWrongf = 0,
	
	iVecf,
	iFeaf;

float
	fProbf,
	fScaProf =0.0;

float *fVecf = new float[nDimf];

if (nIndic_1st_2ndf == 1)
{
	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVecf[iFeaf] = fVecArrf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		
	fProbf = fProbab(
					nDimf,
					fWeif,
					fVecf);

		if (fProbf > 0.5)
			nNumRightf += 1;
		else
			nNumWrongf  += 1;
/*
	printf("\n\nnIndic_1st_2ndf = %d, iVecf = %d, fProbf = %E",nIndic_1st_2ndf,iVecf,fProbf);
	printf("\nnNumRightf = %d",nNumRightf);

	fprintf(fout,"\n\nnIndic_1st_2ndf = %d, iVecf = %d, fProbf = %E",nIndic_1st_2ndf,iVecf,fProbf);
	fprintf(fout,"\nnNumRightf = %d",nNumRightf);
*/
	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

} // if (nIndic_1st_2ndf == 1)
else if (nIndic_1st_2ndf == -1)
{
	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVecf[iFeaf] = fVecArrf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		
	fProbf = fProbab(
					nDimf,
					fWeif,
					fVecf);

		if (fProbf > 0.5)
			nNumWrongf  += 1;
		else
			nNumRightf += 1;
/*
	printf("\n\nnIndic_1st_2ndf = %d, iVecf = %d, fProbf = %E",nIndic_1st_2ndf,iVecf,fProbf);
	printf("\nnNumRightf = %d",nNumRightf);

	fprintf(fout,"\n\nnIndic_1st_2ndf = %d, iVecf = %d, fProbf = %E",nIndic_1st_2ndf,iVecf,fProbf);
	fprintf(fout,"\nnNumRightf = %d",nNumRightf);
*/
	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

} // if (nIndic_1st_2ndf == -1)
else
{
printf("\n\nAn error in 'Effici_EstimLogisReg': nIndic_1st_2ndf = %d",nIndic_1st_2ndf);
getchar(); exit(1);
} // else

delete [] fVecf;

return (float)(nNumRightf)/(float)(nNumVecf);
} // float	Effici_EstimLogisReg(...

int	BiasEstimLogisRegr(
		const int nDimf,
		
		const float fepsf,

		const float fPerc_Of2ndMinf,

		const int nNumVec_1stf,
		const int nNumVec_2ndf,

		const float fBiasMinf, 
		const float fBiasMaxf, 
		const float fBiasDelf, 

		const int nNumBiasStepsf,

		const float fVecTrain_1stf[],
		const float fVecTrain_2ndf[],
		const float fWeif[],

		float &fBiasOf2ndMinf,
		float &fEfficiOf2ndMin_1stf,
		float &fEfficiOf2ndMin_2ndf,

		float &fRatioMaxf,
		float &fBiasRatioMaxf,
		float &fEfficiRatioMax_1stf,
		float &fEfficiRatioMax_2ndf)
{
float fEffici_EstimLogisReg_With_Bias(
		const int nDimf,
		const int nNumVecf,

		const int nIndic_1st_2ndf,

		const float fBiasf,

		const float fVecArrf[],
		const float fWeif[]);

int
	nIndicExitOf2ndMinf = 0, //no exit
	//nIndicExitRatiof = 0, //no exit
	nIndic_1st_2ndf,
	iBiasf;

float
	fBiasf,
	fRatioCurf,

	fEffici_1stPrevf,
	fEffici_2ndPrevf,
	fEffici_1stf,
	fEffici_2ndf;

fRatioMaxf = -1.0;

for (iBiasf = 0; iBiasf < nNumBiasStepsf; iBiasf++)
{
fBiasf = fBiasMinf + iBiasf*fBiasDelf;

nIndic_1st_2ndf = 1;
fEffici_1stf = fEffici_EstimLogisReg_With_Bias(
										nDimf,
										nNumVec_1stf,

										nIndic_1st_2ndf,

										fBiasf,

										 fVecTrain_1stf,
										 fWeif);

	if (fEffici_1stf < -eps || fEffici_1stf > 1.0) 
	{
	printf("\n\nAn error in 'fBiasOf2ndMinfEstimLogisRegr': fEffici_1stf = %E < -eps || fEffici_1stf > 1.0",fEffici_1stf);
	getchar(); exit(1);
	} // if (fEffici_1stf < -eps || fEffici_1stf > 1.0) 

nIndic_1st_2ndf = -1;
fEffici_2ndf = fEffici_EstimLogisReg_With_Bias(
										nDimf,
										nNumVec_2ndf,

										nIndic_1st_2ndf,

										fBiasf,

										 fVecTrain_2ndf,
										 fWeif);

	if (fEffici_2ndf < -eps || fEffici_2ndf > 1.0) 
	{
	printf("\n\nAn error in 'fBiasOf2ndMinfEstimLogisRegr': fEffici_2ndf = %E < -eps || fEffici_2ndf > 1.0",fEffici_2ndf);
	getchar(); exit(1);
	} // if (fEffici_2ndf < -eps || fEffici_2ndf > 1.0) 

	if (iBiasf == 0 && fEffici_2ndf < fPerc_Of2ndMinf)
	{
	printf("\n\niBiasf == 0 && fEffici_2ndf = %E < fPerc_Of2ndMinf, fEffici_1stf = %E",fEffici_2ndf,fEffici_1stf);

	printf("\nPlease decrease 'fBiasOf2ndMinfMin'");

	printf("\n\nPlease press any key to continue:"); getchar();

	break;
	} //if (iBiasf == 0 && fEffici_2ndf < 1.0)

	if (fEffici_2ndf < fPerc_Of2ndMinf && nIndicExitOf2ndMinf == 0)
	{
	printf("\n\nAn exit from 'BiasEstimLogisRegr': iBiasf = %d, fEffici_2ndf = %E, fEffici_1stf = %E",iBiasf ,fEffici_2ndf,fEffici_1stf);

	fBiasOf2ndMinf = fBiasf - fBiasDelf;

	fEfficiOf2ndMin_1stf = fEffici_1stPrevf;
	fEfficiOf2ndMin_2ndf = fEffici_2ndPrevf;

	nIndicExitOf2ndMinf = 1;
	//break;
	} //if (fEffici_2ndf < fPerc_Of2ndMinf && nIndicExitf == 0))

	if (fEffici_2ndf + fepsf < 1.0)
		fRatioCurf = fEffici_1stf/(1.0 - fEffici_2ndf);
	else
		fRatioCurf = (fEffici_1stf*nNumVec_1stf)/2.0; //can be adjusted


	if (fRatioCurf > fRatioMaxf)
	{
	fRatioMaxf = fRatioCurf;

	fBiasRatioMaxf = fBiasf;
	fEfficiRatioMax_1stf = fEffici_1stf;

	fEfficiRatioMax_2ndf = fEffici_2ndf;
	} //if (fRatioCurf > fBiasRatioMaxf)

	if (iBiasf > 0)
	{
	fEffici_1stPrevf = fEffici_1stf;
	fEffici_2ndPrevf = fEffici_2ndf;
	} //if (iBiasf > 0)
	else
	{
	fEffici_1stPrevf = -1.0;
	fEffici_2ndPrevf = -1.0;
	} // else

} //for (iBiasf = 0; iBiasf < nNumBiasStepsf; iBiasf++)

return 1;
} // int	BiasEstimLogisRegr(...

float fEffici_EstimLogisReg_With_Bias(
		const int nDimf,
		const int nNumVecf,

		const int nIndic_1st_2ndf,

		const float fBiasf,

		const float fVecArrf[],
		const float fWeif[])
{

float fProbabWithBias(
				const int nDimf,
				const float fBiasf,

				 const float fArr1f[],
				 const float fArr2f[]);

int
	nNumRightf = 0,
	nNumWrongf = 0,
	
	iVecf,
	iFeaf;

float
	fProbf,
	fScaProf =0.0;

float *fVecf = new float[nDimf];

if (nIndic_1st_2ndf == 1)
{
	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVecf[iFeaf] = fVecArrf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		
	fProbf = fProbabWithBias(
					nDimf,
					fBiasf,
					fWeif,
					fVecf);


		if (fProbf > 0.5)
			nNumRightf += 1;
		else
			nNumWrongf  += 1;
/*
	printf("\n\nnIndic_1st_2ndf = %d, iVecf = %d, fProbf = %E",nIndic_1st_2ndf,iVecf,fProbf);
	printf("\nnNumRightf = %d",nNumRightf);

	fprintf(fout,"\n\nnIndic_1st_2ndf = %d, iVecf = %d, fProbf = %E",nIndic_1st_2ndf,iVecf,fProbf);
	fprintf(fout,"\nnNumRightf = %d",nNumRightf);
*/
	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

} // if (nIndic_1st_2ndf == 1)
else if (nIndic_1st_2ndf == -1)
{
	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
		for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		{
		fVecf[iFeaf] = fVecArrf[iFeaf + iVecf*nDimf];
		} //for (iFeaf = 0; iFeaf < nDimf;  iFeaf++)
		
	fProbf = fProbabWithBias(
					nDimf,
					fBiasf,
					fWeif,
					fVecf);

		if (fProbf > 0.5)
			nNumWrongf  += 1;
		else
			nNumRightf += 1;
/*
	printf("\n\nnIndic_1st_2ndf = %d, iVecf = %d, fProbf = %E",nIndic_1st_2ndf,iVecf,fProbf);
	printf("\nnNumRightf = %d",nNumRightf);

	fprintf(fout,"\n\nnIndic_1st_2ndf = %d, iVecf = %d, fProbf = %E",nIndic_1st_2ndf,iVecf,fProbf);
	fprintf(fout,"\nnNumRightf = %d",nNumRightf);
*/
	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

} // if (nIndic_1st_2ndf == -1)
else
{
printf("\n\nAn error in 'Effici_EstimLogisReg': nIndic_1st_2ndf = %d",nIndic_1st_2ndf);
getchar(); exit(1);
} // else

delete [] fVecf;

return (float)(nNumRightf)/(float)(nNumVecf);

} //float fEffici_EstimLogisReg_With_Bias(

float fProbabWithBias(
		const int nDimf,
		const float fBiasf,

		 const float fArr1f[],
		 const float fArr2f[])
{
float fSclarProd(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[]);


float
	fExpf,
	fScaProf,
	fScaProBiasedf,
	fProbf;

fScaProf = fSclarProd(
					nDimf,
					fArr1f,
					fArr2f);

fScaProBiasedf = fScaProf + fBiasf;

fExpf = exp(fScaProBiasedf);

if (fExpf == HUGE_VAL)
{
printf("\n\nAn error in 'fProbab': fExpf == HUGE_VAL, fScaProf = %E",fScaProf);
getchar(); exit(1);

} //if (fExpf == HUGE_VAL)

if (fExpf == 0.0)
{
printf("\n\nAn error in 'fProbab': fExpf == 0.0, fScaProf = %E",fScaProf);
getchar(); exit(1);

} //if (fExpf == 0.0)

fProbf = fExpf/(1.0 + fExpf);

return fProbf;
} // float fProbabWithBias(

int SettingInitArr(int nIndexDimf, int nIndexForActiveArrf[])
{

int
	i1;

for (i1 = 0; i1 < nIndexDimf; i1++)
	nIndexForActiveArrf[i1] = i1;

return 1;
} // int SettingInitArr(int nIndexDimf, int nIndexForActiveArrf[])

int Rand_Selec (
				const int nDimf,
				const int nVecInitf,
				const int nVecSelecf, // < nVecInitf
				const float fFeaAll_InitArrf[],
				
				float fFeaSelecAll_FeasArr[])
{
int
	nRanSelecf, // <= nVecSelecf
	iFeaf,
	iVecf;

for (iVecf = 0; iVecf < nVecSelecf; iVecf++)
{
nRanSelecf = (int)( nVecInitf*(float)( rand() )/(float)(RAND_MAX) );

	if (nRanSelecf == nVecInitf)
	{
	nRanSelecf = nVecInitf - 1;
	} //if (nRanSelecf == nVecInitf)
	else if (nRanSelecf > nVecInitf)
	{
	printf("\n\nAn error in 'Rand_Selec': nRanSelecf = %d > nVecInitf = %d",
							nRanSelecf,nVecInitf);
	fprintf(fout,"\n\nAn error in 'Rand_Selec': nRanSelecf = %d > nVecInitf = %d",
							nRanSelecf,nVecInitf);

	getchar();	exit(1);
	} // else if (nRanSelecf > nVecInitf)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
	fFeaSelecAll_FeasArr[iFeaf + (iVecf*nDimf)] = fFeaAll_InitArrf[iFeaf + (nRanSelecf*nDimf)];
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

} // for (iVecf = 0; iVecf < nVecSelecf; iVecf++)

return 1;
} //int Rand_Selec (...

////////////////////////////////////////////////////////////////////
int Float_Belongs_To_Arr_Or_Not(
											const float epsf,
											const float fLargef,

											const int nDimf,
											const float fTestf,
											
											int &nNumBelongsf, //'1' --> belongs and '-1' otherwise

											float fArrf[])
{
int
	i1;

float
	fDiff1f,
	fDiff2f;

//fDiff1f = fLargef - fTestf;
fDiff1f = -fLargef - fTestf;

if (fDiff1f > -epsf && fDiff1f < epsf)
{
printf("\n\nAn error in 'Float_Belongs_To_Arr_Or_Not': fTestf = %E",fTestf);
fprintf(fout,"\n\nAn error in 'Float_Belongs_To_Arr_Or_Not': fTestf = %E",fTestf);

getchar();	exit(1);
} // if (fDiff1f > -epsf && fDiff1f < epsf)

nNumBelongsf = 0; //impossible

for (i1 = 0; i1 < nDimf; i1++)
{
fDiff1f = fArrf[i1] - fTestf;

	if (fDiff1f > -epsf && fDiff1f < epsf)
	{
	nNumBelongsf = 1; //belongs
	break;
	} // if (fDiff1f > -epsf && fDiff1f < epsf)
	
//fDiff2f = fArrf[i1] - fLargef;
fDiff2f = fArrf[i1] + fLargef;
	if (fDiff2f > -epsf && fDiff2f < epsf)
	{
	nNumBelongsf = -1; //does not belong

	fArrf[i1] = fTestf;
	break;
	} // if (fDiff2f > -epsf && fDiff2f < epsf)

} // for (i1 = 0; i1 < nDimf; i1++)

if (nNumBelongsf == 0)
{
printf("\n\nAn error in 'Float_Belongs_To_Arr_Or_Not': nNumBelongsf == 0, fTestf = %E",fTestf);
fprintf(fout,"\n\nAn error in 'Float_Belongs_To_Arr_Or_Not': nNumBelongsf == 0, fTestf = %E",fTestf);

getchar();	exit(1);
} // if (nNumBelongsf == 0)

return 1;
} // int Float_Belongs_To_Arr_Or_Not(...

int Int_Belongs_To_Arr_Or_Not(
								const int nDimf,
								const int nTestf,
								const int nArrf[],

								int &nNumBelongsf, //'1' --> belongs and '-1' otherwise
								
								int nPosArrf[]) //[nDimf]
{
int
	i1,

	nDiff1f;

nNumBelongsf = 0; //impossible
for (i1 = 0; i1 < nDimf; i1++)
	nPosArrf[i1] = -1;

for (i1 = 0; i1 < nDimf; i1++)
{
nDiff1f = nArrf[i1] - nTestf;

	if (nDiff1f == 0)
	{
	nNumBelongsf += 1; //belongs
	nPosArrf[nNumBelongsf - 1] = i1;
	//return 1;
	} // if (nDiff1f == 0)

} // for (i1 = 0; i1 < nDimf; i1++)

return 1;
} // int Int_Belongs_To_Arr_Or_Not(...

int compa_int (const void * na, const void * nb)
{
return ( *(int*)na - *(int*)nb );
} //int compa_int (const void * na, const void * nb)

int fDiffOfLastMeans(
				const int nNumRecordsLastMaxf,
				const float fepsf,
				const int nDimForAver1f,
				const int nDimForAver2f,
				
				const float fArrf[],
				float &fDiff)
{
int
	i1,
	nNum1f = 0,
	nNum2f = 0;

float
	fMean1f = 0.0,
	fMean2f = 0.0;

if (nDimForAver1f < 1 || nDimForAver1f > nNumRecordsLastMaxf)
{
printf("\n\nAn error in 'fDiffOfLastMeans': nDimForAver1f = %d < 1 || nDimForAver1f > nNumRecordsLastMaxf = %d",
						nDimForAver1f,nNumRecordsLastMaxf);

fprintf(fout,"\n\nAn error in 'fDiffOfLastMeans': nDimForAver1f = %d < 1 || nDimForAver1f > nNumRecordsLastMaxf = %d",
						nDimForAver1f,nNumRecordsLastMaxf);

getchar(); exit(1);
} //if (nDimForAver1f < 1 || nDimForAver1f > nNumRecordsLastMaxf)

if (nDimForAver2f < 1 || nDimForAver2f > nNumRecordsLastMaxf || nDimForAver2f <= nDimForAver1f )
{
printf("\n\nAn error in 'fDiffOfLastMeans': nDimForAver2f = %d < 1 || nDimForAver2f > nNumRecordsLastMaxf = %d || nDimForAver2f <= nDimForAver1f = %d",
						nDimForAver2f,nNumRecordsLastMaxf,nDimForAver1f);

fprintf(fout,"\n\nAn error in 'fDiffOfLastMeans': nDimForAver2f = %d < 1 || nDimForAver2f > nNumRecordsLastMaxf = %d || nDimForAver2f <= nDimForAver1f = %d",
						nDimForAver2f,nNumRecordsLastMaxf,nDimForAver1f);

getchar(); exit(1);
} //if (nDimForAver2f < 1 || nDimForAver2f > nNumRecordsLastMaxf)

for (i1 = 0; i1 < nDimForAver1f; i1++)
{
	if (fArrf[i1] > -1.0 - fepsf && fArrf[i1] < -1.0 + fepsf)
		break;

nNum1f += 1;
fMean1f += fArrf[i1];
} // for (i1 = 0; i1 < nDimForAver1f; i1++)

if (nNum1f == 0)
{
//fMean1f = 0.0;
printf("\n\nAn error in 'fDiffOfLastMeans': nNum1f == 0");
fprintf(fout,"\n\nAn error in 'fDiffOfLastMeans': nNum1f == 0");
getchar(); exit(1);
} // if (nNum1f == 0)
else
	fMean1f = fMean1f/nNum1f;

for (i1 = 0; i1 < nDimForAver2f; i1++)
{
	if (fArrf[i1] > -1.0 - fepsf && fArrf[i1] < -1.0 + fepsf)
		break;

nNum2f += 1;
fMean2f += fArrf[i1];
} // for (i1 = 0; i1 < nDimForAver2f; i1++)

if (nNum2f == 0)
{
//fMean2f = 0.0;
printf("\n\nAn error in 'fDiffOfLastMeans': nNum2f == 0");
fprintf(fout,"\n\nAn error in 'fDiffOfLastMeans': nNum2f == 0");
getchar(); exit(1);
} // if (nNum2f == 0)
else
	fMean2f = fMean2f/nNum2f;

fDiff = fMean1f - fMean2f;
return 1;
} // int fDiffOfMeans(

int fMeanOfIntArr( const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,
					const int nArrf[], //	ObjDataSymbArrAllf.nNumLongFailureAskIntervArrf, //nNumLongEntriesCurf, //const int nCurf,
						
					float &fMeanf)
{
int 
	nNumRealMembf = 0,
	i1;

fMeanf = 0.0;
for (i1 = 0; i1 < nDimf; i1++)
{
	if (nArrf[i1] == -1)
		break;

nNumRealMembf += 1;
/*
	if (nArrf[i1] < 1)
	{
	printf("\n\nAn error in 'fMeanOfIntArr': nArrf[%d] = %d < 1, ObjDataSymbArrAllf.get_nNumRecordsTotCurf() = %d",
		i1,nArrf[i1],ObjDataSymbArrAllf.get_nNumRecordsTotCurf() );
	fprintf(fout,"\n\nAn error in 'fMeanOfIntArr': nArrf[%d] = %d < 1, ObjDataSymbArrAllf.get_nNumRecordsTotCurf() = %d",
		i1,nArrf[i1],ObjDataSymbArrAllf.get_nNumRecordsTotCurf() );

	getchar(); exit(1);
	} //if (nArrf[i1] < 1)
*/
fMeanf += (float)(nArrf[i1]);
} //for (i1 = 0; i1 < nDimf; i1++)

if (i1 == 0)
{
fMeanf = -1.0;
return 1;
} //if (i1 == 0)
else
{
fMeanf = fMeanf/nNumRealMembf;
} //else

return 1;
} //int fMeanOfIntArr( const int nDimf,	//nDimSelecInterv_Glob, //const int nDimf,

int Normalizing_Every_Fea_To_ARange(
				const float fLargef,
				const float fepsf,

				const int nDimf,

				const int nVec1stf,
				const int nVec2ndf, 

				float fFeaMinArrf[],
				float fFeaMaxArrf[],
				
				float fFeaArr1stf[], //to be normalized
				float fFeaArr2ndf[]) //to be normalized
{

int
	//nResf,
	iFeaf,
	iVecf;

float
	fFeaCurf,
	fDiffFeaMaxMinCurf;

for(iFeaf = 0; iFeaf < nDimf; iFeaf++)
{
fFeaMinArrf[iFeaf] = fLargef;
fFeaMaxArrf[iFeaf] = -fLargef;

	for (iVecf = 0; iVecf < nVec1stf; iVecf++)
	{
	fFeaCurf = fFeaArr1stf[iFeaf + iVecf*nDimf];

		if (fFeaCurf < fFeaMinArrf[iFeaf])
			fFeaMinArrf[iFeaf] = fFeaCurf;

		if (fFeaCurf > fFeaMaxArrf[iFeaf])
			fFeaMaxArrf[iFeaf] = fFeaCurf;

	} // for (iVecf = 0; iVecf < nVec1stf; iVecf++)

	for (iVecf = 0; iVecf < nVec2ndf; iVecf++)
	{
	fFeaCurf = fFeaArr2ndf[iFeaf + iVecf*nDimf];

		if (fFeaCurf < fFeaMinArrf[iFeaf])
			fFeaMinArrf[iFeaf] = fFeaCurf;

		if (fFeaCurf > fFeaMaxArrf[iFeaf])
			fFeaMaxArrf[iFeaf] = fFeaCurf;

	} // for (iVecf = 0; iVecf < nVec2ndf; iVecf++)

} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

//Normalization

for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
{
fDiffFeaMaxMinCurf = fFeaMaxArrf[iFeaf] - fFeaMinArrf[iFeaf];

	if (fDiffFeaMaxMinCurf > fepsf)
	{
		for (iVecf = 0; iVecf < nVec1stf; iVecf++)
		{
		fFeaCurf = fFeaArr1stf[iFeaf + iVecf*nDimf];

		fFeaArr1stf[iFeaf + iVecf*nDimf] = (fFeaCurf - fFeaMinArrf[iFeaf])/fDiffFeaMaxMinCurf;

			if (fFeaArr1stf[iFeaf + iVecf*nDimf] < -fepsf || fFeaArr1stf[iFeaf + iVecf*nDimf] > 1.0 + fepsf)
			{
			printf("\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fFeaArr1stf[iFeaf + iVecf*nDimf] = %E < -fepsf ||... , iFeaf = %d, iVecf = %d",
				fFeaArr1stf[iFeaf + iVecf*nDimf], iFeaf, iVecf);
			
			fprintf(fout,"\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fFeaArr1stf[iFeaf + iVecf*nDimf] = %E < -fepsf ||... , iFeaf = %d, iVecf = %d",
				fFeaArr1stf[iFeaf + iVecf*nDimf], iFeaf, iVecf);

			getchar(); exit(1);
			} //if (fFeaArr1stf[iFeaf + iVecf*nDimf] < -fepsf || fFeaArr1stf[iFeaf + iVecf*nDimf] > 1.0 + fepsf)

		} // for (iVecf = 0; iVecf < nVec1stf; iVecf++)

		for (iVecf = 0; iVecf < nVec2ndf; iVecf++)
		{
		fFeaCurf = fFeaArr2ndf[iFeaf + iVecf*nDimf];

		fFeaArr2ndf[iFeaf + iVecf*nDimf] = (fFeaCurf - fFeaMinArrf[iFeaf])/fDiffFeaMaxMinCurf;

			if (fFeaArr2ndf[iFeaf + iVecf*nDimf] < -fepsf || fFeaArr2ndf[iFeaf + iVecf*nDimf] > 1.0 + fepsf)
			{
			printf("\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fFeaArr2ndf[iFeaf + iVecf*nDimf] = %E < -fepsf ||... , iFeaf = %d, iVecf = %d",
				fFeaArr2ndf[iFeaf + iVecf*nDimf], iFeaf, iVecf);
			
			fprintf(fout,"\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fFeaArr2ndf[iFeaf + iVecf*nDimf] = %E < -fepsf ||... , iFeaf = %d, iVecf = %d",
				fFeaArr2ndf[iFeaf + iVecf*nDimf], iFeaf, iVecf);

			getchar(); exit(1);
			} //if (fFeaArr2ndf[iFeaf + iVecf*nDimf] < -fepsf || fFeaArr2ndf[iFeaf + iVecf*nDimf] > 1.0 + fepsf)
		
		} // for (iVecf = 0; iVecf < nVec2ndf; iVecf++)

	} // if (fDiffFeaMaxMinCurf > fepsf)
	else if (fDiffFeaMaxMinCurf > -fepsf && fDiffFeaMaxMinCurf < fepsf)
	{
	fFeaArr1stf[iFeaf + iVecf*nDimf] = 0.0;
	fFeaArr2ndf[iFeaf + iVecf*nDimf] = 0.0;

	} //else if (fDiffFeaMaxMinCurf > -fepsf && fDiffFeaMaxMinCurf < fepsf)
	else
	{
	printf("\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fDiffFeaMaxMinCurf = %E, iFeaf = %d",
		fDiffFeaMaxMinCurf,iFeaf);

	fprintf(fout,"\n\nAn error in 'Normalizing_Every_Fea_To_ARange': fDiffFeaMaxMinCurf = %E, iFeaf = %d",
		fDiffFeaMaxMinCurf,iFeaf);

	getchar(); exit(1);
	} //else

} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

return 1;
} //int Normalizing_Every_Fea_To_ARange (...
//////////////////////////////////////////////////////////

int Normalizing_Every_Fea_To_Mean_0_And_StDev_1(
	const float fLargef,
	const float fepsf,

	const int nDimf,

	const int nVecTrainf,
	const int nVecTestf,
/////////////////////////////////////////////
	float fFeaMin_TrainArrf[], //[nDimf]
	float fFeaMax_TrainArrf[], //[nDimf]

	float fFea_TrainArrf[], //to be normalized
	float fFea_TestArrf[]) //to be normalized using the train Mean and StDev
{

	int
		iFeaf,
		iVecf;

	float
		fMeanFor_OneFeaf,
		fVarianceCurf,
		fStDevf,

		fFeaCurf,
		fDiff_Fea_And_MeanCurf, 
		fDiffFeaMaxMinCurf;
//////////////////////////////////////////
	if (nVecTrainf <= 0 || nVecTestf <= 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Normalizing_Every_Fea_To_Mean_0_And_StDev_1': nVecTrainf = %d, nVecTestf = %d", nVecTrainf, nVecTestf);
		fprintf(fout, "\n\nAn error in 'Normalizing_Every_Fea_To_Mean_0_And_StDev_1': nVecTrainf = %d, nVecTestf = %d", nVecTrainf, nVecTestf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nVecTrainf <= 0)
/////////////////////////////////////////////////

	float *fMean_All_FeasArrf = new float[nDimf];
	if (fMean_All_FeasArrf == NULL)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\nAn error in 'Normalizing_Every_Fea_To_Mean_0_And_StDev_1': fMean_All_FeasArrf == NULL");
		fprintf(fout, "\n\nAn error in 'Normalizing_Every_Fea_To_Mean_0_And_StDev_1': fMean_All_FeasArrf == NULL");
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fMean_All_FeasArrf == NULL)

	float *fStDev_All_FeasArrf = new float[nDimf];
	if (fStDev_All_FeasArrf == NULL)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\nAn error in 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': fStDev_All_FeasArrf == NULL");
		fprintf(fout, "\n\nAn error in 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': fStDev_All_FeasArrf == NULL");

		delete[] fMean_All_FeasArrf;
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fStDev_All_FeasArrf == NULL)

//////////////////////////////////////////////////
//Mean
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeaMin_TrainArrf[iFeaf] = fLargef;
		fFeaMax_TrainArrf[iFeaf] = -fLargef;

		fMeanFor_OneFeaf = 0.0;
		for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
		{
			fFeaCurf = fFea_TrainArrf[iFeaf + iVecf * nDimf];

			if (fFeaCurf < fFeaMin_TrainArrf[iFeaf])
				fFeaMin_TrainArrf[iFeaf] = fFeaCurf;

			if (fFeaCurf > fFeaMax_TrainArrf[iFeaf])
				fFeaMax_TrainArrf[iFeaf] = fFeaCurf;

			fMeanFor_OneFeaf += fFeaCurf;
		} // for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

		fMeanFor_OneFeaf = fMeanFor_OneFeaf / nVecTrainf;
		fMean_All_FeasArrf[iFeaf] = fMeanFor_OneFeaf;
	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)
///////////////////////////////////////////////////////////
//StDev 

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fDiff_Fea_And_MeanCurf = 0.0;
		fVarianceCurf = 0.0;

		for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
		{
			fFeaCurf = fFea_TrainArrf[iFeaf + iVecf * nDimf];
			fDiff_Fea_And_MeanCurf = fFeaCurf - fMean_All_FeasArrf[iFeaf];

			fVarianceCurf += fDiff_Fea_And_MeanCurf*fDiff_Fea_And_MeanCurf;
		} // for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

		fVarianceCurf = fVarianceCurf / nVecTrainf;

		fStDev_All_FeasArrf[iFeaf] = sqrt(fVarianceCurf);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf( "\n\n 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': fMean_All_FeasArrf[%d] = %E, fStDev_All_FeasArrf[%d] = %E",
			iFeaf, fMean_All_FeasArrf[iFeaf], iFeaf, fStDev_All_FeasArrf[iFeaf]);

		printf( "\n fFeaMin_TrainArrf[%d] = %E, fFeaMax_TrainArrf[%d] = %E",
			iFeaf, fFeaMin_TrainArrf[iFeaf], iFeaf, fFeaMax_TrainArrf[iFeaf]);

		fprintf(fout, "\n\n 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': fMean_All_FeasArrf[%d] = %E, fStDev_All_FeasArrf[%d] = %E",
			iFeaf, fMean_All_FeasArrf[iFeaf], iFeaf, fStDev_All_FeasArrf[iFeaf]);

		fprintf(fout, "\n fFeaMin_TrainArrf[%d] = %E, fFeaMax_TrainArrf[%d] = %E",
			iFeaf, fFeaMin_TrainArrf[iFeaf], iFeaf, fFeaMax_TrainArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (fStDev_All_FeasArrf[iFeaf] <= eps)
		{
			printf( "\n\n A warning in 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': too small fStDev_All_FeasArrf[%d] = %E",
				iFeaf, fStDev_All_FeasArrf[iFeaf]);
			fprintf(fout, "\n\n A warning in 'Normalizing_Every_Fea_To_StDev_0_And_StDev_1': too small fStDev_All_FeasArrf[%d] = %E",
				iFeaf, fStDev_All_FeasArrf[iFeaf]);
		} //if (fStDev_All_FeasArrf[iFeaf] <= eps)
	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

//////////////////////////////////////////////////
	//Normalization for train vecs
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
		{
			fFeaCurf = fFea_TrainArrf[iFeaf + iVecf * nDimf];

			if (fStDev_All_FeasArrf[iFeaf] > eps)
			{
				fFea_TrainArrf[iFeaf + iVecf * nDimf] = (fFeaCurf - fMean_All_FeasArrf[iFeaf]) / fStDev_All_FeasArrf[iFeaf];
			} //if (fStDev_All_FeasArrf[iFeaf] > eps)
			else if (fStDev_All_FeasArrf[iFeaf] <= eps)
			{
				fFea_TrainArrf[iFeaf + iVecf * nDimf] = 0.0;
			} //else if (fStDev_All_FeasArrf[iFeaf] <= eps)

		} // for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

//////////////////////////////////////////////////
	//Normalization of test vecs
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		for (iVecf = 0; iVecf < nVecTestf; iVecf++)
		{
			fFeaCurf = fFea_TestArrf[iFeaf + iVecf * nDimf];

			if (fStDev_All_FeasArrf[iFeaf] > eps)
			{
				fFea_TestArrf[iFeaf + iVecf * nDimf] = (fFeaCurf - fMean_All_FeasArrf[iFeaf]) / fStDev_All_FeasArrf[iFeaf];
			} //if (fStDev_All_FeasArrf[iFeaf] > eps)
			else if (fStDev_All_FeasArrf[iFeaf] <= eps)
			{
				fFea_TestArrf[iFeaf + iVecf * nDimf] = 0.0;
			} //else if (fStDev_All_FeasArrf[iFeaf] <= eps)

		} // for (iVecf = 0; iVecf < nVecTestf; iVecf++)

	} //for(iFeaf = 0; iFeaf < nDimf; iFeaf++)

////////////////////////////////////////////////
	delete[] fMean_All_FeasArrf;
	delete[] fStDev_All_FeasArrf;

	return SUCCESSFUL_RETURN;
} //int Normalizing_Every_Fea_To_Mean_0_And_StDev_1 (...

//////////////////////////////////////////////////////////
int FindingBestSeparByRatioOfOneFea(
					const float fLargef,
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fPrecisionOf_Golden_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, 

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					float &fEfficBestSeparOfOneFeaf,

					float &fPosSeparOfOneFeaBestf,

					int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestf,
					float &fNumArr2ndSeparBestf,

					float &fDiffSeparOfArr1stAndArr2ndBestf,
					float &fRatioOfSeparOfArr1stAndArr2ndBestf) 					
{


float EfficiencyOfSeparByRatio(
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					const float fPosSeparf,

					int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparf,
					float &fNumArr2ndSeparf,

					float &fDiffSeparOfArr1stAndArr2ndf,
					float &fRatioOfSeparOfArr1stAndArr2ndf); //fRatioOfSeparOfArr1stAndArr2ndf

int 
	i1,
	nSeparDiffDirectionf,
	nSeparRatioDirectionf;
//////////////////////

float 

	fNumArr1stSeparf,
	fNumArr2ndSeparf,
	
	fBorderBelCurf = fBorderBelf,
	fBorderAboCurf = fBorderAbof,

	fDiffSeparOfArr1stAndArr2ndf,
	fRatioOfSeparOfArr1stAndArr2ndf,

//fPosSeparf,
/////////////////
	fX1f,
	fX2f,

	x1,
	x2;
	//x3;


//fC_Const,fR_Const
fEfficBestSeparOfOneFeaf = fLargef;

x1 = (fC_Const*fBorderBelCurf) + (fR_Const*fBorderAboCurf);

fX1f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x1, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

if (fX1f < fEfficBestSeparOfOneFeaf)
{
fEfficBestSeparOfOneFeaf = fX1f;

fPosSeparOfOneFeaBestf = x1;

nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

fNumArr1stSeparBestf = fNumArr1stSeparf;
fNumArr2ndSeparBestf = fNumArr2ndSeparf;

fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

} //if (fX1f < fEfficBestSeparOfOneFeaf)

x2 = (fR_Const*fBorderBelCurf) + (fC_Const*fBorderAboCurf);

fX2f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x2, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

//fprintf(fout,"\n\n1: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fC_Const = %E",
//	   x1, fX1f, x2, fX2f,fC_Const); fflush(fout);

//printf("\n\n1: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fC_Const = %E",
//	   x1, fX1f, x2, fX2f,fC_Const); getchar();

if (fX2f < fEfficBestSeparOfOneFeaf)
{
fEfficBestSeparOfOneFeaf = fX2f;

fPosSeparOfOneFeaBestf = x2;

nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

fNumArr1stSeparBestf = fNumArr1stSeparf;
fNumArr2ndSeparBestf = fNumArr2ndSeparf;

fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

} //if (fX2f < fEfficBestSeparOfOneFeaf)

for (i1 = 0; i1 < nNumIterMaxForFindingBestSeparByRatioOfOneFeaf; i1++)
{
	if (fX1f < fX2f)
	{
	fBorderAboCurf = x2;

	x2 = x1;

	fX2f = fX1f;

	x1 = (fC_Const*fBorderBelCurf) + (fR_Const*fBorderAboCurf);
	
	fX1f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x1, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

//	fprintf(fout,"\n\n2: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); fflush(fout);

//	printf("\n\n2: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); getchar();


		if (fX1f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX1f;

		fPosSeparOfOneFeaBestf = x1;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

		} //if (fX1f < fEfficBestSeparOfOneFeaf)

	} //if (fX1f < fX2f)
	else
	{
	fBorderBelCurf = x1;

	x1 = x2;

	fX1f = fX2f;
	x2 = (fR_Const*fBorderBelCurf) + (fC_Const*fBorderAboCurf);

	fX2f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x2, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

//	fprintf(fout,"\n\n3: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); fflush(fout);

//	printf("\n\n3: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); getchar();

		if (fX2f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX2f;

		fPosSeparOfOneFeaBestf = x2;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

		} //if (fX2f < fEfficBestSeparOfOneFeaf)

	} //else

	if ( fabs(fBorderAboCurf - fBorderBelCurf) < fPrecisionOf_Golden_Searchf)
	{
	x1 = (fBorderBelCurf + fBorderAboCurf)/2.0;

	fX1f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x1, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

//	fprintf(fout,"\n\n4: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); fflush(fout);

//	printf("\n\n4: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); getchar();

		if (fX1f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX1f;

		fPosSeparOfOneFeaBestf = x1;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

		} //if (fX1f < fEfficBestSeparOfOneFeaf)
/*
	printf("\n\n//////////////////////////////////////////");
	printf("\n\nExit by: fabs(fBorderAboCurf - fBorderBelCurf)= %E  < fPrecisionOf_Golden_Searchf = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
	   fabs(fBorderAboCurf - fBorderBelCurf),fPrecisionOf_Golden_Searchf,fBorderBelCurf,fBorderAboCurf); fflush(fout);

	fprintf(fout,"\n\n//////////////////////////////////////////");

	fprintf(fout,"\n\nExit by: fabs(fBorderAboCurf - fBorderBelCurf)= %E  < fPrecisionOf_Golden_Searchf = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
	   fabs(fBorderAboCurf - fBorderBelCurf),fPrecisionOf_Golden_Searchf,fBorderBelCurf,fBorderAboCurf); fflush(fout);
*/

	return 1;
	} //if ( fabs(fBorderAboCurf - fBorderBelCurf) < fPrecisionOf_Golden_Searchf)

} //for (i1 = 0; i1 < nNumIterMaxForFindingBestSeparByRatioOfOneFeaf; i1++)

printf("\n\nAn error in 'FindingBestSeparByRatioOfOneFea'");
fprintf(fout,"\n\nAn error in 'FindingBestSeparByRatioOfOneFea'");

printf("\n\nx1 = %E, x2 = %E, fNumArr1stSeparf = %E, fNumArr2ndSeparf = %E",
	   x1, x2,fNumArr1stSeparf, fNumArr2ndSeparf);

printf("\n\nnSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf);

getchar();	exit(1);

return 1;
} //int FindingBestSeparByRatioOfOneFea(...


int FindingBestSeparByDiffOfOneFea(
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fPrecisionOf_Golden_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByDiffOfOneFeaf, 

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					float &fEfficBestSeparOfOneFeaf,

					float &fPosSeparOfOneFeaBestf,

					int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestf,
					float &fNumArr2ndSeparBestf,

					float &fDiffSeparOfArr1stAndArr2ndBestf,
					float &fRatioOfSeparOfArr1stAndArr2ndBestf) 					
{


float EfficiencyOfSeparByDiff(
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					const float fPosSeparf,

					int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparf,
					float &fNumArr2ndSeparf,

					float &fDiffSeparOfArr1stAndArr2ndf,
					float &fRatioOfSeparOfArr1stAndArr2ndf); //fRatioOfSeparOfArr1stAndArr2ndf

int 
	i1,
	nSeparDiffDirectionf,
	nSeparRatioDirectionf;
//////////////////////

float 

	fNumArr1stSeparf,
	fNumArr2ndSeparf,
	
	fBorderBelCurf = fBorderBelf,
	fBorderAboCurf = fBorderAbof,

	fDiffSeparOfArr1stAndArr2ndf,
	fRatioOfSeparOfArr1stAndArr2ndf,

//fPosSeparf,
/////////////////
	fX1f,
	fX2f,

	x1,
	x2;
	//x3;


//fC_Const,fR_Const
fEfficBestSeparOfOneFeaf = fLarge;

x1 = (fC_Const*fBorderBelCurf) + (fR_Const*fBorderAboCurf);

fX1f = EfficiencyOfSeparByDiff(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x1, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

if (fX1f < fEfficBestSeparOfOneFeaf)
{
fEfficBestSeparOfOneFeaf = fX1f;

fPosSeparOfOneFeaBestf = x1;

nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

fNumArr1stSeparBestf = fNumArr1stSeparf;
fNumArr2ndSeparBestf = fNumArr2ndSeparf;

fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

} //if (fX1f < fEfficBestSeparOfOneFeaf)

x2 = (fR_Const*fBorderBelCurf) + (fC_Const*fBorderAboCurf);

fX2f = EfficiencyOfSeparByDiff(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x2, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

//fprintf(fout,"\n\n1: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fC_Const = %E",
//	   x1, fX1f, x2, fX2f,fC_Const); fflush(fout);

//printf("\n\n1: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fC_Const = %E",
//	   x1, fX1f, x2, fX2f,fC_Const); getchar();

if (fX2f < fEfficBestSeparOfOneFeaf)
{
fEfficBestSeparOfOneFeaf = fX2f;

fPosSeparOfOneFeaBestf = x2;

nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

fNumArr1stSeparBestf = fNumArr1stSeparf;
fNumArr2ndSeparBestf = fNumArr2ndSeparf;

fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

} //if (fX2f < fEfficBestSeparOfOneFeaf)

for (i1 = 0; i1 < nNumIterMaxForFindingBestSeparByDiffOfOneFeaf; i1++)
{
	if (fX1f < fX2f)
	{
	fBorderAboCurf = x2;

	x2 = x1;

	fX2f = fX1f;

	x1 = (fC_Const*fBorderBelCurf) + (fR_Const*fBorderAboCurf);
	
	fX1f = EfficiencyOfSeparByDiff(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x1, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

//	fprintf(fout,"\n\n2: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); fflush(fout);

//	printf("\n\n2: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); getchar();


		if (fX1f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX1f;

		fPosSeparOfOneFeaBestf = x1;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

		} //if (fX1f < fEfficBestSeparOfOneFeaf)

	} //if (fX1f < fX2f)
	else
	{
	fBorderBelCurf = x1;

	x1 = x2;

	fX1f = fX2f;
	x2 = (fR_Const*fBorderBelCurf) + (fC_Const*fBorderAboCurf);

	fX2f = EfficiencyOfSeparByDiff(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x2, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

//	fprintf(fout,"\n\n3: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); fflush(fout);

//	printf("\n\n3: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); getchar();

		if (fX2f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX2f;

		fPosSeparOfOneFeaBestf = x2;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

		} //if (fX2f < fEfficBestSeparOfOneFeaf)

	} //else

	if ( fabs(fBorderAboCurf - fBorderBelCurf) < fPrecisionOf_Golden_Searchf)
	{
	x1 = (fBorderBelCurf + fBorderAboCurf)/2.0;

	fX1f = EfficiencyOfSeparByDiff(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					x1, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);

//	fprintf(fout,"\n\n4: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); fflush(fout);

//	printf("\n\n4: x1 = %E, fX1f = %E, x2 = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   x1, fX1f, x2, fX2f,fBorderBelCurf,fBorderAboCurf); getchar();

		if (fX1f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX1f;

		fPosSeparOfOneFeaBestf = x1;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

		} //if (fX1f < fEfficBestSeparOfOneFeaf)
/*
	printf("\n\n//////////////////////////////////////////");
	printf("\n\nExit by: fabs(fBorderAboCurf - fBorderBelCurf)= %E  < fPrecisionOf_Golden_Searchf = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
	   fabs(fBorderAboCurf - fBorderBelCurf),fPrecisionOf_Golden_Searchf,fBorderBelCurf,fBorderAboCurf); fflush(fout);

	fprintf(fout,"\n\n//////////////////////////////////////////");

	fprintf(fout,"\n\nExit by: fabs(fBorderAboCurf - fBorderBelCurf)= %E  < fPrecisionOf_Golden_Searchf = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
	   fabs(fBorderAboCurf - fBorderBelCurf),fPrecisionOf_Golden_Searchf,fBorderBelCurf,fBorderAboCurf); fflush(fout);
*/

	return 1;
	} //if ( fabs(fBorderAboCurf - fBorderBelCurf) < fPrecisionOf_Golden_Searchf)

} //for (i1 = 0; i1 < nNumIterMaxForFindingBestSeparByDiffOfOneFeaf; i1++)

printf("\n\nAn error in 'FindingBestSeparByDiffOfOneFea'");
fprintf(fout,"\n\nAn error in 'FindingBestSeparByDiffOfOneFea'");

printf("\n\nx1 = %E, x2 = %E, fNumArr1stSeparf = %E, fNumArr2ndSeparf = %E",
	   x1, x2,fNumArr1stSeparf, fNumArr2ndSeparf);

printf("\n\nnSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf);

getchar();	exit(1);

return 1;
} //int FindingBestSeparByDiffOfOneFea(

float EfficiencyOfSeparByRatio(
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					const float fPosSeparf,

					int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparf,
					float &fNumArr2ndSeparf,

					float &fDiffSeparOfArr1stAndArr2ndf,
					float &fRatioOfSeparOfArr1stAndArr2ndf) //fRatioOfSeparOfArr1stAndArr2ndf

{
int nNumArr1AboSepar_Arr2AboSepar(
					const int nDim1stf,
					const int nDim2ndf,
					const  float fepsf,

					const float fArr1stf[], //0 -- 1.0
					const  float fArr2ndf[], //0 -- 1.0

					const float fPosSeparf,

					int &nNumArr1stAboSeparf,
					int &nNumArr2ndAboSeparf,

					float &fNumArr1stAboSeparf,
					float &fNumArr2ndAboSeparf);


int
	nResf,

	nNumArr1stAboSeparf = -1,
	nNumArr2ndAboSeparf = -1;

float
	fDiffAbof,
	fDiffBelf,

	fEfficf,
	fEfficTemp1f,
	fEfficTemp2f,

	fRatioAbof,
	fRatioBelf,

	fNumArr1stBelSeparf,
	fNumArr2ndBelSeparf,

	fNumArr1stAboSeparf = -1.0,
	fNumArr2ndAboSeparf = -1.0;

nResf = nNumArr1AboSepar_Arr2AboSepar(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,
					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1.0
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1.0

					fPosSeparf, //const float fPosSeparf,

					nNumArr1stAboSeparf, //int &nNumArr1stAboSeparf,
					nNumArr2ndAboSeparf, //int &nNumArr2ndAboSeparf,

					fNumArr1stAboSeparf, //float &fNumArr1stAboSeparf,
					fNumArr2ndAboSeparf); //float &fNumArr2ndAboSeparf);

if (nNumArr1stAboSeparf < 0 || nNumArr1stAboSeparf > nDim1stf || fNumArr1stAboSeparf < nNumArr1stAboSeparf)
{  
printf("\n\nAn error in 'EfficiencyOfSeparByRatio': nNumArr1stAboSeparf = %d < 0 ||..., nDim1stf = %d, fNumArr1stAboSeparf = %E, nNumArr1stAboSeparf = %d",
						nNumArr1stAboSeparf,nDim1stf,fNumArr1stAboSeparf,nNumArr1stAboSeparf);

fprintf(fout,"\n\nAn error in 'EfficiencyOfSeparByRatio': nnNumArr1stAboSeparf = %d < 0 ||..., nDim1stf = %d, fNumArr1stAboSeparf = %E, nNumArr1stAboSeparf = %d",
						nNumArr1stAboSeparf,nDim1stf,fNumArr1stAboSeparf,nNumArr1stAboSeparf);


getchar();	exit(1);
} //if (nNumArr1stAboSeparf < 0 || nNumArr1stAboSeparf > nDim1stf || ...)

if (nNumArr2ndAboSeparf < 0 || nNumArr2ndAboSeparf > nDim2ndf || fNumArr2ndAboSeparf < nNumArr2ndAboSeparf)
{  
printf("\n\nAn error in 'EfficiencyOfSeparByRatio': nNumArr2ndAboSeparf = %d < 0 ||..., nDim2ndf = %d, fNumArr2ndAboSeparf = %E, nNumArr2ndAboSeparf = %d",
						nNumArr2ndAboSeparf,nDim2ndf,fNumArr2ndAboSeparf,nNumArr2ndAboSeparf);

fprintf(fout,"\n\nAn error in 'EfficiencyOfSeparByRatio': nnNumArr2ndAboSeparf = %d < 0 ||..., nDim2ndf = %d, fNumArr2ndAboSeparf = %E, nNumArr2ndAboSeparf = %d",
						nNumArr2ndAboSeparf,nDim2ndf,fNumArr2ndAboSeparf,nNumArr2ndAboSeparf);

getchar();	exit(1);
} //if (nNumArr2ndAboSeparf < 0 || nNumArr2ndAboSeparf > nDim2ndf || ...)

fNumArr1stBelSeparf = nDim1stf - fNumArr1stAboSeparf;
fNumArr2ndBelSeparf = nDim2ndf - fNumArr2ndAboSeparf;

fDiffAbof = fNumArr1stAboSeparf - fNumArr2ndAboSeparf;

if (fNumArr2ndAboSeparf > fepsf)
	fRatioAbof = fNumArr1stAboSeparf/fNumArr2ndAboSeparf; //(float)(nDim1stf); //fNumArr2ndAboSeparf;
else
	fRatioAbof = fNumArr1stAboSeparf + 1.0; //(float)(2*nDim1stf) + 1.0; //more than otherwise the max possible value 'nDim1stf/0.5'

fDiffBelf = fNumArr1stBelSeparf - fNumArr2ndBelSeparf;

if (fNumArr2ndBelSeparf > fepsf)
	fRatioBelf = fNumArr1stBelSeparf/fNumArr2ndBelSeparf; //(float)(nDim1stf;
else
	fRatioBelf = fNumArr1stBelSeparf; //(float)(2*nDim1stf) + 1.0; //more than otherwise the max possible value 'nDim1stf/0.5'

if (fDiffAbof > fDiffBelf)
{
nSeparDiffDirectionf = 1;
fDiffSeparOfArr1stAndArr2ndf = fDiffAbof;

} //if (fDiffAbof > fDiffBelf)
else
{
nSeparDiffDirectionf = -1;
fDiffSeparOfArr1stAndArr2ndf = fDiffBelf;
} //else

if (fRatioAbof > fRatioBelf)
{
nSeparRatioDirectionf = 1;
fRatioOfSeparOfArr1stAndArr2ndf = fRatioAbof;

} //if (fRatioAbof > fRatioBelf)
else
{
nSeparRatioDirectionf = -1;
fRatioOfSeparOfArr1stAndArr2ndf = fRatioBelf;
} //else

fEfficTemp1f = fNumArr1stAboSeparf/nDim1stf + fNumArr2ndBelSeparf/nDim2ndf;

fEfficTemp2f = fNumArr1stBelSeparf/nDim1stf + fNumArr2ndAboSeparf/nDim2ndf;

if (fEfficTemp1f >= fEfficTemp2f)
{
fEfficf = -fEfficTemp1f;
fNumArr1stSeparf = fNumArr1stAboSeparf;
fNumArr2ndSeparf = fNumArr2ndBelSeparf;

} //if (fEfficTemp1f >= fEfficTemp2f)
else
{
fEfficf = -fEfficTemp2f;
fNumArr1stSeparf = fNumArr1stBelSeparf;
fNumArr2ndSeparf = fNumArr2ndAboSeparf;

} //else

//fNumArr1stSeparf


//fEfficf = -( fRatioOfSeparOfArr1stAndArr2ndf + ( fDiffSeparOfArr1stAndArr2ndf/(nDim1stf + nDim2ndf) ) );
/*
printf("\n///////////////////////////////////////////////");
printf("\n\nEfficiencyOfSeparByRatio: fPosSeparf = %E, fNumArr1stAboSeparf = %E, fNumArr2ndAboSeparf = %E, fDiffAbof = %E, fRatioAbof = %E",
	   fPosSeparf,fNumArr1stAboSeparf, fNumArr2ndAboSeparf, fDiffAbof, fRatioAbof);

printf("\n\nEfficiencyOfSeparByRatio: fNumArr1stBelSeparf = %E, fNumArr2ndBelSeparf = %E, fDiffBelf = %E, fRatioBelf = %E",
	   fNumArr1stBelSeparf, fNumArr2ndBelSeparf, fDiffBelf, fRatioBelf);

printf("\n\nEfficiencyOfSeparByRatio: nSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, fEfficf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf, fEfficf); getchar();

fprintf(fout,"\n///////////////////////////////////////////////");
fprintf(fout,"\n\nfPosSeparf = %E, fNumArr1stAboSeparf = %E, fNumArr2ndAboSeparf = %E, fDiffAbof = %E, fRatioAbof = %E",
	   fPosSeparf,fNumArr1stAboSeparf, fNumArr2ndAboSeparf, fDiffAbof, fRatioAbof);

fprintf(fout,"\n\nEfficiencyOfSeparByRatio: fNumArr1stBelSeparf = %E, fNumArr2ndBelSeparf = %E, fDiffBelf = %E, fRatioBelf = %E",
	   fNumArr1stBelSeparf, fNumArr2ndBelSeparf, fDiffBelf, fRatioBelf);

fprintf(fout,"\n\nEfficiencyOfSeparByRatio: nSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, fEfficf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf, fEfficf); 

fflush(fout);
*/

return fEfficf;

//return 1;
} // float EfficiencyOfSeparByRatio(...
////////////////////////////////////////////////////////////////////

float EfficiencyOfSeparByDiff(
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					const float fPosSeparf,

					int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparf,
					float &fNumArr2ndSeparf,

					float &fDiffSeparOfArr1stAndArr2ndf,
					float &fRatioOfSeparOfArr1stAndArr2ndf) //fRatioOfSeparOfArr1stAndArr2ndf

{
int nNumArr1AboSepar_Arr2AboSepar(
					const int nDim1stf,
					const int nDim2ndf,
					const  float fepsf,

					const float fArr1stf[], //0 -- 1.0
					const  float fArr2ndf[], //0 -- 1.0

					const float fPosSeparf,

					int &nNumArr1stAboSeparf,
					int &nNumArr2ndAboSeparf,

					float &fNumArr1stAboSeparf,
					float &fNumArr2ndAboSeparf);


int
	nResf,

	nNumArr1stAboSeparf = -1,
	nNumArr2ndAboSeparf = -1;

float
	fDiffAbof,
	fDiffBelf,

	fEfficf,
	fEfficTemp1f,
	fEfficTemp2f,

	fRatioAbof,
	fRatioBelf,

	fNumArr1stBelSeparf,
	fNumArr2ndBelSeparf,

	fNumArr1stAboSeparf = -1.0,
	fNumArr2ndAboSeparf = -1.0;

nResf = nNumArr1AboSepar_Arr2AboSepar(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,
					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1.0
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1.0

					fPosSeparf, //const float fPosSeparf,

					nNumArr1stAboSeparf, //int &nNumArr1stAboSeparf,
					nNumArr2ndAboSeparf, //int &nNumArr2ndAboSeparf,

					fNumArr1stAboSeparf, //float &fNumArr1stAboSeparf,
					fNumArr2ndAboSeparf); //float &fNumArr2ndAboSeparf);

if (nNumArr1stAboSeparf < 0 || nNumArr1stAboSeparf > nDim1stf || fNumArr1stAboSeparf < nNumArr1stAboSeparf)
{  
printf("\n\nAn error in 'EfficiencyOfSeparByDiff': nNumArr1stAboSeparf = %d < 0 ||..., nDim1stf = %d, fNumArr1stAboSeparf = %E, nNumArr1stAboSeparf = %d",
						nNumArr1stAboSeparf,nDim1stf,fNumArr1stAboSeparf,nNumArr1stAboSeparf);

fprintf(fout,"\n\nAn error in 'EfficiencyOfSeparByDiff': nnNumArr1stAboSeparf = %d < 0 ||..., nDim1stf = %d, fNumArr1stAboSeparf = %E, nNumArr1stAboSeparf = %d",
						nNumArr1stAboSeparf,nDim1stf,fNumArr1stAboSeparf,nNumArr1stAboSeparf);


getchar();	exit(1);
} //if (nNumArr1stAboSeparf < 0 || nNumArr1stAboSeparf > nDim1stf || ...)

if (nNumArr2ndAboSeparf < 0 || nNumArr2ndAboSeparf > nDim2ndf || fNumArr2ndAboSeparf < nNumArr2ndAboSeparf)
{  
printf("\n\nAn error in 'EfficiencyOfSeparByDiff': nNumArr2ndAboSeparf = %d < 0 ||..., nDim2ndf = %d, fNumArr2ndAboSeparf = %E, nNumArr2ndAboSeparf = %d",
						nNumArr2ndAboSeparf,nDim2ndf,fNumArr2ndAboSeparf,nNumArr2ndAboSeparf);

fprintf(fout,"\n\nAn error in 'EfficiencyOfSeparByDiff': nnNumArr2ndAboSeparf = %d < 0 ||..., nDim2ndf = %d, fNumArr2ndAboSeparf = %E, nNumArr2ndAboSeparf = %d",
						nNumArr2ndAboSeparf,nDim2ndf,fNumArr2ndAboSeparf,nNumArr2ndAboSeparf);

getchar();	exit(1);
} //if (nNumArr2ndAboSeparf < 0 || nNumArr2ndAboSeparf > nDim2ndf || ...)

fNumArr1stBelSeparf = nDim1stf - fNumArr1stAboSeparf;
fNumArr2ndBelSeparf = nDim2ndf - fNumArr2ndAboSeparf;

fDiffAbof = fNumArr1stAboSeparf - fNumArr2ndAboSeparf;

if (fNumArr2ndAboSeparf > fepsf)
	fRatioAbof = fNumArr1stAboSeparf/fNumArr2ndAboSeparf; //(float)(nDim1stf); //fNumArr2ndAboSeparf;
else
	fRatioAbof = fNumArr1stAboSeparf + 1.0; //(float)(2*nDim1stf) + 1.0; //more than otherwise the max possible value 'nDim1stf/0.5'

fDiffBelf = fNumArr1stBelSeparf - fNumArr2ndBelSeparf;

if (fNumArr2ndBelSeparf > fepsf)
	fRatioBelf = fNumArr1stBelSeparf/fNumArr2ndBelSeparf; //(float)(nDim1stf;
else
	fRatioBelf = fNumArr1stBelSeparf; //(float)(2*nDim1stf) + 1.0; //more than otherwise the max possible value 'nDim1stf/0.5'

if (fDiffAbof > fDiffBelf)
{
nSeparDiffDirectionf = 1;
fDiffSeparOfArr1stAndArr2ndf = fDiffAbof;

} //if (fDiffAbof > fDiffBelf)
else
{
nSeparDiffDirectionf = -1;
fDiffSeparOfArr1stAndArr2ndf = fDiffBelf;
} //else

if (fRatioAbof > fRatioBelf)
{
nSeparRatioDirectionf = 1;
fRatioOfSeparOfArr1stAndArr2ndf = fRatioAbof;

} //if (fRatioAbof > fRatioBelf)
else
{
nSeparRatioDirectionf = -1;
fRatioOfSeparOfArr1stAndArr2ndf = fRatioBelf;
} //else

fEfficTemp1f = fNumArr1stAboSeparf/nDim1stf + fNumArr2ndBelSeparf/nDim2ndf;

fEfficTemp2f = fNumArr1stBelSeparf/nDim1stf + fNumArr2ndAboSeparf/nDim2ndf;

if (fEfficTemp1f >= fEfficTemp2f)
{
//fEfficf = -fEfficTemp1f;
fNumArr1stSeparf = fNumArr1stAboSeparf;
fNumArr2ndSeparf = fNumArr2ndBelSeparf;

} //if (fEfficTemp1f >= fEfficTemp2f)
else
{
//fEfficf = -fEfficTemp2f;
fNumArr1stSeparf = fNumArr1stBelSeparf;
fNumArr2ndSeparf = fNumArr2ndAboSeparf;

} //else

//fNumArr1stSeparf


//fEfficf = -( fRatioOfSeparOfArr1stAndArr2ndf + ( fDiffSeparOfArr1stAndArr2ndf/(nDim1stf + nDim2ndf) ) );

fEfficf = -( fDiffSeparOfArr1stAndArr2ndf/(nDim1stf + nDim2ndf) );

/*
printf("\n///////////////////////////////////////////////");
printf("\n\nEfficiencyOfSeparByDiff: fPosSeparf = %E, fNumArr1stAboSeparf = %E, fNumArr2ndAboSeparf = %E, fDiffAbof = %E, fRatioAbof = %E",
	   fPosSeparf,fNumArr1stAboSeparf, fNumArr2ndAboSeparf, fDiffAbof, fRatioAbof);

printf("\n\nEfficiencyOfSeparByDiff: fNumArr1stBelSeparf = %E, fNumArr2ndBelSeparf = %E, fDiffBelf = %E, fRatioBelf = %E",
	   fNumArr1stBelSeparf, fNumArr2ndBelSeparf, fDiffBelf, fRatioBelf);

printf("\n\nEfficiencyOfSeparByDiff: nSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, fEfficf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf, fEfficf); getchar();

fprintf(fout,"\n///////////////////////////////////////////////");
fprintf(fout,"\n\nfPosSeparf = %E, fNumArr1stAboSeparf = %E, fNumArr2ndAboSeparf = %E, fDiffAbof = %E, fRatioAbof = %E",
	   fPosSeparf,fNumArr1stAboSeparf, fNumArr2ndAboSeparf, fDiffAbof, fRatioAbof);

fprintf(fout,"\n\nEfficiencyOfSeparByDiff: fNumArr1stBelSeparf = %E, fNumArr2ndBelSeparf = %E, fDiffBelf = %E, fRatioBelf = %E",
	   fNumArr1stBelSeparf, fNumArr2ndBelSeparf, fDiffBelf, fRatioBelf);

fprintf(fout,"\n\nEfficiencyOfSeparByDiff: nSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, fEfficf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf, fEfficf); 

fflush(fout);
*/

return fEfficf;

//return 1;
} // float EfficiencyOfSeparByDiff(...

int nNumArr1AboSepar_Arr2AboSepar(
					const int nDim1stf,
					const int nDim2ndf,
					const  float fepsf,

					const float fArr1stf[], //0 -- 1.0
					const  float fArr2ndf[], //0 -- 1.0

					const float fPosSeparf,

					int &nNumArr1stAboSeparf,
					int &nNumArr2ndAboSeparf,

					float &fNumArr1stAboSeparf,
					float &fNumArr2ndAboSeparf)
{

int 
	i1;

float
	fPosSeparMinf = fPosSeparf - fepsf,
	fPosSeparMaxf = fPosSeparf + fepsf;

nNumArr1stAboSeparf = 0;
nNumArr2ndAboSeparf = 0;

fNumArr1stAboSeparf = 0.0;
fNumArr2ndAboSeparf = 0.0;

for (i1 = 0; i1 < nDim1stf;  i1++)
{
	if (fArr1stf[i1] > fPosSeparMaxf)
	{
	nNumArr1stAboSeparf += 1;
	fNumArr1stAboSeparf += 1.0;
	} //if (fArr1stf[i1] > fPosSeparMaxf)
	else if (fArr1stf[i1] >= fPosSeparMinf && fArr1stf[i1] <= fPosSeparMaxf)
		fNumArr1stAboSeparf += 0.5;

} //for (i1 = 0; i1 < nDim1stf;  i1++)

for (i1 = 0; i1 < nDim2ndf;  i1++)
{
	if (fArr2ndf[i1] > fPosSeparMaxf)
	{
	nNumArr2ndAboSeparf += 1;
	fNumArr2ndAboSeparf += 1.0;
	} //if (fArr2ndf[i1] > fPosSeparMaxf)
	else if (fArr2ndf[i1] >= fPosSeparMinf && fArr2ndf[i1] <= fPosSeparMaxf)
		fNumArr2ndAboSeparf += 0.5;

	//if (fArr2ndf[i1] < fPosSeparf)
	//	nNumArr2ndAboSeparf += 1;


} //for (i1 = 0; i1 < nDim2ndf;  i1++)

return 1;
} // int nNumArr1AboSepar_Arr2AboSepar(...

////////////////////////////////////////////////////////////////////////////////////////
int Extracting_Data_From_OneLine(								 
								 const char *cInputOneLinef,
									const int nDim_D_WithConstf,

								 int &nYf,
	
								 float fFeaOneLineArrf[]) //[nDim_D_WithConst]
{	
int 
	nPosInTheCurrentSegmentf = 0,
	nSegmentNumberCurf = 0,

	nNumOfFeaCurf = -1, //initially
	nPosInTheLinef = 0,
	nLengthOfInputCharLinef = strlen(cInputOneLinef);

char 
	cCharSubstringf[nSubstringLenMax];

	if (nLengthOfInputCharLinef >= nLengthOneLineMax - 5)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\nAn error in 'Extracting_Data_From_OneLine': nLengthOfInputCharLinef >= nLengthOneLineMax - 5");
		fprintf(fout,"\n\nAn error in 'Extracting_Data_From_OneLine': nLengthOfInputCharLinef >= nLengthOneLineMax - 5");
		//getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return UNSUCCESSFUL_RETURN;
	} // if (nLengthOfInputCharLinef >= nLengthOneLineMax - 5)

//printf("\n\n'Extracting_Data_From_OneLine': %s\n",cInputOneLinef);

	do // while(nPosInTheLinef < nLengthOfInputCharLinef);
	{ 
	nPosInTheCurrentSegmentf = 0; 

		do			
		{
		cCharSubstringf[nPosInTheCurrentSegmentf] = cInputOneLinef[nPosInTheLinef];	
		nPosInTheLinef++;

		nPosInTheCurrentSegmentf++; 
		} while( cInputOneLinef[nPosInTheLinef-1] != ' ' &&  cInputOneLinef[nPosInTheLinef - 1] != ':' && cInputOneLinef[nPosInTheLinef-1] != '\t' && cInputOneLinef[nPosInTheLinef-1] != '\0');
		
/*
		while( cInputOneLinef[nPosInTheLinef] == ' ')
		{
		nPosInTheLinef++;
		}// while( cInputOneLinef[nPosInTheLinef] == ' ' )
*/
		if (nSegmentNumberCurf == 0)
			nYf = atoi(cCharSubstringf);
		else if (nSegmentNumberCurf == 1)
		{
			nNumOfFeaCurf = atoi(cCharSubstringf);
			if (nNumOfFeaCurf != 1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Extracting_Data_From_OneLine': nNumOfFeaCurf = %d, nPosInTheLinef = %d at nSegmentNumberCurf == 1", nNumOfFeaCurf, nPosInTheLinef);
				fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine': nNumOfFeaCurf = %d, nPosInTheLinef = %d at nSegmentNumberCurf == 1", nNumOfFeaCurf, nPosInTheLinef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nNumOfFeaCurf != 1)

		} //else if (nSegmentNumberCurf == 1)
		else if (nSegmentNumberCurf == 2)
		{
			fFeaOneLineArrf[0] = atof(cCharSubstringf);
			if (fFeaOneLineArrf[0] < fFeaMin || fFeaOneLineArrf[0] > fFeaMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Extracting_Data_From_OneLine': fFeaOneLineArrf[0] = %E, nPosInTheLinef = %d at nSegmentNumberCurf == 2", fFeaOneLineArrf[0], nPosInTheLinef);
				fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine': fFeaOneLineArrf[0] = %E, nPosInTheLinef = %d at nSegmentNumberCurf == 2", fFeaOneLineArrf[0], nPosInTheLinef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (fFeaOneLineArrf[0] < fFeaMin || fFeaOneLineArrf[0] > fFeaMax)

		} //else if (nSegmentNumberCurf == 2)
		else if (nSegmentNumberCurf == 3)
		{
			nNumOfFeaCurf = atoi(cCharSubstringf);
			if (nNumOfFeaCurf != 2)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Extracting_Data_From_OneLine': nNumOfFeaCurf = %d, nPosInTheLinef = %d at nSegmentNumberCurf == 3", nNumOfFeaCurf, nPosInTheLinef);
				fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine': nNumOfFeaCurf = %d, nPosInTheLinef = %d at nSegmentNumberCurf == 3", nNumOfFeaCurf, nPosInTheLinef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nNumOfFeaCurf != 2)

		} //else if (nSegmentNumberCurf == 3)
		else if (nSegmentNumberCurf == 4)
		{
			fFeaOneLineArrf[1] = atof(cCharSubstringf);
			if (fFeaOneLineArrf[1] < fFeaMin || fFeaOneLineArrf[1] > fFeaMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Extracting_Data_From_OneLine': fFeaOneLineArrf[1] = %E, nPosInTheLinef = %d at nSegmentNumberCurf == 4", fFeaOneLineArrf[1], nPosInTheLinef);
				fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine': fFeaOneLineArrf[1] = %E, nPosInTheLinef = %d at nSegmentNumberCurf == 4", fFeaOneLineArrf[1], nPosInTheLinef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (fFeaOneLineArrf[1] < fFeaMin || fFeaOneLineArrf[1] > fFeaMax)
		} //else if (nSegmentNumberCurf == 4)
		else if (nSegmentNumberCurf == 5)
		{
			nNumOfFeaCurf = atoi(cCharSubstringf);
			if (nNumOfFeaCurf != 3)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Extracting_Data_From_OneLine': nNumOfFeaCurf = %d, nPosInTheLinef = %d at nSegmentNumberCurf == 5", nNumOfFeaCurf, nPosInTheLinef);
				fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine': nNumOfFeaCurf = %d, nPosInTheLinef = %d at nSegmentNumberCurf == 5", nNumOfFeaCurf, nPosInTheLinef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nNumOfFeaCurf != 3)

		} //else if (nSegmentNumberCurf == 5)
		else if (nSegmentNumberCurf == 6)
		{
			fFeaOneLineArrf[2] = atof(cCharSubstringf);
			if (fFeaOneLineArrf[2] < fFeaMin || fFeaOneLineArrf[2] > fFeaMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Extracting_Data_From_OneLine': fFeaOneLineArrf[2] = %E, nPosInTheLinef = %d at nSegmentNumberCurf == 6", fFeaOneLineArrf[2], nPosInTheLinef);
				fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine': fFeaOneLineArrf[2] = %E, nPosInTheLinef = %d at nSegmentNumberCurf == 6", fFeaOneLineArrf[2], nPosInTheLinef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (fFeaOneLineArrf[2] < fFeaMin || fFeaOneLineArrf[2] > fFeaMax)
		} //else if (nSegmentNumberCurf == 6)
		else if (nSegmentNumberCurf == 7)
		{
			nNumOfFeaCurf = atoi(cCharSubstringf);
			if (nNumOfFeaCurf != 4)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Extracting_Data_From_OneLine': nNumOfFeaCurf = %d, nPosInTheLinef = %d at nSegmentNumberCurf == 7", nNumOfFeaCurf, nPosInTheLinef);
				fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine': nNumOfFeaCurf = %d, nPosInTheLinef = %d at nSegmentNumberCurf == 7", nNumOfFeaCurf, nPosInTheLinef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nNumOfFeaCurf != 4)

		} //else if (nSegmentNumberCurf == 7)
		else if (nSegmentNumberCurf == 8)
		{
			fFeaOneLineArrf[3] = atof(cCharSubstringf);
			if (fFeaOneLineArrf[3] < fFeaMin || fFeaOneLineArrf[3] > fFeaMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Extracting_Data_From_OneLine': fFeaOneLineArrf[3] = %E, nPosInTheLinef = %d at nSegmentNumberCurf == 8", fFeaOneLineArrf[3], nPosInTheLinef);
				fprintf(fout, "\n\n  An error in  'Extracting_Data_From_OneLine':fFeaOneLineArrf[3] = %E, nPosInTheLinef = %d at nSegmentNumberCurf == 8", fFeaOneLineArrf[3], nPosInTheLinef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (fFeaOneLineArrf[3] < fFeaMin || fFeaOneLineArrf[3] > fFeaMax)

			return SUCCESSFUL_RETURN;
		} //else if (nSegmentNumberCurf == 6)

	memset( cCharSubstringf, 0, sizeof( cCharSubstringf ) );

	nSegmentNumberCurf = nSegmentNumberCurf + 1;

	} while(nPosInTheLinef < nLengthOfInputCharLinef);

	return SUCCESSFUL_RETURN;
}// int Extracting_Data_From_OneLine(...)
////////////////////////////////////////////////////////////////////////////////////

int Reading_All_LinesOfData_OfOneFile(
	const int nDim_Df,
	const int nNumOfVecsTotf,

	const int nReadTrainOrTestf, // 1-- train, (-1) -- test
	///////////////////////////////////////////////////////
	int nY_Arrf[], //[nNumOfVecsTotf]

	float fFeasAll_Arrf[]) //[nProdTot]
{
	int Extracting_Data_From_OneLine(
		const char *cInputOneLinef,
		const int nDim_Df,

		int &nYf,

		float fFeaOneLineArrf[]); //[nDim_D_WithConst]

	int
		iVecf = -1,
		nIndexf,
		iFeaf,
		nTempf,
		nInputLineLengthf,

		nResf,
		nY_Curf;
	
	float
		fFeaOneLineArrf[nDim_D_WithConst]; //float &fPriceOpenf,;

	char cInputLinef[nInputLineLengthMax];

	for (; ;)
	{
		memset(cInputLinef, 0, sizeof(cInputLinef));

		iVecf += 1;

		if (nReadTrainOrTestf == 1)
		{
			if (fgets(cInputLinef, nInputLineLengthMax, fin_Train) == NULL)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n No data to read: fgets(cInputLinef, nInputLineLengthMaxf, fin_Train) == NULL, iVecf = %d", iVecf);
				fprintf(fout,"\n\n No data to read: fgets(cInputLinef, nInputLineLengthMaxf, fin_Train) == NULL, iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return SUCCESSFUL_RETURN;
			} //if (fgets(cInputLinef, nInputLineLengthMax, fin_Train) == NULL)

		} // if (nReadTrainOrTestf == 1)
		else if (nReadTrainOrTestf == -1)
		{
			if (fgets(cInputLinef, nInputLineLengthMax, fin_Test) == NULL)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n No data to read: fgets(cInputLinef, nInputLineLengthMaxf, fin_Test) == NULL, iVecf = %d", iVecf);
				fprintf(fout, "\n\n No data to read: fgets(cInputLinef, nInputLineLengthMaxf, fin_Test) == NULL, iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return SUCCESSFUL_RETURN;
			} //if (fgets(cInputLinef, nInputLineLengthMax, fin_Test) == NULL)

		} // if (nReadTrainOrTestf == -1)
/////////////////////////////////////
		nResf = Extracting_Data_From_OneLine(
			cInputLinef, //const char *cInputOneLinef,
			nDim_Df, //const int nDim_Df,

			nY_Curf, //int &nYf,

			fFeaOneLineArrf); // float fFeaOneLineArrf[]); //[nDim_D_WithConst]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n An error in 'Reading_All_LinesOfData_OfOneFile' for iVecf = %d", iVecf);
			fprintf(fout, "\n\n An error in 'Reading_All_LinesOfData_OfOneFile' for iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}// if (nResf == UNSUCCESSFUL_RETURN)

		nY_Arrf[iVecf] = nY_Curf;

		nTempf = iVecf * nDim_Df;
		for (iFeaf = 0; iFeaf < nDim_Df; iFeaf++)
		{
			nIndexf = nTempf + iFeaf;

			fFeasAll_Arrf[nIndexf] = fFeaOneLineArrf[iFeaf];
		}//for (iFeaf = 0; iFeaf < nDim_Df; iFeaf++)

	} //for( ; ;)

	return SUCCESSFUL_RETURN;
} //int Reading_All_LinesOfData_OfOneFile(...
/////////////////////////////////////////////////////////////////////////////////

int Reading_All_TrainTest_Data(
	const int nDim_Df,

	const int nNumVecTrainTotf,
	const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
	int nY_Train_Arrf[], //[nNumVecTrainTot]
	int nY_Test_Arrf[], //[nNumVecTestTot]

	float fFeaTrain_Arrf[], //[nProdTrainTot]
	float fFeaTest_Arrf[]) //[nProdTestTot]
{
	int Reading_All_LinesOfData_OfOneFile(
		const int nDim_Df,
		const int nNumOfVecsTotf,

		const int nReadTrainOrTestf, // 1-- train, (-1) -- test
		///////////////////////////////////////////////////////
		int nY_Arrf[], //[nNumOfVecsTotf]

		float fFeasAll_Arrf[]); //[nProdTrainTot]

	int
		nResf,
		nReadTrainOrTestf; // 1-- train, (-1) -- test

////////////////////////////////////////////////////
//Train
	nReadTrainOrTestf = 1;
	nResf = Reading_All_LinesOfData_OfOneFile(
				nDim_Df, //const int nDim_Df,
				nNumVecTrainTotf, //const int nNumOfVecsTotf,

				nReadTrainOrTestf, //const int nReadTrainOrTestf, // 1-- train, (-1) -- test
				///////////////////////////////////////////////////////
				nY_Train_Arrf, //int nY_Arrf[], //[nNumOfVecsTotf]

				fFeaTrain_Arrf); // float fFeasAll_Arrf[]); //[nProdTrainTot]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in 'Reading_All_TrainTest_Data' for train data");
		fprintf(fout, "\n\n An error in 'Reading_All_TrainTest_Data' for train data");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (nResf == UNSUCCESSFUL_RETURN)

////////////////////
//Test
	nReadTrainOrTestf = -1;
	nResf = Reading_All_LinesOfData_OfOneFile(
			nDim_Df, //const int nDim_Df,
			nNumVecTestTotf, //const int nNumOfVecsTotf,

			nReadTrainOrTestf, //const int nReadTestOrTestf, // 1-- train, (-1) -- test
			///////////////////////////////////////////////////////
			nY_Test_Arrf, //int nY_Arrf[], //[nNumOfVecsTotf]

			fFeaTest_Arrf); // float fFeasAll_Arrf[]); //[nProdTestTot]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in 'Reading_All_TrainTest_Data' for test data");
		fprintf(fout, "\n\n An error in 'Reading_All_TrainTest_Data' for test data");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (nResf == UNSUCCESSFUL_RETURN)

	return SUCCESSFUL_RETURN;
}//int Reading_All_TrainTest_Data(...

//////////////////////////////////////////////////////////////////////////////////
int SelectingBestFeas(
					const float fLargef,
					const  float fepsf,

					const int nDimf,
					const int nDimSelecMaxf,

					const int nNumVec1stf,
					const int nNumVec2ndf,

					const float fPrecisionOf_Golden_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
					const float fEfficBestSeparOfOneFeaAcceptMinf,

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

					int &nDimSelecf,
					//float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf]
					//float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf]

					float &fRatioBestMinf,

					int &nPosOneFeaBestMaxf,

					int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestMaxf,
					float &fNumArr2ndSeparBestMaxf,

					float fRatioBestArrf[], //nDimSelecMaxf

					int nPosFeaSeparBestArrf[]) //nDimSelecMaxf
{
int FindingBestSeparByRatioOfOneFea(
					const float fLargef,
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fPrecisionOf_Golden_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, 

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					float &fEfficBestSeparOfOneFeaf,

					float &fPosSeparOfOneFeaBestf,

					int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestf,
					float &fNumArr2ndSeparBestf,

					float &fDiffSeparOfArr1stAndArr2ndBestf,
					float &fRatioOfSeparOfArr1stAndArr2ndBestf); 					

int
	nResf,
	nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestff, //1 -- Abof, -1 -- Bel
	nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestff, //1 -- Abof, -1 -- Bel

	iFeaf,
	iVecf;

float

		fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaff,

		fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestff,


		fNumArr1stSeparBestf, //float &fNumArr1stSeparBestff,
		fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestff,

		fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestff,
		fRatioOfSeparOfArr1stAndArr2ndBestf; //

//Initialization
nDimSelecf = 0;

fRatioBestMinf = fLargef;

nPosOneFeaBestMaxf = -1;

for (iFeaf = 0; iFeaf < nDimSelecMaxf; iFeaf++)
{
nPosFeaSeparBestArrf[iFeaf]  = -1; //nDimSelecMaxf
fRatioBestArrf[iFeaf]  = -1.0;
} // for (iFeaf = 0; iFeaf < nDimSelecMaxf; iFeaf++)

//////////////////////////////////////
float *fOneFea1stArrf = new float[nNumVec1stf];
float *fOneFea2ndArrf = new float[nNumVec2ndf];

if (fOneFea1stArrf == NULL || fOneFea2ndArrf == NULL)
{
printf("\n\nAn error in 'SelectingBestFeas': fOneFea1stArrf == NULL || fOneFea2ndArrf == NULL");
fprintf(fout,"\n\nAn error in 'SelectingBestFeas': fOneFea1stArrf == NULL || fOneFea2ndArrf == NULL");

getchar();	exit(1);
} //if (fOneFea1stArrf == NULL || fOneFea2ndArrf == NULL)

for (iFeaf = 0; iFeaf < nDim; iFeaf++)
{
	for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
	{
	fOneFea1stArrf[iVecf] = fArr1stf[iFeaf + iVecf*nDim];
/*		
		if (fOneFea1stArrf[iVecf] < -fepsf || fOneFea1stArrf[iVecf] > 1.0 + fepsf)
		{
		printf("\n\nAn error in 'SelectingBestFeas': fOneFea1stArrf[%d] = %E, iFeaf = %d", 
						iVecf,fOneFea1stArrf[iVecf],iFeaf);

		fprintf(fout,"\n\nAn error in 'SelectingBestFeas': fOneFea1stArrf[%d] = %E, iFeaf = %d", 
						iVecf,fOneFea1stArrf[iVecf],iFeaf);

		getchar();	exit(1);
		} //if (fOneFea1stArrf[iVecf] < -fepsf || fOneFea1stArrf[iVecf] > 1.0 + fepsf)
*/
	} //for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

	for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
	{
	fOneFea2ndArrf[iVecf] = fArr2ndf[iFeaf + iVecf*nDim]; //	fOneFeaTrain_2ndArr[nNumVecTrain_2nd],
/*
		if (fOneFea2ndArrf[iVecf] < -fepsf || fOneFea2ndArrf[iVecf] > 1.0 + fepsf)
		{
		printf("\n\nAn error in 'SelectingBestFeas': fOneFea2ndArrf[%d] = %E, iFeaf = %d", 
						iVecf,fOneFea2ndArrf[iVecf],iFeaf);

		fprintf(fout,"\n\nAn error in 'SelectingBestFeas': fOneFea2ndArrf[%d] = %E, iFeaf = %d", 
						iVecf,fOneFea2ndArrf[iVecf],iFeaf);

		getchar();	exit(1);
		} //if (fOneFea2ndArrf[iVecf] < -fepsf || fOneFea2ndArrf[iVecf] > 1.0 + fepsf)
*/
	} //for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)


nResf = FindingBestSeparByRatioOfOneFea(
					fLargef, //const float fLargef,

					nNumVec1stf, //const int nDim1stf,
					nNumVec2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fPrecisionOf_Golden_Searchf, //const float fPrecisionOf_Golden_Searchf,

					0.0, //const  float fBorderBelf,
					1.0, //const  float fBorderAbof,

					nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, 

					fOneFea1stArrf, //const float fArr1stf[], //0 -- 1
					fOneFea2ndArrf, //const  float fArr2ndf[], //0 -- 1

					fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaff,

					fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestff,

					nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestff, //1 -- Abof, -1 -- Bel
					nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestff, //1 -- Abof, -1 -- Bel

					fNumArr1stSeparBestf, //float &fNumArr1stSeparBestff,
					fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestff,

					fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestff,
					fRatioOfSeparOfArr1stAndArr2ndBestf); //float &fRatioOfSeparOfArr1stAndArr2ndBestf);

printf("\n\nfEfficBestSeparOfOneFeaf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E, nSeparRatioDirectionBestf = %d, iFeaf = %d",
	   fEfficBestSeparOfOneFeaf,fNumArr1stSeparBestf,fNumArr2ndSeparBestf,nSeparRatioDirectionBestf,iFeaf);

	if (fEfficBestSeparOfOneFeaf <= -fEfficBestSeparOfOneFeaAcceptMinf)
	{
	nDimSelecf += 1;
//nPosSeparOfOneFeaBestArrf[]) //nDimSelecMaxf
		if (nDimSelecf - 1 < nDimSelecMaxf)
		{
		nPosFeaSeparBestArrf[nDimSelecf - 1] = iFeaf;
		fRatioBestArrf[nDimSelecf - 1] = fEfficBestSeparOfOneFeaf;

		} //if (nDimSelecf - 1 < nDimSelecMaxf)
		else
		{
		printf("\n\nToo many features: nDimSelecf = %d > nDimSelecMaxf = %d",nDimSelecf,nDimSelecMaxf);
		printf("\n\nPlease increase 'nDimSelecMaxf' or 'fEfficBestSeparOfOneFeaAcceptMinf'");
		
		delete [] fOneFea1stArrf;
		delete [] fOneFea2ndArrf;
		return 2;
		} //else

		if (fEfficBestSeparOfOneFeaf < fRatioBestMinf)
		{
		fRatioBestMinf = fEfficBestSeparOfOneFeaf;
		nPosOneFeaBestMaxf = iFeaf;
		} // if (fEfficBestSeparOfOneFeaf < fRatioBestMinf)

	} //if (fEfficBestSeparOfOneFeaf <= -fEfficBestSeparOfOneFeaAcceptMinf)

} //for (iFeaf = 0; iFeaf < nDim; iFeaf++)

delete [] fOneFea1stArrf;
delete [] fOneFea2ndArrf;

	if (nDimSelecf <= nDimSelecMaxf)
	{
		return 1;
	} //
		else if (nDimSelecf > nDimSelecMaxf)
	{
	printf("\n\nToo many features: nDimSelecf = %d > nDimSelecMaxf = %d",nDimSelecf,nDimSelecMaxf);
	printf("\n\nPlease increase 'nDimSelecMaxf' or 'fEfficBestSeparOfOneFeaAcceptMinf'");
	return 2;
	} //else if (nDimSelecf > nDimSelecMaxf)
	else if (nDimSelecf == 0)
	{
		return 0;
	} //

} //int SelectingBestFeas(

int Converting_Arr_To_Selec(
					const int nDimf,
					const int nDimSelecf,

					const int nNumVecf,

					const int nPosFeaSeparBestArr[], //[nDimSelecf]

					const float fVecArr[],
					float fVecSelecArr[]) //[nDimSelecf]
{
int
	iFeaf,
	iVecf,
	nPosSelecCurf;
	
for (iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
{
nPosSelecCurf = nPosFeaSeparBestArr[iFeaf];
	if (nPosSelecCurf < 0 || nPosSelecCurf > nDimf - 1)
	{
	printf("\n\nAn error in 'Converting_Arr_To_Selec': nPosSelecCurf = %d < 0 ||..., iFeaf = %d", 
					nPosSelecCurf,iFeaf);
	
	fprintf(fout,"\n\nAn error in 'Converting_Arr_To_Selec': nPosSelecCurf = %d < 0 ||..., iFeaf = %d", 
					nPosSelecCurf,iFeaf);

	getchar();	exit(1);
	} //if (nPosSelecCurf < 0 || nPosSelecCurf > nDimf - 1)

	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
	fVecSelecArr[iFeaf + iVecf*nDimSelecf] = fVecArr[nPosSelecCurf + iVecf*nDimf];
	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

} //for (iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)

return 1;
} //int Converting_Arr_To_Selec(...
////////////////////////////////////////////////////////////////
int NumOfNonZeros(
				  const int nDimf,
				  const int nArrf[], // [nDimf]

				  int &nNumDayIntervalOfNonZeros)
{
int
	i1;

nNumDayIntervalOfNonZeros = 0;

for(i1 = 0; i1 < nDimf;  i1++)
{
	if (nArrf[i1] > 0)
		nNumDayIntervalOfNonZeros +=1;
	else if (nArrf[i1] < 0)
	{
	printf("\n\nAn error in 'NumOfNonZeros': nArrf[%d] = %d < 0", 
						i1,nArrf[i1]);
	fprintf(fout,"\n\nAn error in 'NumOfNonZeros': nArrf[%d] = %d < 0", 
						i1,nArrf[i1]);

	getchar();	exit(1);
	} //else if (nArrf[i1] < 0)

} //for(i1 = 0; i1 < nDimf;  i1++)

return 1;
} //int NumOfNonZeros(

void Copying_Float_Arr1_To_Arr2(
				  const int nDimf,
				  const float fArr1f[], // [nDimf]
				  float fArr2f[]) // [nDimf]
{
int 
	i1;

for (i1 = 0; i1 < nDimf; i1++)
{
	fArr2f[i1] = fArr1f[i1];
} //

} // void Copying_Float_Arr1_To_Arr2(...

void Copying_Int_Arr1_To_Arr2(
	const int nDimf,
	const int nArr1f[], // [nDimf]
	int nArr2f[]) // [nDimf]
{
	int
		i1;

	for (i1 = 0; i1 < nDimf; i1++)
		nArr2f[i1] = nArr1f[i1];

} // void Copying_Int_Arr1_To_Arr2(...
///////////////////////////////////////////////////

int Int_Belongs_To_Arr_Or_Not_With_Update(
											const int nDimMaxf,

											const int nDimInitf,
											const int nTestf,
											
											int &nBelongsf, //'1' --> belongs and '-1' otherwise
											int &nDimCurf,

											int nArrf[])
{
int
	i1;

int
	nDifff;

nBelongsf = 0; //impossible
nDimCurf = -1;

for (i1 = 0; i1 < nDimInitf; i1++)
{
nDifff = nArrf[i1] - nTestf;

	if (nDifff == 0)
	{
	nBelongsf = 1; //belongs
	nDimCurf = nDimInitf;

	break;
	} // if (nDifff > -fepsf && nDifff < fepsf)
	
} // for (i1 = 0; i1 < nDimInitf; i1++)

if (nBelongsf == 0)
{
//Updating
nDimCurf = nDimInitf + 1;
	if (nDimCurf > nDimMaxf)
	{
	printf("\n\nAn error in 'Int_Belongs_To_Arr_Or_Not_With_Update': nDimCurf = %d > nDimMaxf = %d", 
		nDimCurf,nDimMaxf);

	fprintf(fout,"\n\nAn error in 'Int_Belongs_To_Arr_Or_Not_With_Update': nDimCurf = %d > nDimMaxf = %d", 
		nDimCurf,nDimMaxf);

	getchar();	exit(1);
	} //if (nDimCurf > nDimMaxf)

nArrf[nDimInitf] = nTestf;
} // if (nBelongsf == 0)

return 1;
} // int Int_Belongs_To_Arr_Or_Not_With_Update(...

/////////////////////////////////////////////////////////////////////////////////////////////////////////
void Loss(
	const int nDim_Hf,
	const int nYtf, // 1 or -1 (not 0)

	const float fZ_Arrf[], //[nDim_Hf]

	const float fW_Arrf[], //[nDim_Hf]

	int &nY_Estimatedf, //0 or 1 (not -1 or 1)
	float &fLossf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);
	////////////////////////////////////////
	int
		nPrintedf = 0,
		iFea_Hf;

	float
		fProdf,
		fScalar_Prodf,
		fScalar_ProdfWithBiasf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Loss': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, nYtf = %d", iVec_Train_Glob, nY_Train_Actual_Glob, nYtf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	Scalar_Product(
		nDim_Hf, //const int nDimf,

		fZ_Arrf, //const float fFeas_Arr_1f[],

		fW_Arrf, //const float fFeas_Arr_2f[],
		fScalar_Prodf); // float &fScalar_Prodf);

	fScalar_ProdfWithBiasf = fScalar_Prodf + fBiasForClassifByLossFunction_Glob;

	//if (fScalar_Prodf < 0.0)
	if (fScalar_ProdfWithBiasf < 0.0)
	{
		nY_Estimatedf = 0; // not -1;
	}// if (fScalar_ProdfWithBiasf < 0.0)
	else
	{
		nY_Estimatedf = 1;
	}//else

	fProdf = (float)(nYtf)*fScalar_Prodf;

	if (fProdf >= 1.0)
	{
		fLossf = 0.0;
	}//if ( (float)(nYtf)*fScalar_Prodf >= 1.0)
	else
	{
		fLossf = 1.0 - fProdf;
	}//else
	
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'Loss': fLossf = %E, fScalar_Prodf = %E, nY_Estimatedf = %d", fLossf, fScalar_Prodf, nY_Estimatedf);

	if (nYtf == -1 && fLossf > 0.0)
	{
		fprintf(fout, "\n A loss for a neg vector");

		if (nPrintedf == 0)
		{
			for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
			{
				fprintf(fout, "\n fZ_Arrf[%d] = %E, fW_Arrf[%d] = %E", iFea_Hf, fZ_Arrf[iFea_Hf], iFea_Hf, fW_Arrf[iFea_Hf]);

			} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

			nPrintedf = 1;
		} //if (nPrintedf == 0)

	} //if (nYtf == -1 && fLossf > 0.0)

	if (nY_Train_Actual_Glob != nY_Estimatedf && nYtf == -1)
	{
		fprintf(fout, "\n A different classification in 'Loss' for a neg vector");

		if (nPrintedf == 0)
		{
			for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
			{
				fprintf(fout, "\n fZ_Arrf[%d] = %E, fW_Arrf[%d] = %E", iFea_Hf, fZ_Arrf[iFea_Hf], iFea_Hf, fW_Arrf[iFea_Hf]);

			} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

			//nPrintedf = 1;
		} //if (nPrintedf == 0)

	} //if (nY_Train_Actual_Glob != nY_Estimatedf  && nYtf == -1) 

#endif //#ifndef COMMENT_OUT_ALL_PRINTS


}// void Loss(...

//////////////////////////////////////////////////////////////////////////////////////////////

int Updating_W_Arr(
	const int nDim_Hf,

	const float fCf,
	const float fAlphaf, // < 1.0

	const float fLossf,

	const int nYtf, // 1 or -1

	const float fZ_Arrf[], //[nDim_Hf]

	const float fW_Init_Arrf[], //[nDim_Hf]

	float fW_Fin_Arrf[]) //[nDim_Hf]

{
	int NormEuclidean_Of_A_Vector(
		const int nDimf,
		const float fFeas_Arrf[],

		float &fNormEuclid_Of_A_Vectorf);

	int
		nResf,
		iFea_Hf;

	float
		fNormEuclid_Of_A_Vectorf,
		fTempf,

		fChange_For_fWf,
		fTauf;

	if (fLossf < 0.0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_W_Arr': fLossf = %E < 0.0", fLossf);
		fprintf(fout, "\n\n  An error in 'Updating_W_Arr': fLossf = %E < 0.0", fLossf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fLossf < 0.0)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_W_Arr': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, fLossf = %E", iVec_Train_Glob, nY_Train_Actual_Glob, fLossf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nResf = NormEuclidean_Of_A_Vector(
		nDim_Hf, //const int nDimf,
		fZ_Arrf, //const float fFeas_InitArrf[],

		fNormEuclid_Of_A_Vectorf); // float &fNormOfAVectorf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Updating_W_Arr' by 'NormEuclidean_Of_A_Vector' ");
			fprintf(fout, "\n\n  An error in 'Updating_W_Arr' by 'NormEuclidean_Of_A_Vector' ");
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (fNormEuclid_Of_A_Vectorf < eps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_W_Arr': fNormEuclid_Of_A_Vectorf = %E",fNormEuclid_Of_A_Vectorf);
		fprintf(fout, "\n\n  An error in 'Updating_W_Arr': fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fNormEuclid_Of_A_Vectorf < eps)
///////////////////////////
	fTempf = (1.0 - fAlphaf)*fLossf/(fNormEuclid_Of_A_Vectorf);

	if (fTempf < fCf)
	{
		fTauf = fTempf;
	}//if (fTempf < fCf)
	else
	{
		fTauf = fCf;
	}//else

///////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'Updating_W_Arr': fNormEuclid_Of_A_Vectorf = %E, fTauf = %E, nYtf = %d, fTempf = %E, fCf = %E, fAlphaf = %E",
		fNormEuclid_Of_A_Vectorf, fTauf, nYtf,fTempf, fCf, fAlphaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		fChange_For_fWf = fTauf * nYtf * fZ_Arrf[iFea_Hf];

		fW_Fin_Arrf[iFea_Hf] = fW_Init_Arrf[iFea_Hf] + fChange_For_fWf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n 'Updating_W_Arr': iFea_Hf = %d, fChange_For_fWf = %E, fW_Init_Arrf[iFea_Hf] = %E, fW_Fin_Arrf[iFea_Hf] = %E, fZ_Arrf[iFea_Hf] = %E",
			iFea_Hf, fChange_For_fWf, fW_Init_Arrf[iFea_Hf], fW_Fin_Arrf[iFea_Hf], fZ_Arrf[iFea_Hf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
}// int Updating_W_Arr(...

//////////////////////////////////////////////////////////////////////////////////////////////
int Updating_Z(
	const int nDim_Hf,

	const float fLossf,

	const int nYtf, // 1 or -1

	const float fW_Fin_Arrf[], //[nDim_Hf]

	const float fZ_Init_Arrf[], //[nDim_Hf]

	float fZ_Fin_Arrf[]) //[nDim_Hf]

{

	int NormEuclidean_Of_A_Vector(
		const int nDimf,
		const float fFeas_Arrf[],

		float &fNormEuclid_Of_A_Vectorf);

	int
		nResf,
		iFea_Hf;

	float
		fChangef,
		fNormEuclid_Of_A_Vectorf,
		fTauf;

	nResf = NormEuclidean_Of_A_Vector(
		nDim_Hf, //const int nDimf,
		fZ_Init_Arrf, //const float fFeas_InitArrf[],

		fNormEuclid_Of_A_Vectorf); // float &fNormOfAVectorf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_Z': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, nYtf = %d", iVec_Train_Glob, nY_Train_Actual_Glob, nYtf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_Z' by 'StDev_Of_A_Vector' ");
		fprintf(fout, "\n\n  An error in 'Updating_Z' by 'StDev_Of_A_Vector' ");
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (fNormEuclid_Of_A_Vectorf < eps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_Z': fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
		fprintf(fout, "\n\n  An error in 'Updating_Z': fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fNormEuclid_Of_A_Vectorf < eps)
/////////////////////////////////////////////////////
	fTauf = fLossf/ fNormEuclid_Of_A_Vectorf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_Z': fTauf = %E, fLossf = %E, fNormEuclid_Of_A_Vectorf = %E", fTauf, fLossf, fNormEuclid_Of_A_Vectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		fChangef = fTauf * nYtf * fW_Fin_Arrf[iFea_Hf];
		fZ_Fin_Arrf[iFea_Hf] = fZ_Init_Arrf[iFea_Hf] + fChangef;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n 'Updating_Z': iFea_Hf = %d, fChangef = %E, fZ_Init_Arrf[iFea_Hf] = %E, fZ_Fin_Arrf[iFea_Hf] = %E",
			iFea_Hf, fChangef, fZ_Init_Arrf[iFea_Hf], fZ_Fin_Arrf[iFea_Hf]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (fZ_Fin_Arrf[iFea_Hf] > fLarge || fZ_Fin_Arrf[iFea_Hf] < -fLarge)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Updating_Z': fZ_Fin_Arrf[iFea_Hf] > fLarge || ...");
			fprintf(fout, "\n\n  An error in 'Updating_Z': fZ_Fin_Arrf[iFea_Hf] > fLarge || ...");
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (fZ_Fin_Arrf[iFea_Hf] > fLarge || fZ_Fin_Arrf[iFea_Hf] < -fLarge)

	}//for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
}// int Updating_Z(...

//////////////////////////////////////////////////////////////////////////////////////////////
int Selecting_Hyperplane_For_OneFea_OfNonlinearSpace(
	const int nStrategyForSelectingHyperplanef, //1 or 2

	const int nDim_D_WithConstf, // = dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space

	const int nKf, // nNumOfHyperplanes per one fea in nonlinear space
	
	const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

	const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

	const float fX_Arrf[], //[nDim_D_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

	int &nHyperplaneSelectedf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	int
		iFeaf,

		nIndexf,

		nIndexMaxf = nDim_D_WithConstf * nDim_Hf * nKf - 1,
		iHyperplanef;

	float
			fZ_Targetf = fZ_Fin_Arrf[nFea_Hf],
		fScalar_Prodf,

		fDiffAbsolValueMinf = fLarge,

		fDiffAbsolValueCurf,

		fU_ForAFea_H_Arrf[nDim_D_WithConst];

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nStrategyForSelectingHyperplanef == 1) //hyperplane with max scalar prod
	{
		nHyperplaneSelectedf = nHyperplaneWithMaxScaProdArrf[nFea_Hf];

		if (nHyperplaneSelectedf < 0 || nHyperplaneSelectedf >= nKf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace' 1: nHyperplaneSelectedf = %d, nKf = %d", nHyperplaneSelectedf, nKf);

			fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace' 1: nHyperplaneSelectedf = %d, nKf = %d", nHyperplaneSelectedf, nKf);
			//getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nHyperplaneSelectedf < 0 || nHyperplaneSelectedf >= nKf)

		return SUCCESSFUL_RETURN;
	}//if (nStrategyForSelectingHyperplanef == 1)
	else if (nStrategyForSelectingHyperplanef == 2) //hyperplane with scalar prod closest to fZ_Fin_Arrf[nFea_Hf]
	{
		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
			{
				nIndexf = (nDim_D_WithConstf*nDim_Hf*iHyperplanef) + (nDim_D_WithConstf*nFea_Hf) + iFeaf;

				if (nIndexf > nIndexMaxf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': nIndexf = %d < nIndexMaxf = %d", nIndexf, nIndexMaxf);
					fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': nIndexf = %d < nIndexMaxf = %d", nIndexf, nIndexMaxf);

					//getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)

				fU_ForAFea_H_Arrf[iFeaf] = fU_Arrf[nIndexf];
			}//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

			Scalar_Product(
				nDim_D_WithConstf, //const int nDimf,

				fX_Arrf, //const float fFeas_Arr_1f[],

				fU_ForAFea_H_Arrf, //const float fFeas_Arr_2f[],
				fScalar_Prodf); // float &fScalar_Prodf);

			if (fScalar_Prodf > fZ_Targetf)
			{
				fDiffAbsolValueCurf = fScalar_Prodf - fZ_Targetf;

			} // if (fScalar_Prodf > fZ_Targetf)
			else
			{
				fDiffAbsolValueCurf = fZ_Targetf - fScalar_Prodf;
			}//else

			if (fDiffAbsolValueCurf < fDiffAbsolValueMinf)
			{
				fDiffAbsolValueMinf = fDiffAbsolValueCurf;

				nHyperplaneSelectedf = iHyperplanef;
			} // if (fDiffAbsolValueCurf < fDiffAbsolValueMinf)

		} //for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)

		if (nHyperplaneSelectedf < 0 || nHyperplaneSelectedf >= nKf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace' 2: nHyperplaneSelectedf = %d, nKf = %d", nHyperplaneSelectedf, nKf);

			fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace' 2: nHyperplaneSelectedf = %d, nKf = %d", nHyperplaneSelectedf, nKf);
			//getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nHyperplaneSelectedf < 0 || nHyperplaneSelectedf >= nKf)

		return SUCCESSFUL_RETURN;
	}//else if (nStrategyForSelectingHyperplanef == 2) //hyperplane with scalar prod closest to fZ_Fin_Arrf[nFea_Hf]
	else 
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': nStrategyForSelectingHyperplanef = %d is not equal to 1 or 2", nStrategyForSelectingHyperplanef);

		fprintf(fout, "\n\n An error in 'Selecting_Hyperplane_For_OneFea_OfNonlinearSpace': nStrategyForSelectingHyperplanef = %d is not equal to 1 or 2", nStrategyForSelectingHyperplanef);
		//getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//else

}// int Selecting_Hyperplane_For_OneFea_OfNonlinearSpace(...
////////////////////////////////////////////////////////////////////////////////////////////////

int Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace(
	const int nStrategyForSelectingHyperplanef, //1 or 2

	const int nDim_D_WithConstf, // = dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space

	const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

	const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

	const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

	const float fX_Arrf[], //[nDim_D_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

	int nHyperplaneSelected_For_Z_Arrf[]) //[nDim_Hf]
{
	int Selecting_Hyperplane_For_OneFea_OfNonlinearSpace(
		const int nStrategyForSelectingHyperplanef, //1 or 2

		const int nDim_D_WithConstf, // = dimension of the original space

		const int nDim_Hf, // dimension of nonlinear space

		const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

		const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

		const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

		const float fX_Arrf[], //[nDim_D_WithConstf]
		const float fZ_Fin_Arrf[], //[nDim_Hf]

		const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

		int &nHyperplaneSelectedf);

	int
		nResf,
		iFea_Hf,

		nHyperplaneSelectedf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nResf = Selecting_Hyperplane_For_OneFea_OfNonlinearSpace(
					nStrategyForSelectingHyperplanef, //const int nStrategyForSelectingHyperplanef, //1 or 2

					nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space

					nDim_Hf, //const int nDim_Hf, // dimension of nonlinear space

					nKf, //const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

					nFea_Hf, //const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

					nHyperplaneWithMaxScaProdArrf, //const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

					fX_Arrf, //const float fX_Arrf[], //[nDim_D_WithConstf]
					fZ_Fin_Arrf, //const float fZ_Fin_Arrf[], //[nDim_Hf]

					fU_Arrf, //const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

					nHyperplaneSelectedf); // int &nHyperplaneSelectedf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n An error in 'Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace' for iFea_Hf = %d", iFea_Hf);
			fprintf(fout, "\n\n An error in 'Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace' for iFea_Hf = %d", iFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		nHyperplaneSelected_For_Z_Arrf[iFea_Hf] = nHyperplaneSelectedf;
	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
} //Selecting_Hyperplanes_For_AllFeas_OfNonlinearSpace(...
////////////////////////////////////////////////////////////////////////////////////////////////

void LossEpsilon(
	const int nDim_D_WithConstf, // = dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space
	
	const int nHyperplaneSelectedf, //< nKf

	const float fX_Arrf[], //[nDim_D_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const float fU_ForAFea_H_Arrf[], // [nDim_D_WithConstf]

	const float fEpsilonf,

	float &fScalar_Prod_OfUij_and_Xf,
	float &fLossEpsf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	float	
		fZf = fZ_Fin_Arrf[nHyperplaneSelectedf],
		fDiff,
		fScalar_Prodf;
//////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'LossEpsilon': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	Scalar_Product(
		nDim_D_WithConstf, //const int nDimf,

		fX_Arrf, //const float fFeas_Arr_1f[],

		fU_ForAFea_H_Arrf, //const float fFeas_Arr_2f[],

		fScalar_Prod_OfUij_and_Xf); // float &fScalar_Prodf);

	if (fScalar_Prod_OfUij_and_Xf >= fZf)
	{
		fDiff = fScalar_Prod_OfUij_and_Xf - fZf;
	}//if ( (float)(nYtf)*fScalar_Prodf >= 1.0)
	else
	{
		fDiff = fZf - fScalar_Prod_OfUij_and_Xf;
	}//else
/////////////////////////////////////////////
	if (fDiff <= fEpsilonf)
	{
		fLossEpsf = 0.0;
	}//
	else
	{
		fLossEpsf = fDiff - fEpsilonf;
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'LossEpsilon': fScalar_Prod_OfUij_and_Xf = %E, fDiff = %E, fLossEpsf = %E, fEpsilonf = %E", fScalar_Prod_OfUij_and_Xf, fDiff, fLossEpsf, fEpsilonf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

}// void LossEpsilon(...
////////////////////////////////////////////////////////////////////////////////////////////////

// i == nFea_Hf, j == nHyperplanef
int Updating_Uij( 

	const int nDim_D_WithConstf, // = dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space

	const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

	const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf
	const int nHyperplanef, //the number of a specific hyperplane of the specific nonlinear fea; < nKf

	//const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

	const float fX_Arrf[], //[nDim_D_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

	const float fEpsilonf,
	const float fCrf,

	///////////////////////////////////////////////////////
	float fU_Arrf[]) // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes
{
/*
	int StDev_Of_A_Vector(
		const int nDimf,
		const float fFeas_InitArrf[],

		float &StDev_Of_A_Vectorf);
*/

	int NormEuclidean_Of_A_Vector(
		const int nDimf,
		const float fFeas_Arrf[],

		float &fNormEuclid_Of_A_Vectorf);

	void LossEpsilon(
		const int nDim_D_WithConstf, // = dimension of the original space

		const int nDim_Hf, // dimension of nonlinear space

		const int nHyperplaneSelectedf, //< nKf

		const float fX_Arrf[], //[nDim_D_WithConstf]
		const float fZ_Fin_Arrf[], //[nDim_Hf]

		const float fU_ForAFea_H_Arrf[], // [nDim_D_WithConst]

		const float fEpsilonf,

		float &fScalar_Prod_OfUij_and_Xf,
		float &fLossEpsf);

	int
		nResf,
		nIndexf,
		nIndexMaxf = (nDim_D_WithConstf * nDim_Hf * nKf) - 1,

		nSignf,
		nHyperplaneSelected_For_Zf, // = nHyperplaneSelected_For_Z_Arrf[nFea_Hf], // < nKf

		nTempf,

		iFeaf;

	float
		fRatio_OfLossAndSquaredNormf,

		fZf, // = fZ_Fin_Arrf[nHyperplaneSelected_For_Zf],

		fScalar_Prod_OfUij_and_Xf,
		fLossEpsf,

		fChangef,
		fU_Prevf,

		fNormEuclid_Of_An_X_Vectorf,
		fU_ForAFea_H_Arrf[nDim_D_WithConst],

		fTauf;

	if (nHyperplanef < 0 || nHyperplanef >= nKf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_Uij': nHyperplanef = %d >= nKf = %d", nHyperplanef, nKf);
		fprintf(fout, "\n\n An error in 'Updating_Uij': nHyperplanef = %d >= nKf = %d", nHyperplanef, nKf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nHyperplanef < 0 || nHyperplanef >= nKf)

////////////////////////////////////////////////////////////////////////////////////////////
	if (nFea_Hf < 0 || nFea_Hf >= nDim_Hf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_Uij': nFea_Hf = %d >= nDim_Hf = %d", nFea_Hf, nDim_Hf);
		fprintf(fout, "\n\n An error in 'Updating_Uij': nFea_Hf = %d >= nDim_Hf = %d", nFea_Hf, nDim_Hf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nFea_Hf < 0 || nFea_Hf >= nDim_Hf)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_Uij': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, nFea_Hf = %d, nHyperplanef = %d", 
		iVec_Train_Glob, nY_Train_Actual_Glob, nFea_Hf, nHyperplanef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	///////////////////////////////////////////////
	nResf = NormEuclidean_Of_A_Vector(
		nDim_D_WithConstf, //const int nDimf,
		fX_Arrf, //const float fFeas_InitArrf[],

		fNormEuclid_Of_An_X_Vectorf); // float &fNormOfAVectorf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_Uij' by 'StDev_Of_A_Vector' ");
		fprintf(fout, "\n\n  An error in 'Updating_Uij' by 'StDev_Of_A_Vector' ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	if (fNormEuclid_Of_An_X_Vectorf < eps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_Uij': fNormEuclid_Of_An_X_Vectorf = %E", fNormEuclid_Of_An_X_Vectorf);
		fprintf(fout, "\n\n  An error in 'Updating_Uij': fNormEuclid_Of_An_X_Vectorf = %E", fNormEuclid_Of_An_X_Vectorf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fNormEuclid_Of_An_X_Vectorf < eps)

	///////////////////////////////////////////////////////////////
	nHyperplaneSelected_For_Zf = nHyperplaneSelected_For_Z_Arrf[nFea_Hf]; // < nKf

	if (nHyperplaneSelected_For_Zf < 0 || nHyperplaneSelected_For_Zf >= nKf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_Uij': nHyperplaneSelected_For_Zf = %d >= nKf = %d", nHyperplaneSelected_For_Zf, nKf);
		fprintf(fout, "\n\n An error in 'Updating_Uij': nHyperplaneSelected_For_Zf = %d >= nKf = %d", nHyperplaneSelected_For_Zf, nKf);

		//getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nHyperplaneSelected_For_Zf < 0 || nHyperplaneSelected_For_Zf >= nKf)

	if (nHyperplaneSelected_For_Zf != nHyperplanef)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Updating_Uij': nHyperplaneSelected_For_Zf = %d != nHyperplanef = %d", nHyperplaneSelected_For_Zf, nHyperplanef);
		fprintf(fout, "\n\n An error in 'Updating_Uij': nHyperplaneSelected_For_Zf = %d != nHyperplanef = %d", nHyperplaneSelected_For_Zf, nHyperplanef);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nHyperplaneSelected_For_Zf != nHyperplanef)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'Updating_Uij': fNormEuclid_Of_An_X_Vectorf = %E, nHyperplaneSelected_For_Zf = %d, nFea_Hf = %d", fNormEuclid_Of_An_X_Vectorf, nHyperplaneSelected_For_Zf, nFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////
	fZf = fZ_Fin_Arrf[nHyperplaneSelected_For_Zf];

	//////////////////////////////////////////////////////////////////////////////////////
	nTempf = (nFea_Hf*nDim_D_WithConstf) + (nHyperplanef*nDim_D_WithConstf*nDim_Hf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n'Updating_Uij': nFea_Hf = %d, nHyperplanef = %d, fZf = %E, nTempf = %d",nFea_Hf, nHyperplanef, fZf, nTempf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
	{
		//nIndexf = (nDim_D_WithConstf*nDim_Hf*nHyperplaneSelected_For_Zf) + (nDim_D_WithConstf*nFea_Hf) + iFeaf;
//	nIndexf = iFeaf + nProd_nFea_Hf_nDim_D_WithConstf + nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf;

		nIndexf = nTempf + iFeaf;

		if (nIndexf > nIndexMaxf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Updating_Uij': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			fprintf(fout, "\n\n An error in 'Updating_Uij': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

		fU_ForAFea_H_Arrf[iFeaf] = fU_Arrf[nIndexf];

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n'Updating_Uij': fU_ForAFea_H_Arrf[%d] = %E = fU_Arrf[nIndexf], nIndexf = %d", iFeaf, fU_ForAFea_H_Arrf[iFeaf], nIndexf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

/////////////////////////////
	LossEpsilon(
		nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space

		nDim_Hf, //const int nDim_Hf, // dimension of nonlinear space

		nHyperplaneSelected_For_Zf, //const int nHyperplaneSelected_For_Zf, //< nKf

		fX_Arrf, //const float fX_Arrf[], //[nDim_D_WithConstf]
		fZ_Fin_Arrf, //const float fZ_Fin_Arrf[], //[nDim_Hf]

		fU_ForAFea_H_Arrf, //const float fU_ForAFea_H_Arrf[], // [nDim_D_WithConst]

		fEpsilonf, //const float fEpsilonf,

		fScalar_Prod_OfUij_and_Xf, //float &fScalar_Prod_OfUij_and_Xf,
		fLossEpsf); // float &fLossEpsf);

	///////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n'Updating_Uij' (after LossEpsilon): fLossEpsf = %E, fScalar_Prod_OfUij_and_Xf = %E, fNormEuclid_Of_An_X_Vectorf = %E", 
		fLossEpsf, fScalar_Prod_OfUij_and_Xf, fNormEuclid_Of_An_X_Vectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


	fRatio_OfLossAndSquaredNormf = fLossEpsf / (fNormEuclid_Of_An_X_Vectorf);

	if (fRatio_OfLossAndSquaredNormf < fCrf)
	{
		fTauf = fRatio_OfLossAndSquaredNormf;
	}//if (fRatio_OfLossAndSquaredNormf < fCf)
	else
	{
		fTauf = fCrf;
	}//else
//////////////////////////////////////////
	if (fZf >= fScalar_Prod_OfUij_and_Xf)
	{
		nSignf = 1;
	} // if (fZf >= fScalar_Prod_OfUij_and_Xf)
	else
	{
		nSignf = -1;
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n'Updating_Uij': fRatio_OfLossAndSquaredNormf = %E, fCrf = %E, fTauf = %E, nSignf = %d", fRatio_OfLossAndSquaredNormf, fCrf, fTauf, nSignf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

////////////////////////////////////////////////////
	nTempf = (nFea_Hf*nDim_D_WithConstf) + (nHyperplanef*nDim_D_WithConstf*nDim_Hf);
	for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
	{
		//nIndexf = (nDim_D_WithConstf*nDim_Hf*nHyperplaneSelected_For_Zf) + (nDim_D_WithConstf*nFea_Hf) + iFeaf;
		nIndexf = nTempf + iFeaf;

		fChangef = nSignf * fTauf*fX_Arrf[iFeaf];

		fU_Prevf = fU_Arrf[nIndexf];

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n Before updating: fU_Arrf[nIndexf] = fU_Prevf = %E, nSignf = %d, fTauf = %E, iFeaf = %d, nIndexf = %d",
			fU_Arrf[nIndexf], nSignf, fTauf, iFeaf, nIndexf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fU_Arrf[nIndexf] = fU_Prevf + fChangef;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n After updating: a new fU_Arrf[nIndexf] = %E, fU_Prevf = %E, fX_Arrf[%d] = %E, fChangef = %E", 
				fU_Arrf[nIndexf], fU_Prevf, iFeaf,fX_Arrf[iFeaf],fChangef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)


	return SUCCESSFUL_RETURN;
}// int Updating_Uij(...
//////////////////////////////////////////////////////////////////////////////////////////////

int Updating_U_ForSelectedHyperplanes_Arr(

	const int nDim_D_WithConstf, // = dimension of the original space

	const int nDim_Hf, // dimension of nonlinear space

	const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

	const float fX_Arrf[], //[nDim_D_WithConstf]
	const float fZ_Fin_Arrf[], //[nDim_Hf]

	const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

	const float fEpsilonf,
	const float fCrf,

	///////////////////////////////////////////////////////
	float fU_Arrf[]) // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes
{
	// i == nFea_Hf, j == nHyperplanef
	int Updating_Uij(

		const int nDim_D_WithConstf, // = dimension of the original space

		const int nDim_Hf, // dimension of nonlinear space

		const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

		const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf
		const int nHyperplanef, //the number of a specific hyperplane of the specific nonlinear fea; < nKf

		//const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

		const float fX_Arrf[], //[nDim_D_WithConstf]
		const float fZ_Fin_Arrf[], //[nDim_Hf]

		const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

		const float fEpsilonf,
		const float fCrf,

		///////////////////////////////////////////////////////
		float fU_Arrf[]); // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

	int
		nResf,
		iFea_Hf, //nonlinear
		//iFea_Hyperplanef;
		nFea_Hyperplanef;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Updating_U_ForSelectedHyperplanes_Arr': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		//for (iFea_Hyperplanef = 0; iFea_Hyperplanef < nKf; iFea_Hyperplanef++)
		nFea_Hyperplanef = nHyperplaneSelected_For_Z_Arrf[iFea_Hf];

		if (nFea_Hyperplanef < 0 || nFea_Hyperplanef > nKf - 1)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Updating_U_ForSelectedHyperplanes_Arr': nFea_Hyperplanef = %d, nKf - 1 = %d", nFea_Hyperplanef, nKf - 1);
			fprintf(fout, "\n\n An error in 'Updating_Uij': nFea_Hyperplanef = %d, nKf - 1 = %d", nFea_Hyperplanef, nKf - 1);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nFea_Hyperplanef < 0 || nFea_Hyperplanef > nKf - 1)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 'Updating_U_ForSelectedHyperplanes_Arr': iFea_Hf = %d, nFea_Hyperplanef = %d", iFea_Hf, nFea_Hyperplanef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nResf = Updating_Uij(

				nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space

				nDim_Hf, //const int nDim_Hf, // dimension of nonlinear space

				nKf, //const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

				iFea_Hf, //const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf
				nFea_Hyperplanef, //const int nHyperplanef, //the number of a specific hyperplane of the specific nonlinear fea; < nKf

				//const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

				fX_Arrf, //const float fX_Arrf[], //[nDim_D_WithConstf]
				fZ_Fin_Arrf, //const float fZ_Fin_Arrf[], //[nDim_Hf]

				nHyperplaneSelected_For_Z_Arrf, //const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

				fEpsilonf, //const float fEpsilonf,
				fCrf, //const float fCrf,

				///////////////////////////////////////////////////////
				fU_Arrf); // float fU_Arrf[]); // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Updating_U_ForSelectedHyperplanes_Arr': iFea_Hf = %d, nFea_Hyperplanef = %d", iFea_Hf, nFea_Hyperplanef);
				fprintf(fout, "\n\n An error in 'Updating_Uij': iFea_Hf = %d, nFea_Hyperplanef = %d", iFea_Hf, nFea_Hyperplanef);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nResf == UNSUCCESSFUL_RETURN)

	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
} // int Updating_U_ForSelectedHyperplanes_Arr(...

//////////////////////////////////////////////////////////////////////////////////////////////
void Scalar_Product(
	const int nDimf,

	const float fFeas_Arr_1f[],

	const float fFeas_Arr_2f[],
	float &fScalar_Prodf)
{
	int
		iFeaf;

	fScalar_Prodf = 0.0;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fScalar_Prodf += fFeas_Arr_1f[iFeaf] * fFeas_Arr_2f[iFeaf];

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

}// void Scalar_Product(...
/////////////////////////////////////////////////////////////////////////////////////////////

int OneFea_OfNonlinearSpace(
	const int nDim_D_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

	const float fX_Arrf[], //[nDim_D_WithConstf]

	const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes
	
	int &nHyperplaneWithMaxScaProdf,
	float &fZf)
{
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	int
		iFeaf,

		nIndexf,

		nIndexMaxf = nDim_D_WithConstf * nDim_Hf * nKf - 1,

		nProd_nFea_Hf_nDim_D_WithConstf = nFea_Hf*nDim_D_WithConstf,

		nProd_nDim_D_WithConstf_nDim_Hf = nDim_D_WithConstf * nDim_Hf,

		nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf,

		iHyperplanef;

	float
		fSumTempf = 0.0,
		fProdTempf,

		//fX_WithConstArrf[nDim_D_WithConst],
		fScalar_Prodf,
		fU_ForAFea_H_Arrf[nDim_D_WithConst];

	nHyperplaneWithMaxScaProdf = -1; //initially only
	fZf = -fLarge;

	if (nFea_Hf < 0 || nFea_Hf >= nDim_Hf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'OneFea_OfNonlinearSpace': nFea_Hf = %d >= nDim_Hf = %d", nFea_Hf, nDim_Hf);
		fprintf(fout, "\n\n An error in 'OneFea_OfNonlinearSpace': nFea_Hf = %d >= nDim_Hf = %d", nFea_Hf, nDim_Hf);

		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} // if (nFea_Hf < 0 || nFea_Hf >= nDim_Hf)


#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'OneFea_OfNonlinearSpace': nFea_Hf = %d", nFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'OneFea_OfNonlinearSpace': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	//nFea_Hf is fixed
	for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
	{
		nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf = nProd_nDim_D_WithConstf_nDim_Hf * iHyperplanef;

		for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
		{
	
//			nIndexf = iFeaf + (nFea_Hf*nDim_D_WithConstf) + (nDim_D_WithConstf*nDim_Hf*iHyperplanef);
			nIndexf = iFeaf + nProd_nFea_Hf_nDim_D_WithConstf + nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf;

			if (nIndexf > nIndexMaxf)
			{
	#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OneFea_OfNonlinearSpace': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
				fprintf(fout, "\n\n An error in 'OneFea_OfNonlinearSpace': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
				getchar();	exit(1);
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexf > nIndexMaxf)

		fU_ForAFea_H_Arrf[iFeaf] = fU_Arrf[nIndexf];

		fProdTempf = fU_ForAFea_H_Arrf[iFeaf] * fX_Arrf[iFeaf];
		fSumTempf += fProdTempf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 'OneFea_OfNonlinearSpace': iFeaf = %d, nIndexf = %d, iHyperplanef = %d, nFea_Hf = %d", 
			 iFeaf, nIndexf,iHyperplanef, nFea_Hf);

		fprintf(fout, "\n fU_ForAFea_H_Arrf[%d] = %E, fX_Arrf[%d] = %E, fProdTempf = %E, fSumTempf = %E",iFeaf, fU_ForAFea_H_Arrf[iFeaf], iFeaf, fX_Arrf[iFeaf], fProdTempf, fSumTempf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		}//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

		Scalar_Product(
			nDim_D_WithConstf, //const int nDimf,

			fX_Arrf, //const float fFeas_Arr_1f[],

			fU_ForAFea_H_Arrf, //const float fFeas_Arr_2f[],
			fScalar_Prodf); // float &fScalar_Prodf);

		if (fScalar_Prodf > fZf)
		{
			nHyperplaneWithMaxScaProdf = iHyperplanef;
			fZf = fScalar_Prodf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'OneFea_OfNonlinearSpace': iFeaf = %d, nIndexf = %d, iHyperplanef = %d, nFea_Hf = %d",
				iFeaf, nIndexf, iHyperplanef, nFea_Hf);
			fprintf(fout, "\n fU_ForAFea_H_Arrf[%d] = %E, fX_Arrf[%d] = %E, fProdTempf = %E", iFeaf, fU_ForAFea_H_Arrf[iFeaf], iFeaf, fX_Arrf[iFeaf], fProdTempf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} // if (fScalar_Prodf > fZf)

	} //for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)

	return SUCCESSFUL_RETURN;
}//int OneFea_OfNonlinearSpace(
///////////////////////////////////////////////////////////////////////////////////////////////

int All_Feas_OfNonlinearSpace(
	const int nDim_D_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space == 5

	const int nKf, //nNumOfHyperplanes == 3

	const float fX_Arrf[], //[nDim_D_WithConstf]// == 4

	const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 3 // the number of hyperplanes

	int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]

	float fZ_Arrf[]) //[nDim_H]

{
	int OneFea_OfNonlinearSpace(
		const int nDim_D_WithConstf, // = dimension of the original space == 4
		const int nDim_Hf, //dimension of the nonlinear/transformed space == 5

		const int nKf, //nNumOfHyperplanes == 3
		const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

		const float fX_Arrf[], //[nDim_D_WithConstf]

		const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 3 // the number of hyperplanes

		int &nHyperplaneWithMaxScaProdf,

		float &fZf);

	int
		nResf,
		nHyperplaneWithMaxScaProdf,
		iFea_Hf;

	float
		fZf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n 'All_Fefs_OfNonlinearSpace':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'All_Fefs_OfNonlinearSpace': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nResf = OneFea_OfNonlinearSpace(
			nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
			nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

			nKf, //const int nKf, //nNumOfHyperplanes
			iFea_Hf, //const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf

			fX_Arrf, //const float fX_Arrf[], //[nDim_D_WithConstf]

			fU_Arrf, //const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 3 // the number of hyperplanes

			nHyperplaneWithMaxScaProdf, //int &nHyperplaneWithMaxScaProdf,
			fZf); // float &fZf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n An error in 'All_Fefs_OfNonlinearSpace' for iFea_Hf = %d", iFea_Hf);
			fprintf(fout, "\n\n An error in 'All_Fefs_OfNonlinearSpace' for iFea_Hf = %d", iFea_Hf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nHyperplaneWithMaxScaProdf == -1)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'All_Fefs_OfNonlinearSpace' for iFea_Hf = %d, nHyperplaneWithMaxScaProdf == -1", iFea_Hf);
			fprintf(fout, "\n\n An error in 'All_Fefs_OfNonlinearSpace' for iFea_Hf = %d, nHyperplaneWithMaxScaProdf == -1", iFea_Hf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nHyperplaneWithMaxScaProdf == -1)

		nHyperplaneWithMaxScaProdArrf[iFea_Hf] = nHyperplaneWithMaxScaProdf;

		fZ_Arrf[iFea_Hf] = fZf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n 'All_Fefs_OfNonlinearSpace': fZ_Arrf[%d] = %E, nHyperplaneWithMaxScaProdArrf[%d] = %d", 
			iFea_Hf, fZ_Arrf[iFea_Hf], iFea_Hf, nHyperplaneWithMaxScaProdArrf[iFea_Hf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
}//int All_Feas_OfNonlinearSpace(...

///////////////////////////////////////////////////////////////////////////////////////////
int Rand_Vec_Selec(
	const int nDimf,
	const int nVecInitf,
	const int nVecSelecf, // < nVecInitf
	const float fFeaAll_InitArrf[],

	float fFeaSelecArr[])
{
	int
		nRanSelecf, // <= nVecSelecf
		iFeaf,
		iVecf;

	for (iVecf = 0; iVecf < nVecSelecf; iVecf++)
	{
		nRanSelecf = (int)(nVecInitf*(float)(rand()) / (float)(RAND_MAX));

		if (nRanSelecf == nVecInitf)
		{
			nRanSelecf = nVecInitf - 1;
		} //if (nRanSelecf == nVecInitf)
		else if (nRanSelecf > nVecInitf)
		{
			printf("\n\nAn error in 'Rand_Vec_Selec': nRanSelecf = %d > nVecInitf = %d",
				nRanSelecf, nVecInitf);
			fprintf(fout, "\n\nAn error in 'Rand_Vec_Selec': nRanSelecf = %d > nVecInitf = %d",
				nRanSelecf, nVecInitf);

			getchar();	exit(1);
		} // else if (nRanSelecf > nVecInitf)

		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
			fFeaSelecArr[iFeaf + (iVecf*nDimf)] = fFeaAll_InitArrf[iFeaf + (nRanSelecf*nDimf)];
		} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	} // for (iVecf = 0; iVecf < nVecSelecf; iVecf++)

	return 1;
} //int Rand_Vec_Selec(...

int Rand_Selec_Of_2_Int(
	const int nDimf,
	const int nNumOfRandSelecCouplesf,
	const int nNumOfRandInitializingLoopsMaxf,

	int nPositOf1stIntArrf[],  //nNumOfRandSelecCouplesf
	int nPositOf2ndIntArrf[])
{
	int
		nDimLargerf = 3 * nDimf,

		nCounterOfValidCouplesf = 0,

		nRanSelecCurf,
		nRanSelec1stf, // <= nVecSelecf
		nRanSelec2ndf, // <= nVecSelecf

		nNumOfRandInitializingLoopsf = 0,
		i1;

	printf("\n\nInside'Rand_Selec_Of_2_Int' 1"); fflush(stdout);

	int *nRanSelec1stArrf = new int[nDimLargerf];
	int *nRanSelec2ndArrf = new int[nDimLargerf];

	printf("\n\nInside'Rand_Selec_Of_2_Int' 2"); fflush(stdout);

MarkInitializing: nNumOfRandInitializingLoopsf += 1;

	for (i1 = 0; i1 < nDimLargerf; i1++)
	{
		nRanSelecCurf = (int)(nDimf*(float)(rand()) / (float)(RAND_MAX));

		if (nRanSelecCurf == nDimf)
		{
			nRanSelecCurf = nDimf - 1;
		} //if (nRanSelecCurf == nDimf)
		else if (nRanSelecCurf > nDimf || nRanSelecCurf < 0)
		{
			printf("\n\nAn error in 'Rand_Selec_Of_2_Int': nRanSelecCurf = %d > nDimf = %d, ...",
				nRanSelecCurf, nDimf);
			fprintf(fout, "\n\nAn error in 'Rand_Selec_Of_2_Int': nRanSelecCurf = %d > nDimf = %d, ...",
				nRanSelecCurf, nDimf);

			getchar();	exit(1);
		} // else if (nRanSelecCurf > nDimf || ...)

		nRanSelec1stArrf[i1] = nRanSelecCurf;
	} //for (i1 = 0; i1 < nDimLargerf; i1++)

	for (i1 = 0; i1 < nDimLargerf; i1++)
	{
		nRanSelecCurf = (int)(nDimf*(float)(rand()) / (float)(RAND_MAX));

		if (nRanSelecCurf == nDimf)
		{
			nRanSelecCurf = nDimf - 1;
		} //if (nRanSelecCurf == nDimf)
		else if (nRanSelecCurf > nDimf || nRanSelecCurf < 0)
		{
			printf("\n\nAn error in 'Rand_Selec_Of_2_Int': nRanSelecCurf = %d > nDimf = %d, ...",
				nRanSelecCurf, nDimf);
			fprintf(fout, "\n\nAn error in 'Rand_Selec_Of_2_Int': nRanSelecCurf = %d > nDimf = %d, ...",
				nRanSelecCurf, nDimf);

			getchar();	exit(1);
		} // else if (nRanSelecCurf > nDimf || ...)

		nRanSelec2ndArrf[i1] = nRanSelecCurf;
	} //for (i1 = 0; i1 < nDimLargerf; i1++)

	printf("\n\nInside'Rand_Selec_Of_2_Int' 3"); fflush(stdout);

	//It is assumed that  'nRanSelec1stArrf[i1] < nRanSelec2ndArrf[i1']
	for (i1 = 0; i1 < nDimLargerf; i1++)
	{
		nRanSelec1stf = nRanSelec1stArrf[i1];
		nRanSelec2ndf = nRanSelec2ndArrf[i1];

		if (nRanSelec1stf != nRanSelec2ndf)
		{
			nCounterOfValidCouplesf += 1;
			if (nRanSelec1stf > nRanSelec2ndf) //must be vice versa
			{
				nPositOf1stIntArrf[nCounterOfValidCouplesf - 1] = nRanSelec2ndf;
				nPositOf2ndIntArrf[nCounterOfValidCouplesf - 1] = nRanSelec1stf;
			} //if (nRanSelec1stf > nRanSelec2ndf) //must be vice versa
			else //the order is preserved
			{
				nPositOf1stIntArrf[nCounterOfValidCouplesf - 1] = nRanSelec1stf;
				nPositOf2ndIntArrf[nCounterOfValidCouplesf - 1] = nRanSelec2ndf;
			} //the order is preserved

			if (nCounterOfValidCouplesf == nNumOfRandSelecCouplesf)
				break;
		} //if (nRanSelec1stf != nRanSelec2ndf)

	} // for (i1 = 0; i1 < nDimLargerf; i1++)

	printf("\n\nInside'Rand_Selec_Of_2_Int' 4, nCounterOfValidCouplesf = %d", nCounterOfValidCouplesf); fflush(stdout);

	if (nCounterOfValidCouplesf < nNumOfRandSelecCouplesf)
	{
		if (nNumOfRandInitializingLoopsf < nNumOfRandInitializingLoopsMaxf)
			goto MarkInitializing;
		else
		{
			printf("\n\nAn error in 'Rand_Selec_Of_2_Int': nNumOfRandInitializingLoopsf = %d >= nNumOfRandInitializingLoopsMaxf = %d",
				nNumOfRandInitializingLoopsf, nNumOfRandInitializingLoopsMaxf);
			fprintf(fout, "\n\nAn error in 'Rand_Selec_Of_2_Int': nNumOfRandInitializingLoopsf = %d >= nNumOfRandInitializingLoopsMaxf = %d",
				nNumOfRandInitializingLoopsf, nNumOfRandInitializingLoopsMaxf);

			getchar();	exit(1);
		} //else

	}// if (nCounterOfValidCouplesf < nNumOfRandSelecCouplesf)
	else if (nCounterOfValidCouplesf > nNumOfRandSelecCouplesf)
	{
		printf("\n\nAn error in 'Rand_Selec_Of_2_Int': nCounterOfValidCouplesf = %d > nNumOfRandSelecCouplesf = %d, nNumOfRandInitializingLoopsf = %d",
			nCounterOfValidCouplesf, nNumOfRandSelecCouplesf, nNumOfRandInitializingLoopsf);
		fprintf(fout, "\n\nAn error in 'Rand_Selec_Of_2_Int': nCounterOfValidCouplesf = %d > nNumOfRandSelecCouplesf = %d, nNumOfRandInitializingLoopsf = %d",
			nCounterOfValidCouplesf, nNumOfRandSelecCouplesf, nNumOfRandInitializingLoopsf);

		getchar();	exit(1);
	} //else if (nCounterOfValidCouplesf > nNumOfRandSelecCouplesf)

	printf("\n\nInside'Rand_Selec_Of_2_Int' 5, nCounterOfValidCouplesf = %d", nCounterOfValidCouplesf); fflush(stdout);

	delete[] nRanSelec1stArrf;
	delete[] nRanSelec2ndArrf;

	printf("\n\nInside'Rand_Selec_Of_2_Int' 6, nCounterOfValidCouplesf = %d", nCounterOfValidCouplesf); fflush(stdout);

	return 1;
} //int Rand_Selec_Of_2_Int(...

///////////////////////////////////////////////////////////////////////////////
void RandVec(
	const int nDimForRandf,

	const float fMinf,
	const float fMaxf,

	float fRandArr[]) //[nDimForRandf]
{
	int
		iPositionf;

	float
		fRangef = fMaxf - fMinf, 

		fValueRandCurf;

	for (iPositionf = 0; iPositionf < nDimForRandf; iPositionf++)
	{
		fValueRandCurf = (float)(rand()) / (float)(RAND_MAX); // from 0.0 to 1.0

		fRandArr[iPositionf] = (fValueRandCurf* fRangef) + fMinf;
	} // for (iPositionf = 0; iPositionf < nDimForRandf; iPositionf++)

} // void RandVec(...

///////////////////////////////////////////////////////////////////////////
void Initializing_W_Arr(
		const int nDim_Hf,
		const float fW_Init_Min_Globf,
		const float fW_Init_Max_Globf,

		float fW_Arrf[])
{
	void RandVec(
		const int nDimForRandf,

		const float fMinf,
		const float fMaxf,

		float fRandArr[]); //[nDimForRandf]

	RandVec(
		nDim_Hf, //const int nDimForRandf,

		fW_Init_Min_Globf, //const float fMinf,
		fW_Init_Max_Globf, //const float fMaxf,

		fW_Arrf); // float fRandArr[]); //[nDimForRandf]

}//void Initializing_W_Arr(...

/////////////////////////////////////////////////////////////////////////////////
void Initializing_U_Arr(
	const int nDim_Uf,
	const float fU_Init_Min_Globf,
	const float fU_Init_Max_Globf,

	float fU_Arrf[])
{
	void RandVec(
		const int nDimForRandf,

		const float fMinf,
		const float fMaxf,

		float fRandArr[]); //[nDimForRandf]

	RandVec(
		nDim_Uf, //const int nDimForRandf,

		fU_Init_Min_Globf, //const float fMinf,
		fU_Init_Max_Globf, //const float fMaxf,

		fU_Arrf); // float fRandArr[]); //[nDimForRandf]

}//void InitializingU_Arr(...

///////////////////////////////////////////////////////////////////////////////////////
int Normalizing_A_Vector_For_ARange(
	const int nDimf,
	const float fFin_Minf,
	const float fFin_Maxf,

	const float fFeas_InitArrf[],

	float fFeas_NormArrf[])
{
	int
		iFeaf;
	float
		fDiffMax_Min_Finf = fFin_Maxf - fFin_Minf,

		fDiffMax_Min_Initf,

		fRatioOfDiff,

		fMinf = fLarge,
		fMaxf = -fLarge;

	if (fDiffMax_Min_Finf < eps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Normalizing_A_Vector_For_ARange': fFin_Maxf = %E - fFin_Minf = %E < eps = %E", fFin_Maxf, fFin_Minf, eps);
		fprintf(fout, "\n\n An error in 'Vec_Normalization': fFin_Maxf = %E - fFin_Minf = %E < eps = %E", fFin_Maxf, fFin_Minf, eps);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fDiffMax_Min_Initf < eps)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		if (fFeas_InitArrf[iFeaf] < fMinf)
			fMinf = fFeas_InitArrf[iFeaf];

		if (fFeas_InitArrf[iFeaf] > fMaxf)
			fMaxf = fFeas_InitArrf[iFeaf];

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	fDiffMax_Min_Initf = fMaxf - fMinf;

	if (fDiffMax_Min_Initf < eps)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Normalizing_A_Vector_For_ARange': fMaxf = %E - fMinf = %E < eps = %E", fMaxf, fMinf, eps);
		fprintf(fout, "\n\n An error in 'Vec_Normalization': fMaxf = %E - fMinf = %E < eps = %E", fMaxf, fMinf, eps);
		#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (fDiffMax_Min_Initf < eps)

//to (0, fDiffMax_Min_Initf)
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeas_NormArrf[iFeaf] = fFeas_InitArrf[iFeaf] - fMinf;

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

//to (0, fDiffMax_Min_Finf)
	fRatioOfDiff = fDiffMax_Min_Finf / fDiffMax_Min_Initf;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeas_NormArrf[iFeaf] = fFeas_InitArrf[iFeaf]* fRatioOfDiff;

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)


//to (fFin_Minf, fFin_Maxf)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeas_NormArrf[iFeaf] = fFeas_InitArrf[iFeaf] - fFin_Minf;

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;

} //int Normalizing_A_Vector_For_ARange (...

/////////////////////////////////////////////////////////////////////////////////
int StDev_Of_A_Vector(
	const int nDimf,
	const float fFeas_InitArrf[],

	float &StDev_Of_A_Vectorf)
{
	int
		iFeaf;

	if (nDimf < 2 || nDimf > nLarge)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'StDev_Of_A_Vector': nDimf = %d", nDimf);
		fprintf(fout, "\n\n  An error in 'StDev_Of_A_Vector':  nDimf = %d", nDimf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nDimf < 2 || nDimf > nLarge)

////////////////////////////////////////////
	StDev_Of_A_Vectorf = 0.0;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		StDev_Of_A_Vectorf += fFeas_InitArrf[iFeaf] * fFeas_InitArrf[iFeaf];
	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	StDev_Of_A_Vectorf = sqrt(StDev_Of_A_Vectorf/ nDimf);

	if (StDev_Of_A_Vectorf < eps || StDev_Of_A_Vectorf > fLarge)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'StDev_Of_A_Vector': StDev_Of_A_Vectorf = %E", StDev_Of_A_Vectorf);
		fprintf(fout, "\n\n  An error in 'StDev_Of_A_Vector': StDev_Of_A_Vectorf = %E", StDev_Of_A_Vectorf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return UNSUCCESSFUL_RETURN;
	} //if (StDev_Of_A_Vectorf < eps || StDev_Of_A_Vectorf > fLarge)
	else
		return SUCCESSFUL_RETURN;

} //int StDev_Of_A_Vector (...
////////////////////////////////////////////////////////////

int NormEuclidean_Of_A_Vector(
	const int nDimf,
	const float fFeas_Arrf[],

	float &fNormEuclid_Of_A_Vectorf)
{
	int
		iFeaf;

	if (nDimf < 2 || nDimf > nLarge)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'NormEuclidean_Of_A_Vector': nDimf = %d", nDimf);
		fprintf(fout, "\n\n  An error in 'NormEuclidean_Of_A_Vector':  nDimf = %d", nDimf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nDimf < 2 || nDimf > nLarge)

////////////////////////////////////////////
	fNormEuclid_Of_A_Vectorf = 0.0;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fNormEuclid_Of_A_Vectorf += fFeas_Arrf[iFeaf] * fFeas_Arrf[iFeaf];
	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	if (fNormEuclid_Of_A_Vectorf > fLarge || fNormEuclid_Of_A_Vectorf < 0.0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'NormEuclidean_Of_A_Vector': fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
		fprintf(fout, "\n\n  An error in 'NormEuclidean_Of_A_Vector':  fNormEuclid_Of_A_Vectorf = %E", fNormEuclid_Of_A_Vectorf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fNormEuclid_Of_A_Vectorf > fLarge || ...)

	return SUCCESSFUL_RETURN;
} //int NormEuclidean_Of_A_Vector (...
////////////////////////////////////////////////////////////////////////////////

int Normalizing_A_Vector_ByStDev(
	const int nDimf,
	const float fFeas_InitArrf[],
	
	float fFeasNormalized_Arrf[])
{
	int StDev_Of_A_Vector(
		const int nDimf,
		const float fFeas_InitArrf[],

		float &StDev_Of_A_Vectorf);

	int
		nResf,
		iFeaf;

	float 
		StDev_Of_A_Vectorf;

	nResf = StDev_Of_A_Vector(
		nDimf, //const int nDimf,
		fFeas_InitArrf, //const float fFeas_InitArrf[],

		StDev_Of_A_Vectorf); // float &StDev_Of_A_Vectorf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'Normalizing_A_Vector_ByStDev': iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d, StDev_Of_A_Vectorf = %E", iVec_Train_Glob, nY_Train_Actual_Glob, StDev_Of_A_Vectorf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Normalizing_A_Vector_ByStDev' by 'StDev_Of_A_Vector' ");
		fprintf(fout, "\n\n  An error in 'Normalizing_A_Vector_ByStDev' by 'StDev_Of_A_Vector' ");
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fFeasNormalized_Arrf[iFeaf] = fFeas_InitArrf[iFeaf]/ StDev_Of_A_Vectorf;

		if (fFeasNormalized_Arrf[iFeaf] < -fLarge || fFeasNormalized_Arrf[iFeaf] > fLarge)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Normalizing_A_Vector_ByStDev': fFeasNormalized_Arrf[iFeaf] =  %E < -fLarge || ...m iFeaf = %d", fFeasNormalized_Arrf[iFeaf], iFeaf);

			fprintf(fout, "\n\n  An error in 'Normalizing_A_Vector_ByStDev': fFeasNormalized_Arrf[iFeaf] =  %E < -fLarge || ...m iFeaf = %d", fFeasNormalized_Arrf[iFeaf], iFeaf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (fFeasNormalized_Arrf[iFeaf] < -fLarge || fFeasNormalized_Arrf[iFeaf] > fLarge)

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
} //int Normalizing_A_Vector_ByStDev (...

/////////////////////////////////////////////////////////////////////////////////
int A_Vec_From_ArrOf_AllVecs(
	const int nDimf,
		const int nNumOfVecsTotf,
	const int nVecf,

		const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

		float fFeas_OneVec_Arrf[]) //[nDim_D_WithConst]
{
	int
		nIndexf,

		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,
		nTempf,
		iFeaf;

	nTempf = nVecf * nDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'A_Vec_From_ArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d", nDimf, nNumOfVecsTotf, nVecf);
	fprintf(fout, "\n\n  'A_Vec_From_ArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d, nTempf = %d", nDimf, nNumOfVecsTotf, nVecf, nTempf);
	fprintf(fout, "\n\n iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		if (nIndexf > nIndexMaxf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'A_Vec_From_ArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			fprintf(fout, "\n\n  An error in 'A_Vec_From_ArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

#ifndef COMMENT_OUT_ALL_PRINTS
		if (nVecf < 4)
		{
			fprintf(fout, "\n 'A_Vec_From_ArrOf_AllVecs': nVecf = %d, iFeaf = %d, nIndexf = %d, fFeas_All_Arrf[nIndexf] = %E", nVecf, iFeaf,nIndexf, fFeas_All_Arrf[nIndexf]);
		}//if (nVecf < 4)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fFeas_OneVec_Arrf[iFeaf] = fFeas_All_Arrf[nIndexf];
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
}// int A_Vec_From_ArrOf_AllVecs{...
///////////////////////////////////////////////////////

int Writing_OneDimVec_To_2DimVec(
	const int nDimf,
	const int nNumOfVecsTotf,

	const int nVecf,

	const float fFeas_OneVec_Arrf[], //[nDim_D_WithConst]

	float fFeas_All_Arrf[]) //[nDimf*nVecTotf]
{

	int
		nIndexf,

		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,
		nTempf,
		iFeaf;

	nTempf = nVecf * nDimf;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		if (nIndexf > nIndexMaxf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Writing_OneDimVec_To_2DimVec': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			fprintf(fout, "\n\n  An error in 'Writing_OneDimVec_To_2DimVec': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

		fFeas_All_Arrf[nIndexf] = fFeas_OneVec_Arrf[iFeaf];
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
}// int Writing_OneDimVec_To_2DimVec{...

///////////////////////////////////////////////////
int PasAggMaxOut_Train(
	const int nNumOfItersOfTrainingTotf,

	//after shuffling
	const float fFea_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], to be normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	const int nVecTrainf,
/////////////////////////

	const int nDim_D_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

///////////////////////
	const float fAlphaf, // < 1.0
	const float fEpsilonf,
	const float fCrf,
	const float fCf,
///////////////////////////////////////////////////
		float fW_Arrf[],
		float fU_Arrf[], //[nDim_U],

	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults)
{
	void Initializing_U_Arr(
		const int nDim_Uf,
		const float fU_Init_Min_Globf,
		const float fU_Init_Max_Globf,

		float fU_Arrf[]);

	void Initializing_W_Arr(
		const int nDim_Hf,
		const float fW_Init_Min_Globf,
		const float fW_Init_Max_Globf,

		float fW_Arrf[]);

	int Normalizing_A_Vector_ByStDev(
		const int nDimf,
		const float fFeas_InitArrf[],

		float fFeasNormalized_Arrf[]);

	int Normalizing_A_Vector_For_ARange(
		const int nDimf,
		const float fFin_Minf,
		const float fFin_Maxf,

		const float fFeas_InitArrf[],

		float fFeas_NormArrf[]);

	int All_Feas_OfNonlinearSpace(
					const int nDim_D_WithConstf, // = dimension of the original space
					const int nDim_Hf, //dimension of the nonlinear/transformed space

					const int nKf, //nNumOfHyperplanes

					const float fX_Arrf[], //[nDim_D_WithConstf]

					const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

		//////////////////////////////////////////
					int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]

					float fZ_Arrf[]); //[nDim_H]

//////////////////////////////////////////////////

	int OrthonormalizationOf_fU_Arrf(
		const int nDim_D_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		float fU_Arrf[]); //nDim_Uf

	//////////////////////////////////////////////
		int Updating_W_Arr(
			const int nDim_Hf,

			const float fCf,
			const float fAlphaf, // < 1.0

			const float fLossf,

			const int nYtf, // 1 or -1

			const float fZ_Arrf[], //[nDim_Hf]

			const float fW_Init_Arrf[], //[nDim_Hf]

			float fW_Fin_Arrf[]); ////[nDim_Hf]

	int Updating_Z(
		const int nDim_Hf,

		const float fLossf,

		const int nYtf, // 1 or -1

		const float fW_Fin_Arrf[], //[nDim_Hf]

		const float fZ_Init_Arrf[], //[nDim_Hf]

		float fZ_Fin_Arrf[]);	//[nDim_Hf]

	int StDev_Of_A_Vector(
		const int nDimf,
		const float fFeas_InitArrf[],

		float &StDev_Of_A_Vectorf);

	int A_Vec_From_ArrOf_AllVecs(
		const int nDimf,
			const int nNumOfVecsTotf,

		const int nVecf,

		const float fFeas_All_Arrf[], //[nProdTrainTot]

		float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	void Loss(
		const int nDim_Hf,
		const int nYtf, // 1 or -1 (not 0)

		const float fZ_Arrf[], //[nDim_Hf]

		const float fW_Arrf[], //[nDim_Hf]

		int &nY_Estimatedf,

		float &fLossf); 

	void Copying_Float_Arr1_To_Arr2(
		const int nDimf,
		const float fArr1f[], // [nDimf]
		float fArr2f[]); // [nDimf]

	int Print_fU_Arr(
		const int nDim_D_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		const float fU_Arrf[]); //nDim_Uf

	void Print_A_FloatOneDim_Arr(
		const int nDimf, //

		const float fArrf[]); //nDimf

	int PasAggMaxOut_Test(
		const float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot], 
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_WithConstf

		const int nVecTestf,

		/////////////////////////
		const int nDim_D_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fW_Train_Arrf[], //[nDim_Hf]
		const float fU_Train_Arrf[], //[nDim_U],
		///////////////////////////////////////////////////

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);
/////////////////////////////////////////////////
	int
		nResf,
			nHyperplaneWithMaxScaProdArrf[nDim_H], //[]

		//nHyperplaneSelected_For_Z_Arrf[nDim_H],

		nY_For_LossCurf,
		nY_Estimatedf,
			nY_Estimated_Arrf[nNumVecTrainTot],

		nNumOfPosit_Y_Totf = 0,
		nNumOfNegat_Y_Totf = 0,

		nNumOfCorrect_Y_Totf = 0,

		nNumOfPositCorrect_Y_Totf = 0,
		nNumOfNegatCorrect_Y_Totf = 0,

		nY_Actualf,

		iFeaf,

		iFea_Nonlinearf,
		iItersOfTrainingf,
		iVecf;

	float
		fScalar_Prodf,
		fLossf,

		fPercentageOfCorrectTotf = 0.0,
		fPercentageOfCorrect_Positf = 0.0,
		fPercentageOfCorrect_Negatf = 0.0,

			//fU_Arrf[nDim_U],
			//fW_Arrf[nDim_H],

			fW_Fin_Arrf[nDim_H],
			
		fZ_Arrf[nDim_H],

		fZ_NormalizedArrf[nDim_H],
		fZ_Fin_Arrf[nDim_H],

		fFea_OneVec_Train_Arrf[nDim_D_WithConst],
		fFea_OneVec_TrainNormalized_Arrf[nDim_D_WithConst];
	
///////////////////////////
	srand(nSrandInit_Glob);

	//srand(1);
	//srand(2);
//srand(3);
	//srand(4);
	//srand(5);
	//srand(6);
	//srand(7);
	//srand(8);
	//srand(9);
	//srand(10);

	Initializing_W_Arr(
		nDim_Hf, //const int nDim_Hf,
		fW_Init_Min_Glob, //const float fW_Init_Min_Globf,
		fW_Init_Max_Glob, //const float fW_Init_Max_Globf,

		fW_Arrf); // float fW_Arrf[]);

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Train': init fW_Arrf[]");
	fprintf(fout, "\n\n  'PasAggMaxOut_Train': init fW_Arrf[]");

	Print_A_FloatOneDim_Arr(
		nDim_Hf, //const int nDimf, //

		fW_Arrf); // const float fArrf[]) //nDimf
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

///////////////////////
	Initializing_U_Arr(
		nDim_Uf, //const int nDim_Uf,
		fU_Init_Min_Glob, //const float fU_Init_Min_Globf, // -1.0
		fU_Init_Max_Glob, //const float fU_Init_Max_Globf, //+1.0

		fU_Arrf); // float fU_Arrf[]);

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Train': fU_Arrf[] after initialization");
	fprintf(fout, "\n\n 'PasAggMaxOut_Train': fU_Arrf[] after initialization");
	
	nResf = Print_fU_Arr(
		nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		fU_Arrf); // float fU_Arrf[]); //nDim_Uf
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
////////////////////////////////////////////////////

#ifdef INITIAL_ORTHONORMALIOZATION
	nResf = OrthonormalizationOf_fU_Arrf(
		nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		fU_Arrf); // float fU_Arrf[]); //nDim_Uf

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'PasAggMaxOut_Train' by 'OrthonormalizationOf_fU_Arrf'");
		fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'OrthonormalizationOf_fU_Arrf'");
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#endif //#ifdef INITIAL_ORTHONORMALIOZATION

	//////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'PasAggMaxOut_Test' for training data and init fW_Arrf[] and fU_Arrf[], nVecTrainf = %d", nVecTrainf);
	fprintf(fout, "\n\n PasAggMaxOut_Test' for training data and init fW_Arrf[] and fU_Arrf[], nVecTrainf = %d", nVecTrainf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	PAS_AGG_MAX_OUT_RESUTS sPasAggMaxOut_TrainResultsRightAfterInitf;

	nResf = PasAggMaxOut_Test(
		fFea_WithConstTrain_Arrf, //const float fFeaTest_Arrf[], //[nProdTestTot], to be normalized
		nY_Train_Actual_Arrf, //[nNumVecTrainTot], const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_WithConstf

		nVecTrainf, //const int nVecTestf,

		/////////////////////////
		nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		fW_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]
		fU_Arrf, //const float fU_Train_Arrf[], //[nDim_U],
		///////////////////////////////////////////////////

		&sPasAggMaxOut_TrainResultsRightAfterInitf); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'PasAggMaxOut_Train' by 'PasAggMaxOut_Test'");
		fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'PasAggMaxOut_Test'");
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  After initial 'PasAggMaxOut_Test' in 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf);

	fprintf(fout, "\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);

	
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
///////////////////////////////////////////////////////////////

	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n After initial 'PasAggMaxOut_Test' in 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrectTotf);
	printf("\n ...AfterInitf.fPercentageOfCorrect_Positf = %E, ...AfterInitf.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResultsRightAfterInitf.fPercentageOfCorrect_Negatf);

	printf("\n\n ...AfterInitf->nNumOfPosit_Y_Totf = %d, ...AfterInitf->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...AfterInitf->nNumOfNegat_Y_Totf = %d, ...AfterInitf->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegatCorrect_Y_Totf);

//	printf("\n\n Please press any key to continue:"); fflush(fout);  getchar();
///////////////////////////////////////////////////////////////

	if (sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf > 0)
	{
		fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = (float)(sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfPosit_Y_Totf) / (float)(sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'PasAggMaxOut_Train':  fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = %E", fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob);
		fprintf(fout, "\n\n   fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob = %E", fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // if (sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf > 0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'PasAggMaxOut_Train':  sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf);
		fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train': sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf = %d", sPasAggMaxOut_TrainResultsRightAfterInitf.nNumOfNegat_Y_Totf);
		getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//else

//	srand(4);
	for (iItersOfTrainingf = 0; iItersOfTrainingf < nNumOfItersOfTrainingTotf; iItersOfTrainingf++) //  nNumOfItersOfTrainingTot == 1 initially
	{
		nNumOfPosit_Y_Totf = 0;
			nNumOfNegat_Y_Totf = 0;

			nNumOfCorrect_Y_Totf = 0;

			nNumOfPositCorrect_Y_Totf = 0;
			nNumOfNegatCorrect_Y_Totf = 0;

		fPercentageOfCorrectTotf = 0.0;
		fPercentageOfCorrect_Positf = 0.0;
		fPercentageOfCorrect_Negatf = 0.0;

		for (iVecf = 0; iVecf < nVecTrainf; iVecf++)
		{
			iVec_Train_Glob = iVecf;

			////////////////////////////////////////////////////////////////
			nY_Actualf = nY_Train_Actual_Arrf[iVecf];
			nY_Train_Actual_Glob = nY_Actualf;

			if (nY_Actualf == 1)
			{
				nNumOfPosit_Y_Totf += 1;
			} // if (nY_Actualf == 1)
			else if (nY_Actualf == 0) //-1)
			{
				nNumOfNegat_Y_Totf += 1;
			} //else if (nY_Actualf == 0) //-1)
			else
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Train': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}//else

			if (nY_Actualf == 0)
				nY_For_LossCurf = -1;
			else
				nY_For_LossCurf = 1;

///////////////////////////////
			nResf = A_Vec_From_ArrOf_AllVecs(
						nDim_D_WithConstf, //const int nDimf,

						nNumVecTrainTot, //const int nNumOfVecsTotf,

						iVecf, //const int nVecf,

						fFea_WithConstTrain_Arrf, //const float fFeas_All_Arrf[], //[nProdTrainTot]

						fFea_OneVec_Train_Arrf); // float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Train' by 'A_Vec_From_ArrOf_AllVecs'at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'A_Vec_From_ArrOf_AllVecs' at iVecf = %d", iVecf);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'fFea_OneVec_Train_Arrf[]' after 'A_Vec_From_ArrOf_AllVecs', iVecf = %d, iItersOfTrainingf = %d, nDim_D_WithConstf = %d\n", iVecf, iItersOfTrainingf, nDim_D_WithConstf);
			for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
			{
				fprintf(fout, "%d:%E, ", iFeaf, fFea_OneVec_Train_Arrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//int Normalizing_A_Vector_For_ARange() ??
			nResf = Normalizing_A_Vector_ByStDev(
						nDim_D_WithConstf, //const int nDimf,
						fFea_OneVec_Train_Arrf, //const float fFeas_InitArrf[],

						fFea_OneVec_TrainNormalized_Arrf); // float fFeasNormalized_Arrf[]);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Train' by 'Normalizing_A_Vector_ByStDev' 1 at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Normalizing_A_Vector_ByStDev' 1 at iVecf = %d", iVecf);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'fFea_OneVec_TrainNormalized_Arrf[]':\n");
			for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
			{
				fprintf(fout, "%d:%E, ", iFeaf, fFea_OneVec_TrainNormalized_Arrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////

			nResf = All_Feas_OfNonlinearSpace(
						nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
						nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

						nKf, //const int nKf, //nNumOfHyperplanes

						fFea_OneVec_TrainNormalized_Arrf, //const float fX_Arrf[], //[nDim_D_WithConstf]

						fU_Arrf, //const float fU_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

						nHyperplaneWithMaxScaProdArrf, //int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]ec

						fZ_Arrf); // float fZ_Arrf[]); //[nDim_H]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Train' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'fZ_Arrf[]' and 'nHyperplaneWithMaxScaProdArrf[]': nDim_Hf = %d, iVecf = %d, iItersOfTrainingf = %d\n", nDim_Hf, iVecf, iItersOfTrainingf);

			for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
			{
				fprintf(fout, "\n 'nHyperplaneWithMaxScaProdArrf[%d] = %d, fZ_Arrf[%d] = %E",iFeaf,nHyperplaneWithMaxScaProdArrf[iFeaf], iFeaf, fZ_Arrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////////
			nResf = Normalizing_A_Vector_ByStDev(
						nDim_Hf, //const int nDimf,
						fZ_Arrf, //const float fFeas_InitArrf[],

						fZ_NormalizedArrf); // float fFeasNormalized_Arrf[]);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Train' by 'Normalizing_A_Vector_ByStDev' 2 at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Normalizing_A_Vector_ByStDev' 2 at iVecf = %d", iVecf);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n 'fZ_NormalizedArrf[]':\n");
			for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
			{
				fprintf(fout, "%d:%E, ", iFeaf, fZ_NormalizedArrf[iFeaf]);
			} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/////////////////////////////
			Loss(
				nDim_Hf, //const int nDim_Hf,
				nY_For_LossCurf, //const int nYtf, // 1 or -1

				fZ_NormalizedArrf, //const float fZ_Arrf[], //[nDim_Hf]

				fW_Arrf, //const float fW_Arrf[], //[nDim_Hf]

				nY_Estimatedf, //int &nY_Estimatedf, //0 or 1

				fLossf); // float &fLossf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n After 'Loss() for 'fZ_NormalizedArrf[]' and 'fW_Arrf[]': nY_For_LossCurf = %d, nY_Estimatedf = %d, fLossf = %E, iVecf = %d", nY_For_LossCurf, nY_Estimatedf, fLossf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifdef INCLUDE_BIAS_POS_TO_NEG
			if (fLossf > 0.0)
			{
				if (nY_Actualf == 1)
				{
					if (fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob < 1.0) //fewer pos vecs than neg -- increasing the loss each pos vec
					{
						fLossf = fLossf * (2.0 - fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob);

#ifndef COMMENT_OUT_ALL_PRINTS
						//printf("\n\n Adjusting a loss for a pos vec: nY_Actualf = %d, fLossf = %E, iVecf = %d", nY_Actualf, fLossf, iVecf);
						fprintf(fout, "\n\n  Adjusting a loss for a pos vec: nY_Actualf = %d, fLossf = %E, iVecf = %d", nY_Actualf, fLossf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					} // if (fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob < 1.0)

				} // if (nY_Actualf == 1)
				else if (nY_Actualf == 0)
				{
					if (fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob > 1.0)
					{
						fLossf = fLossf * fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob;

#ifndef COMMENT_OUT_ALL_PRINTS
						//printf("\n\n Adjusting a loss for a neg vec: nY_Actualf = %d, fLossf = %E, iVecf = %d", nY_Actualf, fLossf, iVecf);
						fprintf(fout, "\n\n  Adjusting a loss for a neg vec: nY_Actualf = %d, fLossf = %E, iVecf = %d", nY_Actualf, fLossf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					}//else if (fRatioOfNumOfPosVecsTo_NumOfNegVecs_Glob > 1.0)
				} //else if (nY_Actualf == 0)
			} // if (fLossf > 0.0)
#endif // #ifdef INCLUDE_BIAS_POS_TO_NEG

			nY_Estimated_Arrf[iVecf] = nY_Estimatedf;

			if (nY_Estimatedf == nY_Actualf)
			{
				nNumOfCorrect_Y_Totf += 1;

				if (nY_Actualf == 1)
				{
					nNumOfPositCorrect_Y_Totf += 1;
				} // if (nY_Actualf == 1)
				else if (nY_Actualf == 0) //-1)
				{
					nNumOfNegatCorrect_Y_Totf += 1;
				} //else if (nY_Actualf == 0) //-1)

			} //if (nY_Estimatedf == nY_Actualf)

			fPercentageOfCorrectTotf = (float)(100.0)*((float)(nNumOfCorrect_Y_Totf) / (float)(iVecf + 1));

			if (nNumOfPosit_Y_Totf > 0)
			{
				fPercentageOfCorrect_Positf = (float)(100.0)*((float)(nNumOfPositCorrect_Y_Totf) / (float)(nNumOfPosit_Y_Totf));
			} //if (nNumOfPosit_Y_Totf > 0)

			if (nNumOfNegat_Y_Totf > 0)
			{
				fPercentageOfCorrect_Negatf = (float)(100.0)*((float)(nNumOfNegatCorrect_Y_Totf) / (float)(nNumOfNegat_Y_Totf));
			} //if (nNumOfNegat_Y_Totf > 0)

#ifndef COMMENT_OUT_ALL_PRINTS

			if ((iVecf / 50) * 50 == iVecf)
			{
				printf("\n\n 'PasAggMaxOut_Train': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

				printf("\n iItersOfTrainingf = %d, nNumOfItersOfTrainingTotf = %d", iItersOfTrainingf, nNumOfItersOfTrainingTotf);

				printf("\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

				printf("\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
					nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

				printf("\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
					nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

			} //f ( (iVecf / 50) * 50 == iVecf)

			fprintf(fout, "\n\n  'PasAggMaxOut_Train': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

			fprintf(fout, "\n iItersOfTrainingf = %d, nNumOfItersOfTrainingTotf = %d",iItersOfTrainingf,nNumOfItersOfTrainingTotf);

			fprintf(fout, "\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

			fprintf(fout, "\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
				nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

			fprintf(fout, "\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
				nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (fLossf > 0.0) //nY_Estimatedf != nY_Actualf)
			{
				nResf = Updating_W_Arr(
							nDim_Hf, //const int nDim_Hf,

							fCf, //const float fCf, //0.125
							fAlphaf, //const float fAlphaf, // 0.9 < 1.0

							fLossf, //const float fLossf,

								nY_For_LossCurf, //not nY_Actualf, //const int nYtf, // 1 or -1

							fZ_NormalizedArrf, //const float fZ_Arrf[], //[nDim_Hf]

							fW_Arrf, //const float fW_Init_Arrf[], //[nDim_Hf]

							fW_Fin_Arrf); // float fW_Fin_Arrf[]); ////[nDim_Hf]

				if (nResf == UNSUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'PasAggMaxOut_Train' by 'Updating_W_Arr' at iVecf = %d", iVecf);
					fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Updating_W_Arr' at iVecf = %d", iVecf);
					getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "\n\n 'fW_Fin_Arrf[]' after 'Updating_W_Arr': \n");
				for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
				{
					fprintf(fout, "%d:%E, ", iFeaf, fW_Fin_Arrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////////////
				Copying_Float_Arr1_To_Arr2(
					nDim_Hf, //const int nDimf,
					fW_Fin_Arrf, //const float fArr1f[], // [nDimf]
					fW_Arrf); // float fArr2f[]) // [nDimf]
	///////////////////////////////////////////////////

				nResf = Updating_Z(
							nDim_Hf, //const int nDim_Hf,

							fLossf, //const float fLossf,

								nY_For_LossCurf, //not nY_Actualf, //const int nYtf, // 1 or -1

							fW_Fin_Arrf, //const float fW_Fin_Arrf[], //[nDim_Hf]

							fZ_NormalizedArrf, //const float fZ_Init_Arrf[], //[nDim_Hf]

							fZ_Fin_Arrf); // float fZ_Fin_Arrf[]);	//[nDim_Hf]

				if (nResf == UNSUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'PasAggMaxOut_Train' by 'Updating_Z' at iVecf = %d", iVecf);
					fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Train' by 'Updating_Z' at iVecf = %d", iVecf);
					getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				} //if (nResf == UNSUCCESSFUL_RETURN)

//				Print_A_FloatOneDim_Arr(
	//				nDim_Hf, //const int nDimf, //

//					const float fArrf[]) //nDimf

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout, "\n\n 'fZ_Fin_Arrf[]' after 'Updating_Z': \n");
				for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)
				{
					fprintf(fout, "\n fZ_NormalizedArrf[%d] = %E,  fZ_Fin_Arrf[%d] = %E", iFeaf, fZ_NormalizedArrf[iFeaf], iFeaf, fZ_Fin_Arrf[iFeaf]);
				} //for (iFeaf = 0; iFeaf < nDim_Hf; iFeaf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//not needed -- fZ_Arrf[] is calculated anew with each iVecf?
				Copying_Float_Arr1_To_Arr2(
					nDim_Hf, //const int nDimf,
					fZ_Fin_Arrf, //const float fArr1f[], // [nDimf]
					fZ_Arrf); // float fArr2f[]) // [nDimf]
	/////////////////////////////////////////////////////////////////////////////

				nResf = Updating_U_ForSelectedHyperplanes_Arr(

					nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space

					nDim_Hf, //const int nDim_Hf, // dimension of nonlinear space

					nKf, //const int nKf, // nNumOfHyperplanes per one fea in nonlinear space

					//const int nFea_Hf, //the number of a specific nonlinear fea; < nDim_Hf
					//const int nHyperplanef, //the number of a specific hyperplane of the specific nonlinear fea; < nKf
					//nHyperplaneWithMaxScaProdArrf, //const int nHyperplaneWithMaxScaProdArrf[], // [nDim_Hf]

					fFea_OneVec_TrainNormalized_Arrf, //const float fX_Arrf[], //[nDim_D_WithConstf]
					fZ_Fin_Arrf, //const float fZ_Fin_Arrf[], //[nDim_Hf]

					nHyperplaneWithMaxScaProdArrf, //const int nHyperplaneSelected_For_Z_Arrf[], //[nDim_Hf]

					fEpsilonf, //const float fEpsilonf,
					fCrf, //const float fCrf,

					///////////////////////////////////////////////////////
					fU_Arrf); // float fU_Arrf[]);// [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

			} //if (fLossf > 0.0)

#ifndef COMMENT_OUT_ALL_PRINTS
			fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iVecf = 0; iVecf < nVecTrainf; iVecf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'PasAggMaxOut_Train': after iItersOfTrainingf = %d, fin fW_Arrf[]", iItersOfTrainingf);
		fprintf(fout, "\n\n  'PasAggMaxOut_Train': after iItersOfTrainingf = %d, fin fW_Arrf[]", iItersOfTrainingf);

		Print_A_FloatOneDim_Arr(
			nDim_Hf, //const int nDimf, //

			fW_Arrf); // const float fArrf[]) //nDimf

		printf("\n\n 'PasAggMaxOut_Train': after iItersOfTrainingf = %d, nNumOfCorrect_Y_Totf = %d,nVecTrainf = %d, fPercentageOfCorrectTotf = %E",
			iItersOfTrainingf,nNumOfCorrect_Y_Totf, nVecTrainf, fPercentageOfCorrectTotf);

		printf("\n\n Train: nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
			nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

		printf("\n  Train: nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
			nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

		fprintf(fout, "\n\n 'PasAggMaxOut_Train': after iItersOfTrainingf = %d, nNumOfCorrect_Y_Totf = %d,nVecTrainf = %d, fPercentageOfCorrectTotf = %E",
			iItersOfTrainingf, nNumOfCorrect_Y_Totf, nVecTrainf, fPercentageOfCorrectTotf);

		fprintf(fout, "\n\n  Train: nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
			nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

		fprintf(fout, "\n  Train: nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
			nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

		fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // for (iItersOfTrainingf= 0; iItersOfTrainingf < nNumOfItersOfTrainingTotf; iItersOfTrainingf++)
	//////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Train': the final fW_Arrf[]");
	fprintf(fout, "\n\n  'PasAggMaxOut_Train': the final fW_Arrf[]");

	Print_A_FloatOneDim_Arr(
		nDim_Hf, //const int nDimf, //

		fW_Arrf); // const float fArrf[]) //nDimf
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	/////////////////////////////////////////////
	sPasAggMaxOut_TrainResults->nNumOfVecs_Totf = nVecTrainf;
	sPasAggMaxOut_TrainResults->nNumOfCorrect_Y_Totf = nNumOfCorrect_Y_Totf;

	sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf = nNumOfPosit_Y_Totf;
	sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf = nNumOfPositCorrect_Y_Totf;

	sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf = nNumOfNegat_Y_Totf;
	sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf = nNumOfNegatCorrect_Y_Totf;

	sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf = fPercentageOfCorrectTotf;
	sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf = fPercentageOfCorrect_Positf;
	sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf = fPercentageOfCorrect_Negatf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'PasAggMaxOut_Train': final nNumOfCorrect_Y_Totf = %d, nVecTrainf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTrainf, fPercentageOfCorrectTotf);
	
	fprintf(fout, "\n\n  Train: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	fprintf(fout, "\n  Train: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

	fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	printf("\n\n 'PasAggMaxOut_Train': final train nNumOfCorrect_Y_Totf = %d,nVecTrainf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTrainf, fPercentageOfCorrectTotf);

	printf("\n\n Train: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	printf("\n  Train: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Train': fin fW_Arrf[]");
	fprintf(fout, "\n\n  'PasAggMaxOut_Train': fin fW_Arrf[]");

	Print_A_FloatOneDim_Arr(
		nDim_Hf, //const int nDimf, //

		fW_Arrf); // const float fArrf[]) //nDimf
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'PasAggMaxOut_Train': fU_Arrf[]");
	fprintf(fout, "\n\nThe end of 'PasAggMaxOut_Train': fU_Arrf[]");

	nResf = Print_fU_Arr(
		nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		fU_Arrf); // float fU_Arrf[]); //nDim_Uf

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} // int PasAggMaxOut_Train(...

//////////////////////////////////////////////////////////////////////////////////////////

int PasAggMaxOut_Test(
			//const float fFea_WithConstTest_Arrf[], //[nProdTestTot], to be normalized
			const float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot],

			const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

			//const int nDimf, == nDim_D_WithConstf

			const int nVecTestf,

			/////////////////////////
			const int nDim_D_WithConstf, // = dimension of the original space
			const int nDim_Hf, //dimension of the nonlinear/transformed space

			const int nKf, //nNumOfHyperplanes
			const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

			///////////////////////
			const float fW_Train_Arrf[],
			const float fU_Train_Arrf[], //[nDim_U],
			///////////////////////////////////////////////////

			PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults)
{

	int Normalizing_A_Vector_For_ARange(
		const int nDimf,
		const float fFin_Minf,
		const float fFin_Maxf,

		const float fFeas_InitArrf[],

		float fFeas_NormArrf[]);

	int All_Feas_OfNonlinearSpace(
		const int nDim_D_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes

		const float fX_Arrf[], //[nDim_D_WithConstf]

		const float fU_Train_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

//////////////////////////////////////////
		int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]

		float fZ_Arrf[]); //[nDim_H]

//////////////////////////////////////////////
	
	int StDev_Of_A_Vector(
		const int nDimf,
		const float fFeas_InitArrf[],

		float &StDev_Of_A_Vectorf);

	int A_Vec_From_ArrOf_AllVecs(
		const int nDimf,
		const int nNumOfVecsTotf,

		const int nVecf,

		const float fFeas_All_Arrf[], //[nProdTestTot]

		float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	int Normalizing_A_Vector_ByStDev(
		const int nDimf,
		const float fFeas_InitArrf[],

		float fFeasNormalized_Arrf[]);

/////////////////////////////////////////////////
	int
		nResf,
		nHyperplaneWithMaxScaProdArrf[nDim_H], //[]

		iFea_Nonlinearf,
		nY_Estimatedf,

		//nY_Estimated_Arrf[nNumVecTestTot],

		nNumOfPosit_Y_Totf = 0,
		nNumOfNegat_Y_Totf = 0,

		nNumOfCorrect_Y_Totf = 0,

		nNumOfPositCorrect_Y_Totf = 0,
		nNumOfNegatCorrect_Y_Totf = 0,

		nY_Actualf,
		nY_For_LossCurf,

		iVecf;

	float
		fScalar_Prodf,
		fLossf,

		fLossMinf = fLarge,

		fLossMaxf = -fLarge,
		fPercentageOfCorrectTotf = 0.0,
		fPercentageOfCorrect_Positf = 0.0,
		fPercentageOfCorrect_Negatf = 0.0,

		//fW_Fin_Arrf[nDim_H],

		fZ_Arrf[nDim_H],
		fZ_NormalizedArrf[nDim_H],
		fZ_Fin_Arrf[nDim_H],

		fFea_OneVec_Arrf[nDim_D_WithConst],
		fFea_OneVecNormalized_Arrf[nDim_D_WithConst];
/////////////////////////////////////////////////

	int* nY_Estimated_Arrf = new int[nVecTestf];
	if (nY_Estimated_Arrf == NULL)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'PasAggMaxOut_Test': nY_Estimated_Arrf == NULL");
		fprintf(fout, "\n\n An error in 'PasAggMaxOut_Test': nY_Estimated_Arrf == NULL");
		//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nY_Estimated_Arrf == NULL)
///////////////////////
		for (iVecf = 0; iVecf < nVecTestf; iVecf++)
		{
			nResf = A_Vec_From_ArrOf_AllVecs(
						nDim_D_WithConstf, //const int nDimf,

						nVecTestf, //const int nNumOfVecsTotf,

						iVecf, //const int nVecf,

						fFea_WithConstTest_Arrf, //const float fFeas_All_Arrf[], //[nProd_WithConstTestTot]

						fFea_OneVec_Arrf); // float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Test' by 'A_Vec_From_ArrOf_AllVecs'at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test' by 'A_Vec_From_ArrOf_AllVecs' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nY_Estimated_Arrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

//int Normalizing_A_Vector_For_ARange() ??
			nResf = Normalizing_A_Vector_ByStDev(
						nDim_D_WithConstf, //const int nDimf,
						fFea_OneVec_Arrf, //const float fFeas_InitArrf[],

						fFea_OneVecNormalized_Arrf); // float fFeasNormalized_Arrf[]);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Test' by 'Normalizing_A_Vector_ByStDev' 1 at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test' by 'Normalizing_A_Vector_ByStDev' 1 at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nY_Estimated_Arrf;
				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)
	////////////////////////////////

			nResf = All_Feas_OfNonlinearSpace(
						nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
						nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

						nKf, //const int nKf, //nNumOfHyperplanes

						fFea_OneVecNormalized_Arrf, //const float fX_Arrf[], //[nDim_D_WithConstf]

						fU_Train_Arrf, //const float fU_Train_Arrf[], // [nDim_U] = (nDim_D_WithConst*nDim_H*nK). nK = nNumOfHyperplanes 4 // the number of hyperplanes

						nHyperplaneWithMaxScaProdArrf, //int nHyperplaneWithMaxScaProdArrf[], //[nDim_Hf]

						fZ_Arrf); // float fZ_Arrf[]); //[nDim_H]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Test' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test' by 'All_Feas_OfNonlinearSpace' at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nY_Estimated_Arrf;
				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

			
#ifndef COMMENT_OUT_ALL_PRINTS
			for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)
			{
				fprintf(fout, "\n 'PasAggMaxOut_Test' (after 'All_Feas_OfNonlinearSpace'): iVecf = %d, nHyperplaneWithMaxScaProdArrf[%d] = %d, fZ_Arrf[%d] = %E", 
					iVecf,iFea_Nonlinearf,nHyperplaneWithMaxScaProdArrf[iFea_Nonlinearf], iFea_Nonlinearf,fZ_Arrf[iFea_Nonlinearf]);
			} //for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////////
			nResf = Normalizing_A_Vector_ByStDev(
				nDim_Hf, //const int nDimf,
				fZ_Arrf, //const float fFeas_InitArrf[],

				fZ_NormalizedArrf); // float fFeasNormalized_Arrf[]);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Test' by 'Normalizing_A_Vector_ByStDev' 2 at iVecf = %d", iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test' by 'Normalizing_A_Vector_ByStDev' 2 at iVecf = %d", iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nY_Estimated_Arrf;
				return UNSUCCESSFUL_RETURN;
			} //if (nResf == UNSUCCESSFUL_RETURN)

			nY_Actualf = nY_Test_Actual_Arrf[iVecf];

			if (nY_Actualf == 1)
			{
				nNumOfPosit_Y_Totf += 1;
			} // if (nY_Actualf == 1)
			else if (nY_Actualf == 0) //-1)
			{
				nNumOfNegat_Y_Totf += 1;
			} //else if (nY_Actualf == 0) //-1)
			else
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'PasAggMaxOut_Test': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
				fprintf(fout, "\n\n  An error in  'PasAggMaxOut_Test': nY_Actualf = %d at iVecf = %d", nY_Actualf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nY_Estimated_Arrf;
				return UNSUCCESSFUL_RETURN;
			}//else

#ifndef COMMENT_OUT_ALL_PRINTS
			for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)
			{
				fprintf(fout, "\n 'PasAggMaxOut_Test' (after normalizing): iVecf = %d, nY_Actualf = %d, fZ_Arrf[%d] = %E, fZ_NormalizedArrf[%d] = %E",
					iVecf, nY_Actualf,iFea_Nonlinearf, fZ_Arrf[iFea_Nonlinearf], iFea_Nonlinearf, fZ_NormalizedArrf[iFea_Nonlinearf]);
			} //for (iFea_Nonlinearf = 0; iFea_Nonlinearf < nDim_Hf; iFea_Nonlinearf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/////////////////////////////
			if (nY_Actualf == 0)
				nY_For_LossCurf = -1;
			else
				nY_For_LossCurf = 1;

			Loss(
				nDim_Hf, //const int nDim_Hf,
				nY_For_LossCurf, //const int nYtf, // 1 or -1 (not 0)

				fZ_NormalizedArrf, //const float fZ_Arrf[], //[nDim_Hf]

				fW_Train_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]

				nY_Estimatedf, //int &nY_Estimatedf, //0 or 1

				fLossf); // float &fLossf);

			if (fLossf < fLossMinf)
				fLossMinf = fLossf;

			if (fLossf > fLossMaxf)
				fLossMaxf = fLossf;

			nY_Estimated_Arrf[iVecf] = nY_Estimatedf;

			if (nY_Estimatedf == nY_Actualf)
			{
				nNumOfCorrect_Y_Totf += 1;

				if (nY_Actualf == 1)
				{
					nNumOfPositCorrect_Y_Totf += 1;
				} // if (nY_Actualf == 1)
				else if (nY_Actualf == 0) //-1)
				{
					nNumOfNegatCorrect_Y_Totf += 1;
				} //else if (nY_Actualf == 0) //-1)

			} //if (nY_Estimatedf == nY_Actualf)

			fPercentageOfCorrectTotf = (float)(100.0)*(float)(nNumOfCorrect_Y_Totf) / (float)(iVecf + 1);

			if (nNumOfPosit_Y_Totf > 0)
			{
				fPercentageOfCorrect_Positf = (float)(100.0)*(float)(nNumOfPositCorrect_Y_Totf) / (float)(nNumOfPosit_Y_Totf);
			} //if (nNumOfPosit_Y_Totf > 0)

			if (nNumOfNegat_Y_Totf > 0)
			{
				fPercentageOfCorrect_Negatf = (float)(100.0)*(float)(nNumOfNegatCorrect_Y_Totf) / (float)(nNumOfNegat_Y_Totf);
			} //if (nNumOfNegat_Y_Totf > 0)

#ifndef COMMENT_OUT_ALL_PRINTS

			if ((iVecf / 50) * 50 == iVecf)
			{
				printf("\n\n 'PasAggMaxOut_Test': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

				printf("\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

				printf("\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
					nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

				printf("\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
					nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

			} //f ( (iVecf / 50) * 50 == iVecf)

			fprintf(fout, "\n\n  'PasAggMaxOut_Test': iVecf = %d, nY_Estimatedf = %d, nY_Actualf = %d, fLossf = %E", iVecf, nY_Estimatedf, nY_Actualf, fLossf);

			fprintf(fout, "\n\n nNumOfCorrect_Y_Totf = %d, iVecf + 1 = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, iVecf + 1, fPercentageOfCorrectTotf);

			fprintf(fout, "\n\n nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
				nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

			fprintf(fout, "\n nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
				nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iVecf = 0; iVecf < nVecTestf; iVecf++)

	/////////////////////////////////////////////
	sPasAggMaxOut_TestResults->nNumOfVecs_Totf = nVecTestf;
	sPasAggMaxOut_TestResults->nNumOfCorrect_Y_Totf = nNumOfCorrect_Y_Totf;

	sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf = nNumOfPosit_Y_Totf;
	sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf = nNumOfPositCorrect_Y_Totf;

	sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf = nNumOfNegat_Y_Totf;
	sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf = nNumOfNegatCorrect_Y_Totf;

	sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf = fPercentageOfCorrectTotf;
	sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf = fPercentageOfCorrect_Positf;
	sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf = fPercentageOfCorrect_Negatf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'PasAggMaxOut_Test': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);
	
	printf("\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	printf("\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n 'PasAggMaxOut_Test': final nNumOfCorrect_Y_Totf = %d, nVecTestf = %d, fPercentageOfCorrectTotf = %E", nNumOfCorrect_Y_Totf, nVecTestf, fPercentageOfCorrectTotf);

	fprintf(fout, "\n\n Test: final nNumOfPositCorrect_Y_Totf = %d, nNumOfPosit_Y_Totf = %d, fPercentageOfCorrect_Positf = %E",
		nNumOfPositCorrect_Y_Totf, nNumOfPosit_Y_Totf, fPercentageOfCorrect_Positf);

	fprintf(fout, "\n Test: final nNumOfNegatCorrect_Y_Totf = %d, nNumOfNegat_Y_Totf = %d, fPercentageOfCorrect_Negatf = %E",
		nNumOfNegatCorrect_Y_Totf, nNumOfNegat_Y_Totf, fPercentageOfCorrect_Negatf);

	fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	printf("\n\n The end of 'PasAggMaxOut_Test': fLossMinf = %E, fLossMaxf = %E", fLossMinf, fLossMaxf);
	delete[] nY_Estimated_Arrf;

	return SUCCESSFUL_RETURN;
} // int PasAggMaxOut_Test(...

//////////////////////////////////////////////////////////////////////////////
void Swap_2Ints(
	int nX_1f, 
	int nX_2f) 
{
	int nTempf = nX_1f;

	nX_1f = nX_2f;
	nX_2f = nTempf;
}//void Swap_2Ints(...

//////////////////////////////////////////
//by Fisher-Yates (Wiki)
void Shuffling_AnIntArr(
	const int nDimf,
	int nArrf[]) //[nDimf]; initially [0,1,2,...,nDimf - 1]
{
	void Swap_2Ints(
		int nX_1f,
		int nX_2f);

	//srand(4);

	int
		nTempff,

		iFea_1f,
		nFea_2f;

	for (iFea_1f = nDimf - 1; iFea_1f > 0; iFea_1f--)
	{
		nFea_2f = rand() % (iFea_1f + 1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n iFea_1f = %d, nFea_2f = %d, init nArrf[iFea_1f] = %d, nArrf[nFea_2f] = %d", iFea_1f, nFea_2f, nArrf[iFea_1f], nArrf[nFea_2f]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nTempff = nArrf[nFea_2f];

		nArrf[nFea_2f] = nArrf[iFea_1f];
		nArrf[iFea_1f] = nTempff;
		
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n Fin nArrf[%d] = %d, nArrf[%d] = %d", iFea_1f, nArrf[iFea_1f], nFea_2f,nArrf[nFea_2f]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} // for (iFea_1f = nDimf - 1; iFea_1f > 0; iFea_1f--)

}//void Shuffling_AnIntArr(...
/////////////////////////////////////////

int Shuffle_OneDim_Int_And_2Dim_Float_Arrs(
	const int nDimf,
	const int nNumOfVecsTotf,

	int nY_Arrf[], // [nNumOfVecsTotf]
	float fFeas_All_Arrf[]) //[//[nDimf*nNumOfVecsTotf]]
{
	void Shuffling_AnIntArr(
		const int nDimf,
		int nArrf[]); //[nDimf]; initially [0,1,2,...,nDimf - 1]

	int A_Vec_From_ArrOf_AllVecs(
		const int nDimf,
			const int nNumOfVecsTotf,
		const int nVecf,

		const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

		float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	int Writing_OneDimVec_To_2DimVec(
		const int nDimf,
		const int nNumOfVecsTotf,

		const int nVecf,

		const float fFeas_OneVec_Arrf[], //[nDim_D_WithConst]

		float fFeas_All_Arrf[]); //[nDimf*nNumOfVecsTotf]

	void Copying_Int_Arr1_To_Arr2(
		const int nDimf,
		const int nArr1f[], // [nDimf]
		int nArr2f[]); // [nDimf]

	void Copying_Float_Arr1_To_Arr2(
		const int nDimf,
		const float fArr1f[], // [nDimf]
		float fArr2f[]); // [nDimf];

	int
		nResf,
		iVecf,
		nVecCurf,

		nPosition_OfVecTempf,

		nIndexf,

		nSizeOf_2DimVecf = nDimf * nNumOfVecsTotf,

		nTempf,
		//nY_Tempf,
		nPosition_OfVecMaxf = nNumOfVecsTotf - 1,
		iFeaf;

int* nPositionsOfVecsArrf = new int[nNumOfVecsTotf];

if (nPositionsOfVecsArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nPositionsOfVecsArrf == NULL");
	fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nPositionsOfVecsArrf == NULL");
	//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return UNSUCCESSFUL_RETURN;
} //if (nPositionsOfVecsArrf == NULL)

int* nY_TempArrf = new int[nNumOfVecsTotf];
if (nY_TempArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nY_TempArrf == NULL");
	fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nY_TempArrf == NULL");
	//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] nPositionsOfVecsArrf;

	return UNSUCCESSFUL_RETURN;
} //if (nPositionsOfVecsArrf == NULL)

for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
{
	nPositionsOfVecsArrf[iVecf] = iVecf;
} // for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)


///////////////////////////////////////////////
Shuffling_AnIntArr(
	nNumOfVecsTotf, //const int nDimf,
	nPositionsOfVecsArrf); // int nArrf[]); //[nDimf]; initially [0,1,2,...,nDimf - 1]

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n Before 'Shuffling_AnIntArr':");
	for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
	{
		nTempf = iVecf * nDimf;
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;
			fprintf(fout, "\n iVecf = %d, iFeaf = %d, fFeas_All_Arrf[%d] = %E", iVecf, iFeaf, nIndexf,fFeas_All_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	} //for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 1: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
{
	nPosition_OfVecTempf = nPositionsOfVecsArrf[iVecf];

	if (nPosition_OfVecTempf < 0 || nPosition_OfVecTempf > nPosition_OfVecMaxf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nPosition_OfVecTempf = %d, nPosition_OfVecMaxf = %d", nPosition_OfVecTempf, nPosition_OfVecMaxf);
		fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': nPosition_OfVecTempf = %d, nPosition_OfVecMaxf = %d", nPosition_OfVecTempf, nPosition_OfVecMaxf);
		//getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nPositionsOfVecsArrf;
		delete[] nY_TempArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nPosition_OfVecTempf < 0 || nPosition_OfVecTempf > nPosition_OfVecMaxf)

	nY_TempArrf[iVecf] = nY_Arrf[nPosition_OfVecTempf];

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n iVecf = %d, nPosition_OfVecTempf = %d, nPosition_OfVecMaxf = %d, nY_TempArrf[iVecf] = %d", 
		iVecf, nPosition_OfVecTempf, nPosition_OfVecMaxf, nY_TempArrf[iVecf]);

	//getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

} //for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 2: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//fin 'nY_Arrf[]' by copying from 'nY_TempArrf[]'
Copying_Int_Arr1_To_Arr2(
	nNumOfVecsTotf, //const int nDimf,

	nY_TempArrf, //const int nArr1f[], // [nDimf]
	nY_Arrf); // int nArr2f[]); // [nDimf]

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n After 'Copying_Int_Arr1_To_Arr2': nDimf = %d, nNumOfVecsTotf = %d", nDimf, nNumOfVecsTotf);
for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
{
	fprintf(fout, "\n nY_Arrf[%d] = %d", iVecf, nY_Arrf[iVecf]);
} //for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 2_1: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////
float* fFeasOf_OneVec_1Arrf = new float[nDimf];

if (fFeasOf_OneVec_1Arrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': fFeasOf_OneVec_1Arrf == NULL");
	fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': fFeasOf_OneVec_1Arrf == NULL");
	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] nPositionsOfVecsArrf;
	delete[] nY_TempArrf;

	return UNSUCCESSFUL_RETURN;
} //if (fFeasOf_OneVec_1Arrf == NULL)

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 2_2: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

float* fFeas_All_TempArrf = new float[nSizeOf_2DimVecf];
if (fFeas_All_TempArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs': fFeas_All_TempArrf == NULL");
	fprintf(fout, "\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs':  fFeas_All_TempArrf == NULL");
	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] nPositionsOfVecsArrf;
	delete[] nY_TempArrf;
	delete[] fFeasOf_OneVec_1Arrf;

	return UNSUCCESSFUL_RETURN;
} //if ( fFeas_All_TempArrf == NULL");)

//////////////////////////////////////////////////////////////////////
for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
{
	nPosition_OfVecTempf = nPositionsOfVecsArrf[iVecf];

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n Inside the loop for shuffling: iVecf = %d, nPosition_OfVecTempf = %d", iVecf, nPosition_OfVecTempf);
	if (nPosition_OfVecTempf < 6)
	{
		fprintf(fout, "\n\n 3: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E", fFeas_All_Arrf[4], fFeas_All_Arrf[5]);
	} // if (nPosition_OfVecTempf < 6)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 3_1: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

///////////////////////////////////////
	nResf = A_Vec_From_ArrOf_AllVecs(
		nDimf, //const int nDimf,
			nNumOfVecsTotf, //const int nNumOfVecsTotf,
		
		nPosition_OfVecTempf, //const int nVecf,

		fFeas_All_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

		fFeasOf_OneVec_1Arrf); // float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs' by 'A_Vec_From_ArrOf_AllVecs' 1 at iVecf = %d", iVecf);
		fprintf(fout, "\n\n  An error in  'Shuffle_OneDim_Int_And_2Dim_Float_Arrs' by 'A_Vec_From_ArrOf_AllVecs' 1 at iVecf = %d", iVecf);
		getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nPositionsOfVecsArrf;
		delete[] nY_TempArrf;

		delete[] fFeasOf_OneVec_1Arrf;
		delete[] fFeas_All_TempArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)


#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 3_2:  fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n Shuffling: iVecf = %d, nPosition_OfVecTempf = %d", iVecf, nPosition_OfVecTempf);

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fprintf(fout, "\n iVecf = %d, fFeasOf_OneVec_1Arrf[%d] = %E", iVecf, iFeaf, fFeasOf_OneVec_1Arrf[iFeaf]);
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	fflush(fout);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


#ifndef COMMENT_OUT_ALL_PRINTS
	//fprintf(fout, "\n\n 3_3: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//////////////////////////////////

	nResf = Writing_OneDimVec_To_2DimVec(
		nDimf, //const int nDimf,
		nNumOfVecsTotf, //const int nNumOfVecsTotf,

		iVecf, //const int nVecf,

		fFeasOf_OneVec_1Arrf, //const float fFeas_OneVec_Arrf[], //[nDim_D_WithConst]

		fFeas_All_TempArrf); // float fFeas_All_Arrf[]); //[nDimf*nNumOfVecsTotf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs' by 'Writing_OneDimVec_To_2DimVec' at iVecf = %d", iVecf);
		fprintf(fout, "\n\n  An error in  'Shuffle_OneDim_Int_And_2Dim_Float_Arrs' by 'Writing_OneDimVec_To_2DimVec' at iVecf = %d", iVecf);
		getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nPositionsOfVecsArrf;
		delete[] nY_TempArrf;

		delete[] fFeasOf_OneVec_1Arrf;
		delete[] fFeas_All_TempArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////
	
#ifndef COMMENT_OUT_ALL_PRINTS
	//fprintf(fout, "\n\n 3_4: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

}//for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

Copying_Float_Arr1_To_Arr2(
	nSizeOf_2DimVecf, //const int nDimf,

	fFeas_All_TempArrf, //const float fArr1f[], // [nDimf]
	fFeas_All_Arrf); // float fArr2f[]); // [nDimf];

#ifndef COMMENT_OUT_ALL_PRINTS
 //fprintf(fout, "\n\n 3_5: fFeas_All_Arrf[4] = %E, fFeas_All_Arrf[5] = %E, iVecf = %d, nPosition_OfVecTempf = %d", fFeas_All_Arrf[4], fFeas_All_Arrf[5], iVecf, nPosition_OfVecTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/*
for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
{
	nPositionsOfVecsArrf[iFeaf] = iFeaf;
} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

*/
//[nDimf*nVecTotf]
delete [] nPositionsOfVecsArrf;
delete[] nY_TempArrf;

delete[] fFeasOf_OneVec_1Arrf;
delete[] fFeas_All_TempArrf;

return SUCCESSFUL_RETURN;
}//int Shuffle_OneDim_Int_And_2Dim_Float_Arrs(...

////////////////////////////////////////////////////////////////////////////
int doPasAggMaxOut_TrainTest(
	const int nNumOfItersOfTrainingTotf,

//const int nDimf, == nDim_D_WithConstf

	const int nNumVecTrainTotf,
	const int nNumVecTestTotf,
/////////////////////////

	const int nDim_D_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

	///////////////////////
	const float fAlphaf, // < 1.0
	const float fEpsilonf,
	const float fCrf,
	const float fCf,
	///////////////////////////////////////////////////
	
	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults,
	PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults)
{
	///////////////////////////////////////////////////
	int Reading_All_TrainTest_Data(
		const int nDim_Df,

		const int nNumVecTrainTotf,
		const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		int nY_Train_Arrf[], //[nNumVecTrainTot]
		int nY_Test_Arrf[], //[nNumVecTestTot]

		float fFeaTrain_Arrf[], //[nProdTrainTot]
		float fFeaTest_Arrf[]); //[nProdTestTot]

	int Shuffle_OneDim_Int_And_2Dim_Float_Arrs(
		const int nDimf,
		const int nNumOfVecsTotf,

		int nY_Arrf[], // [nNumOfVecsTotf]
		float fFeas_All_Arrf[]); //[//[nDimf*nNumOfVecsTotf]]


	int Normalizing_Every_Fea_To_Mean_0_And_StDev_1(
		const float fLargef,
		const float fepsf,

		const int nDimf,

		const int nVecTrainf,
		const int nVecTestf,
		/////////////////////////////////////////////
		float fFeaMin_TrainArrf[],
		float fFeaMax_TrainArrf[],

		float fFea_TrainArrf[], //to be normalized
		float fFea_TestArrf[]); //to be normalized using the train Mean and StDev

	int PasAggMaxOut_Train(
		const int nNumOfItersOfTrainingTotf,

//after shuffling
		const float fFeaTrain_Arrf[], //[nProdTrainTot], to be normalized
		const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	//const int nDimf, == nDim_D_WithConstf

		const int nVecTrainf,
		/////////////////////////

		const int nDim_D_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

///////////////////////
		const float fAlphaf, // < 1.0
		const float fEpsilonf,
		const float fCrf,
		const float fCf,
		///////////////////////////////////////////////////
		float fW_Arrf[],
		float fU_Arrf[], //[nDim_U],

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults);

	int PasAggMaxOut_Test(
		const float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
		const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_WithConstf

		const int nVecTestf,

		/////////////////////////
		const int nDim_D_WithConstf, // = dimension of the original space
		const int nDim_Hf, //dimension of the nonlinear/transformed space

		const int nKf, //nNumOfHyperplanes
		const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		const float fW_Train_Arrf[], //[nDim_Hf]
		const float fU_Train_Arrf[], //[nDim_U],
		///////////////////////////////////////////////////

		PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

	int
		iVecf,
		iFeaf,

		nNumOfActualFeasf = nDim_D_WithConst - 1,

		nIndexf,
		nIndexWithConstf,

		nTempf,
		nTempWithConstf,

		nY_Train_Arrf[nNumVecTrainTot], //[]
		nY_Test_Actual_Arrf[nNumVecTestTot], //[]
		nResf;

	float
		fFeaMin_TrainArrf[nDim_D_WithConst],
		fFeaMax_TrainArrf[nDim_D_WithConst],

		fW_Train_Arrf[nDim_H],
		fU_Train_Arrf[nDim_U], //[],

		fFeaTrain_Arrf[nProdTrainTot], //[]
		fFeaTest_Arrf[nProdTestTot],

		fFea_WithConstTrain_Arrf[nProd_WithConstTrainTot], //[]
		fFea_WithConstTest_Arrf[nProd_WithConstTestTot];

	////////////////////////////////////////////////
	
	nResf = Reading_All_TrainTest_Data(
		nDim_D, //const int nDim_Df, // dimension of original feature space

		nNumVecTrainTotf, //const int nNumVecTrainTotf,
		nNumVecTestTotf, //const int nNumVecTestTotf,

		///////////////////////////////////////////////////////
		nY_Train_Arrf, //int nY_Train_Arrf[], //[nNumVecTrainTot]
		nY_Test_Actual_Arrf, //int nY_Test_Arrf[], //[nNumVecTestTot]

		fFeaTrain_Arrf, //float fFeaTrain_Arrf[], //[nProdTrainTot]
		fFeaTest_Arrf); // float fFeaTest_Arrf[]); //[nProdTestTot]

	//printf("\n\n After 'Reading_All_TrainTest_Data'");

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'doPasAggMaxOut_TrainTest' by 'Reading_All_TrainTest_Data'");
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest' by 'Reading_All_TrainTest_Data'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n The train vectors:");

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		//nTempf = iVecf * nDim_D_WithConstf;
		nTempf = iVecf * nDim;

		fprintf(fout, "\n iVecf = %d, ", iVecf);
		for (iFeaf = 0; iFeaf < nDim; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;
			
			fprintf(fout," %E", fFeaTrain_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim; iFeaf++)

		fprintf(fout, " nY_Train_Arrf[%d] = %d, ", iVecf, nY_Train_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	fprintf(fout, "\n\n The test vectors:");
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		//nTempf = iVecf * nDim_D_WithConstf;
		nTempf = iVecf * nDim;
		fprintf(fout, "\n iVecf = %d, ", iVecf);
		//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
			for (iFeaf = 0; iFeaf < nDim; iFeaf++)
			{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeaTest_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim; iFeaf++)

		fprintf(fout, " nY_Test_Actual_Arrf[%d] = %d", iVecf, nY_Test_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

	//printf("\n\n After 'Reading_All_TrainTest_Data': please press any key"); fflush(fout);  getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifdef USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1

	nResf = Normalizing_Every_Fea_To_Mean_0_And_StDev_1(
		fLarge, //const float fLargef,
		eps, //const float fepsf,

		nDim_D, //const int nDimf,

		nNumVecTrainTotf, //const int nVecTrainf,
		nNumVecTestTotf, //const int nVecTestf,
		/////////////////////////////////////////////
		fFeaMin_TrainArrf, //float fFeaMin_TrainArrf[],
		fFeaMax_TrainArrf, //float fFeaMax_TrainArrf[],

		fFeaTrain_Arrf, //float fFea_TrainArrf[], //to be normalized
		fFeaTest_Arrf); // float fFea_TestArrf[]) //to be normalized using the train Mean and StDev


#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n The train vectors after normalization to mean 0 and stdev 1:");

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		//nTempf = iVecf * nDim_D_WithConstf;
		nTempf = iVecf * nDim;
		fprintf(fout, "\n Normalized train iVecf = %d, ", iVecf);
		//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
		for (iFeaf = 0; iFeaf < nDim; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeaTrain_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim; iFeaf++)

		fprintf(fout, " nY_Train_Arrf[%d] = %d, ", iVecf, nY_Train_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	fprintf(fout, "\n\n The test vectors after normalization to mean 0 and stdev 1:");
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		//nTempf = iVecf * nDim_D_WithConstf;
		nTempf = iVecf * nDim;
		fprintf(fout, "\n Normalized test iVecf = %d, ", iVecf);
	//	for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
			for (iFeaf = 0; iFeaf < nDim; iFeaf++)
			{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeaTest_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim; iFeaf++)

		fprintf(fout, " nY_Test_Actual_Arrf[%d] = %d", iVecf, nY_Test_Actual_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

//printf("\n\n After 'Reading_All_TrainTest_Data': please press any key"); fflush(fout);  getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS


#endif //#ifdef USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1
///////////////////////////////////////////////////////

//adding the const to fea vecs
	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		nTempf = iVecf * nDim_D;
		nTempWithConstf = iVecf * nDim_D_WithConstf;

		for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
		{
			nIndexWithConstf = iFeaf + nTempWithConstf;
			if (iFeaf < nNumOfActualFeasf)
			{
				nIndexf = iFeaf + nTempf;

				fFea_WithConstTrain_Arrf[nIndexWithConstf] = fFeaTrain_Arrf[nIndexf];
			} // if (iFeaf < nNumOfActualFeasf)
			else
			{
				fFea_WithConstTrain_Arrf[nIndexWithConstf] = fFeaConstInit_Glob;
			} //else	

		} // for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

		//fprintf(fout, " nY_Train_Arrf[%d] = %d, ", iVecf, nY_Train_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

////////////////////////////
	for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)
	{
		nTempf = iVecf * nDim_D;
		nTempWithConstf = iVecf * nDim_D_WithConstf;

		for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
		{
			nIndexWithConstf = iFeaf + nTempWithConstf;

			if (iFeaf < nNumOfActualFeasf)
			{
				nIndexf = iFeaf + nTempf;

				fFea_WithConstTest_Arrf[nIndexWithConstf] = fFeaTest_Arrf[nIndexf];
			} // if (iFeaf < nNumOfActualFeasf)
			else
			{
				fFea_WithConstTest_Arrf[nIndexWithConstf] = fFeaConstInit_Glob;
			} //else	

		} // for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

	}//for (iVecf = 0; iVecf < nNumVecTestTotf; iVecf++)

/////////////////////////////////////////////////////////
//shuffling train vectors only -- not test ones
	nResf = Shuffle_OneDim_Int_And_2Dim_Float_Arrs(
				nDim_D_WithConstf, //const int nDimf,
				nNumVecTrainTotf, //const int nNumOfVecsTotf,

				nY_Train_Arrf, //int nY_Arrf[], // [nNumOfVecsTotf]
				fFea_WithConstTrain_Arrf); // float fFeas_All_Arrf[]); //[//[nDimf*nNumOfVecsTotf]]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'doPasAggMaxOut_TrainTest' by 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs'");
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest' by 'Shuffle_OneDim_Int_And_2Dim_Float_Arrs'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n The train vectors after shuffling:");

	for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)
	{
		nTempf = iVecf * nDim_D_WithConstf;
		fprintf(fout, "\n Shuffled iVecf = %d, ", iVecf);
		for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;

			fprintf(fout, " %E", fFeaTrain_Arrf[nIndexf]);
		} // for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

		fprintf(fout, " nY_Train_Arrf[%d] = %d", iVecf, nY_Train_Arrf[iVecf]);
	}//for (iVecf = 0; iVecf < nNumVecTrainTotf; iVecf++)

	//printf("\n\n After shuffling: please press any key"); fflush(fout);  getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nResf = PasAggMaxOut_Train(
		nNumOfItersOfTrainingTotf, //const int nNumOfItersOfTrainingTotf,

//after shuffling
		fFea_WithConstTrain_Arrf, //const float fFea_WithConstTrain_Arrf[], //[nProd_WithConstTrainTot], to be normalized
		nY_Train_Arrf, //const int nY_Train_Actual_Arrf[], //[nNumVecTrainTot],

	//const int nDimf, == nDim_D_WithConstf

		nNumVecTrainTotf, //const int nVecTrainf,
		/////////////////////////

		nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

///////////////////////
		fAlphaf, //const float fAlphaf, // < 1.0
		fEpsilonf, //const float fEpsilonf,
		fCrf, //const float fCrf,
		fCf, //const float fCf,
			///////////////////////////////////////////////////
		fW_Train_Arrf, //float fW_Arrf[],
		fU_Train_Arrf, //float fU_Arrf[], //[nDim_U],

		sPasAggMaxOut_TrainResults); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TrainResults);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'doPasAggMaxOut_TrainTest' by 'PasAggMaxOut_Train'");
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest' by 'PasAggMaxOut_Train'");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  After 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

	fprintf(fout, "\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E", 
		sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...TrainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf, 
		sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n After 'PasAggMaxOut_Train': sPasAggMaxOut_TrainResults.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TrainResults->fPercentageOfCorrectTotf);

	printf("\n ...TrainResults.fPercentageOfCorrect_Positf = %E, ...TrainResults.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TrainResults->fPercentageOfCorrect_Negatf);

	printf("\n\n ...rainResults->nNumOfPosit_Y_Totf = %d, ...TrainResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...TrainResults->nNumOfNegat_Y_Totf = %d, ...TrainResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TrainResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TrainResults->nNumOfNegatCorrect_Y_Totf);

	//printf("\n\n Please press any key to continue:"); fflush(fout); getchar();

#ifdef EXIT_AFTER_TRAINING
	return SUCCESSFUL_RETURN;
#endif //#ifdef EXIT_AFTER_TRAINING

	nResf = PasAggMaxOut_Test(
		fFea_WithConstTest_Arrf, // const float fFea_WithConstTest_Arrf[], //[nProd_WithConstTestTot],
		nY_Test_Actual_Arrf, //const int nY_Test_Actual_Arrf[], //[nNumVecTestTot],

		//const int nDimf, == nDim_D_WithConstf

		nNumVecTestTotf, //const int nVecTestf,

		/////////////////////////
		nDim_D_WithConstf, //const int nDim_D_WithConstf, // = dimension of the original space
		nDim_Hf, //const int nDim_Hf, //dimension of the nonlinear/transformed space

		nKf, //const int nKf, //nNumOfHyperplanes
		nDim_Uf, //const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

		///////////////////////
		fW_Train_Arrf, //const float fW_Train_Arrf[], //[nDim_Hf]
		fU_Train_Arrf, //const float fU_Train_Arrf[], //[nDim_U],
		///////////////////////////////////////////////////

		sPasAggMaxOut_TestResults); // PAS_AGG_MAX_OUT_RESUTS *sPasAggMaxOut_TestResults);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'doPasAggMaxOut_TrainTest' by 'PasAggMaxOut_Test'");
		fprintf(fout, "\n\n  An error in  'doPasAggMaxOut_TrainTest' by 'PasAggMaxOut_Test'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	
	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n  After final 'PasAggMaxOut_Test: sPasAggMaxOut_TestResults.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf);

	fprintf(fout, "\n ...TestResults.fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

	fprintf(fout, "\n\n///////////////////////////////////////////////////////////");
	fprintf(fout, "\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

	fprintf(fout, "\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	printf("\n\n///////////////////////////////////////////////////////////");
	printf("\n\n After final 'PasAggMaxOut_Test': sPasAggMaxOut_TestResults.fPercentageOfCorrectTotf = %E, ", sPasAggMaxOut_TestResults->fPercentageOfCorrectTotf);
	printf("\n ...TestResults.fPercentageOfCorrect_Positf = %E, ...TestResults.fPercentageOfCorrect_Negatf = %E",
		sPasAggMaxOut_TestResults->fPercentageOfCorrect_Positf, sPasAggMaxOut_TestResults->fPercentageOfCorrect_Negatf);

	printf("\n\n ...TestResults->nNumOfPosit_Y_Totf = %d, ...TestResults->nNumOfPositCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfPosit_Y_Totf,
		sPasAggMaxOut_TestResults->nNumOfPositCorrect_Y_Totf);

	printf("\n\n ...TestResults->nNumOfNegat_Y_Totf = %d, ...TestResults->nNumOfNegatCorrect_Y_Totf = %d", sPasAggMaxOut_TestResults->nNumOfNegat_Y_Totf,
		sPasAggMaxOut_TestResults->nNumOfNegatCorrect_Y_Totf);
	printf("\n\n Please press any key to continue:"); fflush(fout);  getchar();


	return SUCCESSFUL_RETURN;
}//int doPasAggMaxOut_TrainTest(...
///////////////////////////////////////////////////////////////////////////////////

void Print_A_FloatOneDim_Arr(
	const int nDimf, //

	const float fArrf[]) //nDimf
{
	int
		iFeaf;

	fprintf(fout, "\n\n ///////////////////////////////////////////////////////////////////////////");
	printf("\n 'Print_A_Float_Arr': nDimf = %d\n", nDimf);
	fprintf(fout, "\n 'Print_A_Float_Arr': nDimf = %d\n", nDimf);

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
	printf( "%d:%E, ", iFeaf, fArrf[iFeaf]);
	fprintf(fout, "%d:%E, ", iFeaf, fArrf[iFeaf]);
	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	fprintf(fout, "\n");
}//void Print_A_FloatOneDim_Arr(...

//////////////////////////////////////////////////////////
int Print_fU_Arr(
	const int nDim_D_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

	const float fU_Arrf[]) //nDim_Uf
{
	int
		nIndexf,

		nIndexMaxf = nDim_Uf - 1,
		iFea_Hf,
		iHyperplanef,

		nProd_iFea_Hf_nDim_D_WithConstf, // = nFea_Hf * nDim_D_WithConstf,

		nProd_nDim_D_WithConstf_nDim_Hf = nDim_D_WithConstf * nDim_Hf,

		nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf,

		iFeaf;

	fprintf(fout, "\n\n ///////////////////////////////////////////////////////////////////////////");
	fprintf(fout, "\n 'Print_fU_Arr': nDim_Hf = %d, nKf = %d, nDim_D_WithConstf = %d, nDim_Uf = %d", nDim_Hf, nKf, nDim_D_WithConstf, nDim_Uf);

	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_D_WithConstf = iFea_Hf * nDim_D_WithConstf;
		fprintf(fout, "\n\n The next nonlinear fea: iFea_Hf = %d", iFea_Hf);

		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf = nProd_nDim_D_WithConstf_nDim_Hf * iHyperplanef;

			fprintf(fout, "\n\n The next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);

			for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
			{
				//nIndexf = iFeaf + (nDim_D_WithConstf*iFea_Hf) + (nDim_D_WithConstf*nDim_Hf*iHyperplanef) ;

				nIndexf = iFeaf + nProd_iFea_Hf_nDim_D_WithConstf + nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf;

				if (nIndexf > nIndexMaxf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'Print_fU_Arr': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
					fprintf(fout, "\n\n An error in 'Print_fU_Arr': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);

					getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				}//if (nIndexf > nIndexMaxf)

				fprintf(fout, "%d:%E, ", iFeaf, fU_Arrf[nIndexf]);

			}//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

		} //for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	return SUCCESSFUL_RETURN;
}//int Print_fU_Arr(...
////////////////////////////////////////////////////////////////////////////

int OrthonormalizationOfVectorsByGrammSchimdt(
	const int nDimf, //nDim_WithConst or nDim
	const int nNumOfVecsTotf,

	float fFeaVecsAll_Arrf[])
	//float fFeaVecsAll_Orthonormal_Arrf[])
{
	int A_Vec_From_ArrOf_AllVecs(
		const int nDimf,
		const int nNumOfVecsTotf,
		const int nVecf,

		const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

		float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	int Normalizing_A_Vector_ByStDev(
		const int nDimf,
		const float fFeas_InitArrf[],

		float fFeasNormalized_Arrf[]);

	///////////////////////////////////////////
	int
		nResf,
		nIndex_1f,
		nIndex_2f,

		nTemp_1f,
		nTemp_2f,

		iVec_1f, 
		iVec_2f,  
		iFeaf; 

	float
		fFeaVec_1_Arrf[nDim_WithConst], //or [nDim]
		fFeaVec_2_Arrf[nDim_WithConst], //or [nDim]
		fFeaVecNormalized_Arrf[nDim_WithConst], //or [nDim]

		fCorrectingfactorf,
		fScalarProd_1f,
		fScalarProd_2f;

	for (iVec_1f = 1; iVec_1f < nNumOfVecsTotf; ++iVec_1f)
	{

		nResf = A_Vec_From_ArrOf_AllVecs(
			nDimf, //const int nDimf,
			nNumOfVecsTotf, //const int nNumOfVecsTotf,
			iVec_1f, //const int nVecf,

			fFeaVecsAll_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

			fFeaVec_1_Arrf); //float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 1: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 1: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nResf == UNSUCCESSFUL_RETURN)
/////////////////////////////////////
		nTemp_1f = iVec_1f * nDimf;

		for (iVec_2f = 0; iVec_2f < iVec_1f; ++iVec_2f)
		{
			nResf = A_Vec_From_ArrOf_AllVecs(
				nDimf, //const int nDimf,
				nNumOfVecsTotf, //const int nNumOfVecsTotf,
				iVec_2f, //const int nVecf,

				fFeaVecsAll_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

				fFeaVec_2_Arrf); //float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d, iVec_2f = %d",
					iVec_1f, iVec_2f);
				fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d, iVec_2f = %d",
					iVec_1f, iVec_2f);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////////
			Scalar_Product(
				nDimf, //const int nDimf,

				fFeaVec_1_Arrf, //const float fFeas_Arr_1f[],

				fFeaVec_2_Arrf, //const float fFeas_Arr_2f[],

				fScalarProd_1f); //float &fScalar_Prodf)

			Scalar_Product(
				nDimf, //const int nDimf,

				fFeaVec_2_Arrf, //const float fFeas_Arr_1f[],

				fFeaVec_2_Arrf, //const float fFeas_Arr_2f[],
				fScalarProd_2f); //float &fScalar_Prodf)

			if (fScalarProd_2f > -eps && fScalarProd_2f < eps)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': fScalarProd_2f = %E is too small;  iVec_1f = %d, iVec_2f = %d",
					fScalarProd_2f, iVec_1f, iVec_2f);

				fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt': fScalarProd_2f = %E is too small;  iVec_1f = %d, iVec_2f = %d",
					fScalarProd_2f, iVec_1f, iVec_2f);
				getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (fScalarProd_2f > -feps && fScalarProd_2f < feps)

			fCorrectingfactorf = fScalarProd_1f / fScalarProd_2f;
			///////////////////////////////////////////////////
			nTemp_2f = iVec_2f * nDimf;

			for (iFeaf = 0; iFeaf < nDimf; ++iFeaf)
			{
				nIndex_1f = iFeaf + nTemp_1f; //(iVec_1f*nDimf);
				nIndex_2f = iFeaf + nTemp_2f; //iVec_2f*nDimf;

				fFeaVecsAll_Arrf[nIndex_1f] -= fCorrectingfactorf * fFeaVecsAll_Arrf[nIndex_2f];
			} // for (iFeaf = 0; iFeaf < nDimf; ++iFeaf)

		}//for (iVec_2f = 0; iVec_2f < iVec_1f; ++iVec_2f)
	}//for (iVec_1f = 1; iVec_1f < nNumOfVecsTotf; ++iVec_1f)
///////////////////////////////////////////////////////////////////////

#ifdef NORMALIOZATION_IN_OrthonormalizationOfVectorsByGrammSchimdt
// normalizing all already orthogonal vectors 
	for (iVec_1f = 0; iVec_1f < nNumOfVecsTotf; ++iVec_1f)
	{
		nResf = A_Vec_From_ArrOf_AllVecs(
			nDimf, //const int nDimf,
			nNumOfVecsTotf, //const int nNumOfVecsTotf,
			iVec_1f, //const int nVecf,

			fFeaVecsAll_Arrf, //const float fFeas_All_Arrf[], //[nDimf*nVecTotf]

			fFeaVec_1_Arrf); //float fFeas_OneVec_Arrf[]); //[nDim_D_WithConst]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 2: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			fprintf(fout, "\n\n An error in 'OrthonormalizationOfVectorsByGrammSchimdt' 2: nResf == UNSUCCESSFUL_RETURN for iVec_1f = %d", iVec_1f);
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nResf == UNSUCCESSFUL_RETURN)

		nResf = Normalizing_A_Vector_ByStDev(
			nDimf, //const int nDimf,
			fFeaVec_1_Arrf, //const float fFeas_InitArrf[],

			fFeaVecNormalized_Arrf); //float fFeasNormalized_Arrf[])

		nTemp_1f = iVec_1f * nDimf;
		for (iFeaf = 0; iFeaf < nDimf; ++iFeaf)
		{
			nIndex_1f = iFeaf + nTemp_1f; //(iVec_1f*nDimf);

			fFeaVecsAll_Arrf[nIndex_1f] = fFeaVecNormalized_Arrf[iFeaf];
		} // for (iFeaf = 0; iFeaf < nDimf; ++iFeaf)

	} //for (iVec_1f = 0; iVec_1f < nNumOfVecsTotf; ++iVec_1f)

#endif //#ifdef NORMALIOZATION_IN_OrthonormalizationOfVectorsByGrammSchimdt

	return SUCCESSFUL_RETURN;
} //int OrthonormalizationOfVectorsByGrammSchimdt(...
///////////////////////////////////////////////////

int OrthonormalizationOf_fU_Arrf(
	const int nDim_D_WithConstf, // = dimension of the original space
	const int nDim_Hf, //dimension of the nonlinear/transformed space

	const int nKf, //nNumOfHyperplanes
	const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

	 float fU_Arrf[]) //nDim_Uf
{
	int OrthonormalizationOfVectorsByGrammSchimdt(
		const int nDimf, //nDim_WithConst or nDim
		const int nNumOfVecsTotf,

		float fFeaVecsAll_Arrf[]);

	//////////////////////////////////////////////////////////////////////////////////////////////
	void Scalar_Product(
		const int nDimf,

		const float fFeas_Arr_1f[],

		const float fFeas_Arr_2f[],
		float &fScalar_Prodf);

	int
		nResf,
		nIndex_fU_Arrf,
		nIndex_fFeaVecsOf_OneNonliearFea_Arrf,

		nIndexMaxf = nDim_Uf - 1,
		iFea_Hf,
		iHyperplanef,

		nProd_iFea_Hf_nDim_D_WithConstf, // = nFea_Hf * nDim_D_WithConstf,

		nProd_nDim_D_WithConstf_nDim_Hf = nDim_D_WithConstf * nDim_Hf,

		nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf,
////////////////
		nProd_iHyperplane_nDim_D_WithConstf,

		nNumOfHyperplanesForOrthonormalizationf,
		iFeaf;

	float
		fScalar_Prodf;

	//	fFeaVecCur_Arrf[nDim_D_WithConst],
		//fFeaVecCur_Arrf[nDim_D_WithConst];

		//fFeaVecsOf_OneNonliearFea_Arrf[nK * nDim_D_WithConst];

	if (nKf <= nDim_D_WithConstf)
	{
		nNumOfHyperplanesForOrthonormalizationf = nKf;
	} // if (nKf <= nDim_D_WithConstf)
	else
	{
		nNumOfHyperplanesForOrthonormalizationf = nDim_D_WithConstf;
	} // 

	float *fFeaVecsOf_OneNonliearFea_Arrf = new float[nNumOfHyperplanesForOrthonormalizationf * nDim_D_WithConst];
	if (fFeaVecsOf_OneNonliearFea_Arrf == NULL)
	{
		printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecsOf_OneNonliearFea_Arrf == NULL");
		fprintf(fout,"\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecsOf_OneNonliearFea_Arrf == NULL");
		getchar(); exit(1);
	} //if (fFeaVecsOf_OneNonliearFea_Arrf == NULL)

	float *fFeaVecCur_Arrf = new float[nDim_D_WithConst];
	if (fFeaVecCur_Arrf == NULL)
	{
		printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecCur_Arrf == NULL");
		fprintf(fout, "\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecCur_Arrf == NULL");
		delete[] fFeaVecsOf_OneNonliearFea_Arrf;

		getchar(); exit(1);
	} //if (fFeaVecCur_Arrf == NULL)

	float *fFeaVecPrev_Arrf = new float[nDim_D_WithConst];
	if (fFeaVecPrev_Arrf == NULL)
	{
		printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecPrev_Arrf == NULL");
		fprintf(fout, "\n\n An error in 'OrthonormalizationOf_fU_Arrf': fFeaVecPrev_Arrf == NULL");
		delete[] fFeaVecsOf_OneNonliearFea_Arrf;
		delete[] fFeaVecCur_Arrf;

		getchar(); exit(1);
	} //if (fFeaVecPrev_Arrf == NULL)

///////////////////////////////////////////////////////
//Initialization
	for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
	{
		fFeaVecCur_Arrf[iFeaf] = fLarge;
		fFeaVecPrev_Arrf[iFeaf] = fLarge;
	}//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n ///////////////////////////////////////////////////////////////////////////");
	fprintf(fout, "\n 'OrthonormalizationOf_fU_Arrf': nDim_Hf = %d, nKf = %d, nDim_D_WithConstf = %d, nDim_Uf = %d, nNumOfHyperplanesForOrthonormalizationf = %d", 
		nDim_Hf, nKf, nDim_D_WithConstf, nDim_Uf, nNumOfHyperplanesForOrthonormalizationf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/*
	nProd_iFea_Hf_nDim_D_WithConstf = iFea_Hf * nDim_D_WithConstf;
		fprintf(fout, "\n\n The next nonlinear fea: iFea_Hf = %d", iFea_Hf);

		for (iHyperplanef = 0; iHyperplanef < nKf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf = nProd_nDim_D_WithConstf_nDim_Hf * iHyperplanef;

			fprintf(fout, "\n\n The next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);
*/
	for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)
	{
		nProd_iFea_Hf_nDim_D_WithConstf = iFea_Hf * nDim_D_WithConstf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n The next nonlinear fea: iFea_Hf = %d", iFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		for (iHyperplanef = 0; iHyperplanef < nNumOfHyperplanesForOrthonormalizationf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_WithConstf = iHyperplanef* nDim_D_WithConstf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n 'OrthonormalizationOf_fU_Arrf': the next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf = nProd_nDim_D_WithConstf_nDim_Hf * iHyperplanef;

			for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
			{
				nIndex_fFeaVecsOf_OneNonliearFea_Arrf = iFeaf + nProd_iHyperplane_nDim_D_WithConstf;

				nIndex_fU_Arrf = iFeaf + nProd_iFea_Hf_nDim_D_WithConstf + nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf;

				if (nIndex_fFeaVecsOf_OneNonliearFea_Arrf > nIndexMaxf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': nIndex_fU_Arrf = %d > nIndexMaxf = %d", nIndex_fU_Arrf, nIndexMaxf);
					fprintf(fout, "\n\n An error in 'OrthonormalizationOf_fU_Arrf': nIndex_fU_Arrf = %d > nIndexMaxf = %d", nIndex_fU_Arrf, nIndexMaxf);
					delete[] fFeaVecsOf_OneNonliearFea_Arrf;
					delete[] fFeaVecCur_Arrf;
					delete[] fFeaVecPrev_Arrf;

					getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				}//if (nIndex_fFeaVecsOf_OneNonliearFea_Arrf > nIndexMaxf)

				fFeaVecsOf_OneNonliearFea_Arrf[nIndex_fFeaVecsOf_OneNonliearFea_Arrf] = fU_Arrf[nIndex_fU_Arrf];

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "%d:%E, ", iFeaf, fU_Arrf[nIndex_fU_Arrf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			}//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

		} //for (iHyperplanef = 0; iHyperplanef < nNumOfHyperplanesForOrthonormalizationf; iHyperplanef++)

		nResf = OrthonormalizationOfVectorsByGrammSchimdt(
			nDim_D_WithConstf, //const int nDimf, //nDim_WithConst or nDim
			nNumOfHyperplanesForOrthonormalizationf, //const int nNumOfVecsTotf,

			fFeaVecsOf_OneNonliearFea_Arrf); // float fFeaVecsAll_Arrf[]);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'OrthonormalizationOf_fU_Arrf': nResf == UNSUCCESSFUL_RETURN for iFea_Hf = %d", iFea_Hf);
			fprintf(fout, "\n\n An error in 'OrthonormalizationOf_fU_Arrf': nResf == UNSUCCESSFUL_RETURN for iFea_Hf = %d", iFea_Hf);
			delete[] fFeaVecsOf_OneNonliearFea_Arrf;
			delete[] fFeaVecCur_Arrf;
			delete[] fFeaVecPrev_Arrf;
			getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nResf == UNSUCCESSFUL_RETURN)

/////////////////////////////////
//copying backwards to 'fU_Arrf[]'
		fScalar_Prodf = -fLarge;
		for (iHyperplanef = 0; iHyperplanef < nNumOfHyperplanesForOrthonormalizationf; iHyperplanef++)
		{
			nProd_iHyperplane_nDim_D_WithConstf = iHyperplanef * nDim_D_WithConstf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n After 'Orthonormalization: the next iHyperplanef = %d, iFea_Hf = %d\n", iHyperplanef, iFea_Hf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf = nProd_nDim_D_WithConstf_nDim_Hf * iHyperplanef;
			
			for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)
			{
				nIndex_fFeaVecsOf_OneNonliearFea_Arrf = iFeaf + nProd_iHyperplane_nDim_D_WithConstf;

				nIndex_fU_Arrf = iFeaf + nProd_iFea_Hf_nDim_D_WithConstf + nProd_iHyperplane_nDim_D_WithConstf_nDim_Hf;

				fU_Arrf[nIndex_fU_Arrf] = fFeaVecsOf_OneNonliearFea_Arrf[nIndex_fFeaVecsOf_OneNonliearFea_Arrf];

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout, "%d:%E, ", iFeaf, fU_Arrf[nIndex_fU_Arrf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				fFeaVecCur_Arrf[iFeaf] = fFeaVecsOf_OneNonliearFea_Arrf[nIndex_fFeaVecsOf_OneNonliearFea_Arrf];

				if (iHyperplanef > 0)
				{
					fFeaVecPrev_Arrf[iFeaf] = fFeaVecCur_Arrf[iFeaf];
				} // if (iHyperplanef > 0)
			}//for (iFeaf = 0; iFeaf < nDim_D_WithConstf; iFeaf++)

			if (iHyperplanef > 0)
			{
				Scalar_Product(
					nDim_D_WithConstf, //const int nDimf,

					fFeaVecCur_Arrf, //const float fFeas_Arr_1f[],

					fFeaVecPrev_Arrf, //const float fFeas_Arr_2f[],
					fScalar_Prodf); // float &fScalar_Prodf)

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After 'Orthonormalization: iHyperplanef = %d, iFea_Hf = %d, fScalar_Prodf = %E\n", iHyperplanef, iFea_Hf, fScalar_Prodf);
				fprintf(fout, "\n\n After 'Orthonormalization: iHyperplanef = %d, iFea_Hf = %d, fScalar_Prodf = %E\n", iHyperplanef, iFea_Hf, fScalar_Prodf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (fScalar_Prodf > fScalarProdOfNormalizedHyperplanesMax)
				{
					//#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n A warning in 'OrthonormalizationOf_fU_Arrf': fScalar_Prodf = %E > fScalarProdOfNormalizedHyperplanesMax = %E\n", fScalar_Prodf, fScalarProdOfNormalizedHyperplanesMax);
					printf("\n iHyperplanef = %d, iFea_Hf = %d, fScalar_Prodf = %E\n", iHyperplanef, iFea_Hf, fScalar_Prodf);

					//fprintf(fout, "\n A warning in 'OrthonormalizationOf_fU_Arrf': fScalar_Prodf = %E > fScalarProdOfNormalizedHyperplanesMax = %E\n", fScalar_Prodf, fScalarProdOfNormalizedHyperplanesMax);
					printf("\n Please press any key:");  getchar();

					//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} // if (fScalar_Prodf > fScalarProdOfNormalizedHyperplanesMax)

			} // if (iHyperplanef > 0)

		} //for (iHyperplanef = 0; iHyperplanef < nNumOfHyperplanesForOrthonormalizationf; iHyperplanef++)

	} //for (iFea_Hf = 0; iFea_Hf < nDim_Hf; iFea_Hf++)

	delete[] fFeaVecsOf_OneNonliearFea_Arrf;
	delete[] fFeaVecCur_Arrf;
	delete[] fFeaVecPrev_Arrf;

	return SUCCESSFUL_RETURN;
}//int OrthonormalizationOf_fU_Arrf(...

//printf("\n\nPlease press any key:"); getchar();